﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NHSO.IHR.MED.Entity
{
    [Serializable]
    public class ReportAccessHistoryEntity
    {
        public string DRUGBRING_ID { get; set; }
        public string DRUGBRING_ORDER { get; set; }
        public string EMP_BUREAU { get; set; }
        public string EMP_BUREAUID { get; set; }
        public string EMP_ID { get; set; }
        public string EMP_THNAME { get; set; }
        public string MEDICINE_AMOUNT { get; set; }
        public string CREATE_DATE { get; set; }
        public string DRUGLIST_INDEX { get; set; }
        public string DRUGLIST_NAME { get; set; }
        public string AMOUNT { get; set; }
        public string BEGIN_DATE { get; set; }
        public string END_DATE { get; set; }
        public string SYMPTOM_ID { get; set; }
        public string DRUGLIST_ID { get; set; }
        public string SYMPTOM_NAME { get; set; }
        public string LABEL_DESC { get; set; }
    }
}
