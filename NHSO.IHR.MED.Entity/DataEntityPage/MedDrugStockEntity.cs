﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NHSO.IHR.MED.Entity
{
    [Serializable]
    public class MedDrugStockEntity
    {
        public string DRUGLIST_INDEX { get; set; }
        public string DRUGLIST_ID { get; set; }
        public string DRUGLIST_NAME { get; set; }
        public string DRUGLIST_DESC { get; set; }
        public string DRUGGROUP_ID { get; set; }
        public string DRUGGROUP_NAME { get; set; }
        public string DRUGTYPE_ID { get; set; }
        public string DRUGTYPE_NAME { get; set; }
        public string UNITLIST_ID { get; set; }
        public string UNITLIST_NAME { get; set; }
        public string DRUGLIST_DOSE { get; set; }
        public string UNITTYPE_ID { get; set; }
        public string UNITTYPE_NAME { get; set; }
        public string BARCODE_STANDARD { get; set; }
        public string BARCODE_TEMP { get; set; }
        public string FLAG { get; set; }
        public string AVAILABLE_INDEX { get; set; }
        public string STOCK_BALANCE { get; set; }
        public string STOCK_INPUT { get; set; }
        public string STOCK_OUTPUT { get; set; }
        public string UPDATE_BY { get; set; }
        public string UPDATE_DATE { get; set; }
        public string CREATE_BY { get; set; }
        public string CREATE_DATE { get; set; }
        public string STOCK_MIN { get; set; }
        public string STOCK_MAX { get; set; }
    }
}
