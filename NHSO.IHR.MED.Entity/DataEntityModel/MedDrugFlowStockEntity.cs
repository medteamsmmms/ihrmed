﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NHSO.IHR.MED.Entity
{
    [Serializable]
    public class MedDrugFlowStockEntity
    {
        public string FLOWSTOCK_ID { get; set; }
        public string DRUGLIST_INDEX { get; set; }
        public string STOCK_INPUT { get; set; }
        public string STOCK_OUTPUT { get; set; }
        public string CREATE_BY { get; set; }
        public string CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public string UPDATE_DATE { get; set; }
    }
}
