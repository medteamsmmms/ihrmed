﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NHSO.IHR.MED.Entity
{
    [Serializable]
    public class MedDrugLabelEntity
    {
        public string DRUGLIST_INDEX { get; set; }
        public string DRUGLIST_ID { get; set; }
        public string DRUGLIST_NAME { get; set; }
        public string DRUGLIST_DESC { get; set; }
        public string DRUGGROUP_ID { get; set; }
        public string DRUGGROUP_NAME { get; set; }
        public string DRUGTYPE_ID { get; set; }
        public string DRUGTYPE_NAME { get; set; }
        public string UNITLIST_ID { get; set; }
        public string UNITLIST_NAME { get; set; }
        public string DRUGLIST_DOSE { get; set; }
        public string UNITTYPE_ID { get; set; }
        public string UNITTYPE_NAME { get; set; }
        public string BARCODE_STANDARD { get; set; }
        public string BARCODE_TEMP { get; set; }
        public string FLAG { get; set; }
        public string DRUG_USETIME { get; set; }
        public string DRUGUSE_ID { get; set; }
        public string DRUG_MEALTIME0 { get; set; }
        public string DRUG_MEALTIME1 { get; set; }
        public string DRUG_TIME0 { get; set; }
        public string DRUG_TIME1 { get; set; }
        public string DRUG_TIME2 { get; set; }
        public string DRUG_TIME3 { get; set; }
        public string DRUG_USEFOR { get; set; }
        public string LABEL_DESC { get; set; }
        public string CREATE_BY { get; set; }
        public string CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public string UPDATE_DATE { get; set; }
        public string MEALTIME_PERDAY { get; set; }
        public string MEALTIME_DESC { get; set; }
        public string DRUG_TIME_DESC { get; set; }   
    }
}
