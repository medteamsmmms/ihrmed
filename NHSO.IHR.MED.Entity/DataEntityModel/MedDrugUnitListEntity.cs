﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NHSO.IHR.MED.Entity
{
    [Serializable]
    public class MedDrugUnitListEntity
    {
        public string UNITLIST_ID { get; set; }
        public string UNITLIST_NAME { get; set; }
        public string UNITLIST_DESC { get; set; }
        public string CREATE_BY { get; set; }
        public string CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public string UPDATE_DATE { get; set; }
        public string FLAG { get; set; }
        public string COUNTER { get; set; }
    }
}
