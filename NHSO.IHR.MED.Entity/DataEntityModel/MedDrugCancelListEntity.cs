﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NHSO.IHR.MED.Entity
{
    [Serializable]
    public class MedDrugCancelListEntity
    {
        public string CANCEL_ID { get; set; }
        public string CANCEL_CAPTION { get; set; }
    }
}
