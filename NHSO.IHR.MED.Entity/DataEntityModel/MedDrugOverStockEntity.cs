﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NHSO.IHR.MED.Entity
{
    [Serializable]
    public class MedDrugOverStockEntity
    {
        public string DRUGLIST_INDEX { get; set; }
        public string STOCK_BALANCE { get; set; }
        public string UPDATE_BY { get; set; }
        public string UPDATE_DATE { get; set; }
    }
}
