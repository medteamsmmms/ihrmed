﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NHSO.IHR.MED.Entity
{
    [Serializable]
    public class MedDrugTypeEntity
    {
        public string DRUGTYPE_ID { get; set; }
        public string DRUGTYPE_NAME { get; set; }
        public string DRUGTYPE_DESC { get; set; }
        public string CREATE_BY { get; set; }
        public string CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public string UPDATE_DATE { get; set; }
        public string FLAG { get; set; }
        public string COUNTER { get; set; }
        public string SYS_CREATE_DATE { get; set; }

        public string START_DATE { get; set; }
        public string END_DATE { get; set; }

    }
}
