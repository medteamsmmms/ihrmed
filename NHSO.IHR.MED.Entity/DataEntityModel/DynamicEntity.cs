﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;

namespace NHSO.IHR.MED.Entity
{
    public class DynamicEntity : DynamicObject
    {
        private Dictionary<string, object> property = new Dictionary<string, object>();

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            return property.TryGetValue(binder.Name, out result);
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            property[binder.Name] = value;
            return true;
        }

        public void AddProperty(string key, object value)
        {
            this.SetProperty(key, value);
        }

        public void SetProperty(string key, object value)
        {
            property[key] = value;
        }

        public object GetProperty(string key)
        {
            return property[key];
        }

        public Dictionary<string, object> GetProperties()
        {
            return property;
        }
    }
}
