﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NHSO.IHR.MED.Entity
{
    public class WithDrawSearchEntity
    {
        public bool Remain { get; set; }
        public string MedName { get; set; }
        public string MedGroup { get; set; }
        public string MedType { get; set; }
    }
}
