﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NHSO.IHR.MED.Entity
{
    [Serializable]
    public class MedDrugLabelPrintEntity
    {
        public string EMP_THNAME { get; set; }
        public string DRUGLIST_NAME { get; set; }
        public string DRUGLIST_DESC { get; set; }
        public string DRUG_USETIME_DESC { get; set; }
        public string MEALTIME_PERDAY_DESC { get; set; }
        public string MEALTIME_DESC { get; set; }
        public string DRUGTIME_DESC { get; set; }
        public string DRUG_USEFOR { get; set; }
        public string CREATE_DATE { get; set; }
    }
}
