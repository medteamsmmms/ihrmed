﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NHSO.IHR.MED.Entity
{
    public class MedDrugSymptomEntity
    {
        public string SYMPTOM_ID { get; set; }
        public string DRUGGROUP_ID { get; set; }
        public string DRUGGROUP_NAME { get; set; }
        public string SYMPTOM_NAME { get; set; }
        public string SYMPTOM_DESC { get; set; }
        public string CREATE_BY { get; set; }
        public string CREATE_DATE { get; set; }
        public string UPDATE_BY { get; set; }
        public string UPDATE_DATE { get; set; }
        public string FLAG { get; set; }
        public string COUNTER { get; set; }

        public string START_DATE { get; set; }
        public string END_DATE { get; set; }

    }
}
