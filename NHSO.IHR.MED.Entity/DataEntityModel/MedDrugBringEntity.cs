﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NHSO.IHR.MED.Entity
{
   public class MedDrugBringEntity
    {
        public string ID { get; set; }
        public string DRUGBRING_ID { get; set; }
        public string EMP_ID { get; set; }
        public string EMP_NAME { get; set; }
        public string DRUGBRING_ORDER { get; set; }
        public string SYMTOM_NAME { get; set; }
        public string SYMTOM_ID { get; set; }
        public string FLOWSTOCK_ID { get; set; }
        public string DRUGLIST_INDEX { get; set; }
        public string DRUGLIST_ID { get; set; }
        public string DRUGLIST_NAME { get; set; }
        public string UNITLIST_NAME { get; set; }
        public string DRUGGROUP_ID { get; set; }
        public string DRUGGROUP_NAME { get; set; }
        public string STOCK_INPUT { get; set; }
        public string STOCK_OUTPUT { get; set; }
       
    }
}
