﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Admin.Master" AutoEventWireup="true" CodeBehind="reportAccessHistory.aspx.cs" Inherits="NHSO.IHR.MED.admin.reportAccessHistory" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Usercontrols/ucTextDateValidate.ascx" TagPrefix="uc1" TagName="ucTextDateValidate" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1">
    



    <ul class="main pull-left">
        <li class="dropdown">
            <a href="medicineWithdrawManagement.aspx"><i class="fa fa-fw fa-home"></i>หน้าหลัก </a>

        </li>


    </ul>
</asp:Content>


<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder2">


    <asp:Panel ID="MedDrugListPanel" runat="server">

        <div class="row row-app">

           

            <!-- col -->
            <div class="col-sm-9">

                <!-- col-separator.box -->
                <div class="col-separator col-unscrollable box">

                    <!-- col-table -->
                    <div class="col-table">

                        <h4 class="margin-none innerAll border-bottom">รายงานการเบิกยา</h4>

                        <!-- col-table-row -->
                        <div class="col-table-row">

                            <!-- col-app -->
                            <div class="col-app col-unscrollable">

                                <!-- col-app -->
                                <div class="col-app">
                                   
                                        <div class="bg-gray border-bottom  innerAll">
                                            <div class="row ">
                                                <div class="col-md-12">
                                                    <div class="input-group input-group col-md-12">
                                                        <div class="input-group-btn ">
                                                           
                                                            
                                                             <asp:DropDownList ID="ddSearchBy" runat="server" data-toggle="dropdown"  CssClass="btn btn-default dropdown-toggle">
                                                                            </asp:DropDownList>
                                                            
                                                        </div>
                                                        <div class="input-group input-group col-md-12">
                                                            <asp:TextBox ID="txtBoxSearchName" runat="server" CssClass="form-control" placeholder="ค้นหาข้อมูล..."></asp:TextBox>
                                                             
                                                            <span class="input-group-btn">
                                                                
                                                                <asp:LinkButton ID="btnClearSearch" CssClass="btn btn-default" runat="server" OnClick="btnClearSearch_Click">ล้าง</asp:LinkButton>
                                                                
                                                                <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary" runat="server" OnClick="btnSearch_Click"><i class="fa fa-search"></i></asp:LinkButton>

                                                            </span>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row ">                                                <div class="col-md-3">
                                                    <div class="input-group input-group innerT">

                                                        <%--<div class="input-group input-group">
                                                            <span class="input-group-addon">เริ่มต้น</span>
                                                            <asp:TextBox ID="txtSelectDateBegin" runat="server" class="form-control" placeholder="2013-02-14"></asp:TextBox>
                                                            <uc1:ucTextDateValidate runat="server" ID="ucTextDateValidate" />
                                                        </div>--%>
                                                        <div class="input-group input-group date" id="txtSelectDateBegins">
                                                            <span class="input-group-addon">เริ่มต้น</span>
                                                            <asp:TextBox ID="txtSelectDateBegin" runat="server" class="form-control" placeholder="2013-02-14" disabled></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-th"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="input-group input-group innerT ">

                                                        <div class="input-group input-group date" id="txtSelectDateEnds">
                                                            <span class="input-group-addon">สิ้นสุด</span>
                                                            <asp:TextBox ID="txtSelectDateEnd" runat="server" class="form-control" placeholder="2013-02-14" disabled></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-th"></i></span>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                    
                                    
                                </div>



                                <div class="heading-buttons innerLR border-bottom">

                                    <asp:Label ID="lbResult" runat="server" Text="Label"></asp:Label>

                                    <div class="clearfix"></div>
                                </div>
                                <div class="innerAll bg-white">
                                    <asp:Panel ID="pnGridview1" runat="server">
                                        <asp:GridView ID="GridView1" runat="server" OnRowCommand="GridView1_RowCommand" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs"
                                            PageSize="10" AllowPaging="True" Width="100%"
                                            OnPageIndexChanging="GridView1_PageIndexChanging">
                                            <Columns>
                                                <asp:BoundField DataField="DRUGBRING_ID" HeaderText="รหัสเบิก" NullDisplayText="-">
                                                    <HeaderStyle CssClass="center" />
                                                    <ItemStyle CssClass="center" />
                                                </asp:BoundField>

                                                <asp:BoundField DataField="EMP_ID" HeaderText="รหัสพนักงาน" NullDisplayText="-">
                                                    <HeaderStyle CssClass="center" />
                                                    <ItemStyle CssClass="left" />
                                                </asp:BoundField>

                                                <asp:BoundField DataField="EMP_THNAME" HeaderText="ชื่อ" NullDisplayText="-">
                                                    <HeaderStyle CssClass="center" />
                                                    <ItemStyle CssClass="left" />
                                                </asp:BoundField>



                                                <asp:BoundField DataField="MEDICINE_AMOUNT" HeaderText="จำนวน" NullDisplayText="-">
                                                    <HeaderStyle CssClass="center" />
                                                    <ItemStyle CssClass="center" />
                                                </asp:BoundField>

                                                <asp:BoundField DataField="DRUGBRING_ORDER" HeaderText="เลขที่เบิกยา" NullDisplayText="-">
                                                    <HeaderStyle CssClass="center" />
                                                    <ItemStyle CssClass="center" />
                                                </asp:BoundField>

                                                <asp:BoundField DataField="CREATE_DATE" HeaderText="วันที่" NullDisplayText="-">
                                                    <HeaderStyle CssClass="center" />
                                                    <ItemStyle CssClass="center" />
                                                </asp:BoundField>

                                                <asp:TemplateField HeaderStyle-CssClass="center" HeaderText="">
                                                    <ItemStyle CssClass="center" />
                                                    <ItemTemplate>

                                                        <div class="btn-group btn-group-sm">
                                                            <asp:LinkButton ID="btnRowView" runat="server" CommandName="viewDrugBringFunc" CommandArgument='<%#Eval("DRUGBRING_ID") %>' CssClass="btn btn-default fa fa-eye" />
                                                            <asp:LinkButton ID="btnRowPrint" OnClientClick="return confirm('ยืนยันการพิมพ์');" runat="server" CommandName="cmdPrint" CommandArgument='<%#Eval("DRUGBRING_ID") %>' CssClass="btn btn-warning fa fa-print" />

                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>




                                        </asp:GridView>
                                    </asp:Panel>
                                    <asp:Panel ID="pnGridView2" runat="server">
                                        <asp:GridView ID="GridView2" runat="server" OnRowCommand="GridView2_RowCommand" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs"
                                            PageSize="10" AllowPaging="True" Width="100%"
                                            OnPageIndexChanging="GridView2_PageIndexChanging">
                                            <Columns>
                                                <asp:BoundField DataField="DRUGBRING_ID" HeaderText="รหัส" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                    <HeaderStyle CssClass="center" />
                                                    <ItemStyle CssClass="center" />
                                                </asp:BoundField>

                                                <asp:BoundField DataField="EMP_ID" HeaderText="รหัส" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" Visible="false">
                                                    <HeaderStyle CssClass="center" />
                                                    <ItemStyle CssClass="center" />
                                                </asp:BoundField>

                                                <asp:BoundField DataField="EMP_THNAME" HeaderText="ชื่อพนักงาน" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" Visible="false">
                                                    <HeaderStyle CssClass="center" />

                                                </asp:BoundField>

                                                <asp:BoundField DataField="DRUGLIST_INDEX" HeaderText="ชื่อยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-"  Visible="false">
                                                    <HeaderStyle CssClass="center" />

                                                </asp:BoundField>

                                                <asp:BoundField DataField="DRUGLIST_NAME" HeaderText="ชื่อยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                    <HeaderStyle CssClass="center" />

                                                </asp:BoundField>

                                                <asp:BoundField DataField="LABEL_DESC" HeaderText="ฉลากยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                    <HeaderStyle CssClass="center" />

                                                </asp:BoundField>


                                                <asp:BoundField DataField="DRUGBRING_ORDER" HeaderText="เลขสั่ง" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                    <HeaderStyle CssClass="center" />
                                                </asp:BoundField>

                                                <asp:BoundField DataField="CREATE_DATE" HeaderText="สร้างเมื่อ" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                    <HeaderStyle CssClass="center" />

                                                </asp:BoundField>

                                                <asp:TemplateField HeaderStyle-CssClass="center" HeaderText="">
                                                    <ItemStyle CssClass="center" />
                                                    <ItemTemplate>
                                                        <div class="btn-group btn-group-sm">
                                                            <asp:LinkButton ID="btnRowPrint" runat="server" OnClientClick="return confirm('ยืนยันการพิมพ์');" CommandName="cmdPrint" CommandArgument='<%#Eval("DRUGLIST_INDEX") %>' CssClass="btn btn-warning fa fa-print"/>                                                            
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>




                                        </asp:GridView>

                                    </asp:Panel>
                                    <asp:Panel ID="pnGridView3" runat="server">
                                        <asp:GridView ID="GridView3" runat="server" OnRowCommand="GridView3_RowCommand" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs"
                                            PageSize="10" AllowPaging="True" Width="100%"
                                            OnPageIndexChanging="GridView3_PageIndexChanging">
                                            <Columns>
                                                <asp:BoundField DataField="DRUGBRING_ID" HeaderText="รหัส" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                    <HeaderStyle CssClass="center" />
                                                    <ItemStyle CssClass="center" />
                                                </asp:BoundField>

                                                <asp:BoundField DataField="EMP_ID" HeaderText="รหัส" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" Visible="false">
                                                    <HeaderStyle CssClass="center" />
                                                    <ItemStyle CssClass="center" />
                                                </asp:BoundField>

                                                <asp:BoundField DataField="EMP_THNAME" HeaderText="ชื่อพนักงาน" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" Visible="false">
                                                    <HeaderStyle CssClass="center" />

                                                </asp:BoundField>

                                                
                                                <asp:BoundField DataField="DRUGLIST_INDEX" HeaderText="ชื่อยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-"  Visible="false">
                                                    <HeaderStyle CssClass="center" />

                                                </asp:BoundField>


                                                <asp:BoundField DataField="DRUGLIST_NAME" HeaderText="ชื่อยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                    <HeaderStyle CssClass="center" />

                                                </asp:BoundField>

                                                <asp:BoundField DataField="LABEL_DESC" HeaderText="ฉลากยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                    <HeaderStyle CssClass="center" />

                                                </asp:BoundField>


                                                <asp:BoundField DataField="DRUGBRING_ORDER" HeaderText="เลขสั่ง" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                    <HeaderStyle CssClass="center" />
                                                </asp:BoundField>

                                                <asp:BoundField DataField="CREATE_DATE" HeaderText="สร้างเมื่อ" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                    <HeaderStyle CssClass="center" />

                                                </asp:BoundField>

                                                <asp:TemplateField HeaderStyle-CssClass="center" HeaderText="">
                                                    <ItemStyle CssClass="center" />
                                                    <ItemTemplate>
                                                        <div class="btn-group btn-group-sm">
                                                           <asp:LinkButton ID="btnRowPrint" runat="server" OnClientClick="return confirm('ยืนยันการพิมพ์');" CommandName="cmdPrint" CommandArgument='<%#Eval("DRUGLIST_INDEX") %>' CssClass="btn btn-warning fa fa-print"/>    

                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>




                                        </asp:GridView>

                                    </asp:Panel>
                                </div>
                                <div class="text-right innerAll border-top bg-white">
                                    <asp:Panel ID="pnButtonViewDrugBringPage" runat="server">
                                        <div class="btn-group btn-group">
                                            <asp:LinkButton ID="btnShowAll" runat="server" CssClass="btn btn-primary" OnClick="btnShowAll_Click"><i class="fa fa-fw fa-check-circle-o"></i> แสดงการเบิกยาทั้งหมด</asp:LinkButton>
                                            <asp:LinkButton ID="btnConfirm" runat="server" CssClass="btn btn-success" OnClick="btnConfirm_Click"><i class="fa fa-fw fa-check-circle-o"></i> ตกลง</asp:LinkButton>

                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnButtonViewEmpBringPage" runat="server">
                                        <div class="btn-group btn-group">
                                              <asp:LinkButton ID="btnConfirm2" runat="server" CssClass="btn btn-success" OnClick="btnConfirm_Click"><i class="fa fa-fw fa-check-circle-o"></i> ตกลง</asp:LinkButton>

                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <!-- // END col-app -->

                        </div>
                        <!-- // END col-app.col-unscrollable -->

                    </div>
                    <!-- // END col-table-row -->

                </div>
                <!-- // END col-table -->

            </div>
            <!-- // END col-separator.box -->

        </div>
        <!-- // END col -->

        </div>
        

    </asp:Panel>
    
    <asp:HiddenField ID="hdfFunc" runat="server" />
    <asp:HiddenField ID="hdfDrugID" runat="server" />
    <asp:HiddenField ID="hdfEmpID" runat="server" />
    <asp:HiddenField ID="hdfBringID" runat="server" />


    <asp:Panel ID="MedDrugErrorPanel" runat="server">
        <div class="row error">
            <div class="col-md-4 col-md-offset-1 center">
                <div class="center">
                    <img src="../assets//images/error-icon-bucket.png " class="error-icon" />
                </div>
            </div>
            <div class="col-md-5 content center">
                <h1 class="strong">Oups!</h1>
                <h4 class="innerB">This page does not exist.</h4>
                <div class="well">
                    ERROR MESSAGE GOES HERE!
                </div>
            </div>
        </div>
    </asp:Panel>


</asp:Content>
