﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NHSO.IHR.MED.Utility;
using System.IO;
namespace NHSO.IHR.MED.admin
{
    public partial class medicineReportBureau : System.Web.UI.Page
    {
        private ReportBureau _reportbureau;
        protected void Page_Init(object sender, EventArgs e)
        {
            _reportbureau = new ReportBureau();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtSelectDateBegin1.Text = DateTime.Now.AddDays(-7).ToStringUS();
                txtSelectDateEnd1.Text = DateTime.Now.ToStringUS();
                BindingDDL();
                BindingGV();
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            //base.VerifyRenderingInServerForm(control);
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindingGV();
        }
        private void BindingDDL()
        {
            ddDepSearch.DataSource = _reportbureau.GetBurea();
            ddDepSearch.DataTextField = "EMP_BUREAU";
            ddDepSearch.DataValueField = "EMP_BUREAUID";
            ddDepSearch.DataBind();
            ddDepSearch.Items.Insert(0, new ListItem("ทั้งหมด", "", true));
        }
        private void BindingGV()
        {

            //GridView1.DataSource = _reportbureau.GetReportDetail( ddDepSearch.SelectedValue
                                                                // , txtSelectDateBegin1.Text, txtSelectDateEnd1.Text);
            GridView1.DataSource =  new ReportPerson().GetReportDetail("", ddDepSearch.SelectedValue
                                                                , txtSelectDateBegin1.Text,txtSelectDateEnd1.Text);

            GridView1.DataBind();
            if (GridView1.Rows.Count != 0)
            {
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            txtTotalMedicines.Text = "<h5 class='innerAll margin-none bg-white strong border-bottom'>จำนวนที่แสดงผล:&nbsp;&nbsp;" + GridView1.Rows.Count + "&nbsp;รายการ</h5>";

        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
        //    Response.ClearContent();
        //    Response.ClearHeaders();
        //    Response.Buffer = true;
        //    Response.AddHeader("content-disposition", "attachment;filename=" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm") + ".xls");
        //    Response.Charset = "";
        //    Response.ContentType = "application/vnd.ms-excel";
        //    StringWriter sw = new StringWriter();
        //    HtmlTextWriter hw = new HtmlTextWriter(sw);
        //    GridView1.RenderControl(hw);
        //    Response.Write("<table>");
        //    Response.Write("<tr><td>รายงานรายสำนัก</td></tr>");
        //    Response.Write("<tr><td>สำนัก</td><td>" + ddDepSearch.SelectedItem.ToString() + "<td></tr>");
        //    Response.Write("<tr><td>ตั้งแต่วันที่ " + txtSelectDateBegin1.Text + "</td><td>ถึง " + txtSelectDateEnd1.Text + "</td></tr>");
        //    Response.Write("</table>");
        //    Response.Output.Write(sw.ToString());
        //    Response.Flush();
        //    Response.End();
             //string Haederhtml = "<table>" +
             //                   "<tr><td>รายงานรายสำนัก</td></tr>" +
             //                   "<tr><td>สำนัก</td><td>" + ddDepSearch.SelectedItem.ToString() + "<td></tr>" +
             //                   //"<tr><td>ตั้งแต่วันที่ " + txtSelectDateBegin1.Text + "</td><td>ถึง " + txtSelectDateEnd1.Text + "</td></tr>" +
             //                   "</table>";
            string Haederhtml = "<table style='border:1px solid #000;font-weight:bold;'>" +
                               "<tr><td colspan='12' style='text-align:center;background-color:#B0E2FF;'>รายงานการเบิกยารายสำนัก</td></tr>" +
                               "<tr><td colspan='12' style='border:1px solid #000;'>จำวนวนข้อมูล " + GridView1.Rows.Count + " รายการ</td></tr>" +
                               "</table>";
            List<int> HideColumns = new List<int>();
            PrintExcle.PrintLabel(DateTime.Now.ToString("dd_MM_yyyy_HH_mm") + ".xls", Haederhtml, this.Page, GridView1, HideColumns);
        }

        protected void btnClearSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("medicineReportBureau.aspx");
        }
    }
}