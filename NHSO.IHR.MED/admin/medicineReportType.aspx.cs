﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NHSO.IHR.MED.Utility;
using System.IO;
namespace NHSO.IHR.MED.admin
{
    public partial class medicineReportType : System.Web.UI.Page
    {
        private ReportType _reporttype;
        protected void Page_Init(object sender, EventArgs e)
        {
            _reporttype = new ReportType();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtSelectDateBegin1.Text = DateTime.Now.AddDays(-7).ToStringUS();
                txtSelectDateEnd1.Text = DateTime.Now.ToStringUS(); 
                bindingGroup();
                bindingType();
                bindingSymptom();
                BindingGv();
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            //base.VerifyRenderingInServerForm(control);
        }
        private void bindingGroup()
        {
            ddlgroup.DataSource = _reporttype.GetDrugGroup();
            ddlgroup.DataTextField = "DRUGGROUP_NAME";
            ddlgroup.DataValueField = "DRUGGROUP_ID";
            ddlgroup.DataBind();
            ddlgroup.Items.Insert(0, new ListItem("ทั้งหมด", "", true));
        }
        private void bindingType()
        {
            ddltype.DataSource = _reporttype.GetDrugType(ddlgroup.SelectedValue);
            ddltype.DataTextField = "DRUGTYPE_NAME";
            ddltype.DataValueField = "DRUGTYPE_ID";
            ddltype.DataBind();
            ddltype.Items.Insert(0, new ListItem("ทั้งหมด", "", true));
        }
         private void bindingSymptom()
        {
            ddlsymptom.DataSource = _reporttype.GetDrugSym("");
            ddlsymptom.DataTextField = "SYMPTOM_NAME";
            ddlsymptom.DataValueField = "SYMPTOM_ID";
            ddlsymptom.DataBind();
            ddlsymptom.Items.Insert(0, new ListItem("ทั้งหมด", "", true));
        }
         private void BindingGv()
         {

             GridView1.DataSource = _reporttype.GetReportDetail(ddlgroup.SelectedValue, ddltype.SelectedValue,ddlsymptom.SelectedValue
                                                                  , txtSelectDateBegin1.Text, txtSelectDateEnd1.Text);
             GridView1.DataBind();
             if (GridView1.Rows.Count != 0)
             {
                 GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
             }
             txtTotalMedicines.Text = "<h5 class='innerAll margin-none bg-white strong border-bottom'>จำนวนที่แสดงผล:&nbsp;&nbsp;" + GridView1.Rows.Count + "&nbsp;รายการ</h5>";
         }

         protected void btnSearch_Click(object sender, EventArgs e)
         {
             BindingGv();
         }

         protected void ddlgroup_SelectedIndexChanged(object sender, EventArgs e)
         {
             bindingType();
         }

         protected void btnExportExcel_Click(object sender, EventArgs e)
         {
             //Response.ClearContent();
             //Response.ClearHeaders();
             //Response.Buffer = true;
             //Response.AddHeader("content-disposition", "attachment;filename=" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm") + ".xls");
             //Response.Charset ="";
             //Response.ContentType ="application/vnd.ms-excel";
             //StringWriter sw = new StringWriter();
             //HtmlTextWriter hw = new HtmlTextWriter(sw);
             //GridView1.RenderControl(hw);
             //Response.Write("<table>");
             //Response.Write("<tr><td>รายงานรายสรรพคุณ</td></tr>");
             //Response.Write("<tr><td>โรคตามระบบ</td><td>" + ddlgroup.SelectedItem.ToString() + "<td></tr>");
             //Response.Write("<tr><td>สรรพคุณ</td><td>" + ddltype.SelectedItem.ToString() + "<td></tr>");
             //Response.Write("<tr><td>อาการ</td><td>" + ddlsymptom.SelectedItem.ToString() + "<td></tr>");
             //Response.Write("<tr><td>ตั้งแต่วันที่ " + txtSelectDateBegin1.Text + "</td><td>ถึง " + txtSelectDateEnd1.Text + "</td></tr>");
             //Response.Write("</table>");
             //Response.Output.Write(sw.ToString());
             //Response.Flush();
             //Response.End();

             string Haederhtml = "<table>" +
                               "<tr><td>รายงานรายสรรพคุณ</td></tr>" +
                               "<tr><td>โรคตามระบบ</td><td>" + ddlgroup.SelectedItem.ToString() + "<td></tr>" +
                               "<tr><td>สรรพคุณ</td><td>" + ddltype.SelectedItem.ToString() + "<td></tr>" +
                               "<tr><td>อาการ</td><td>" + ddlsymptom.SelectedItem.ToString() + "<td></tr>" +
                               "<tr><td>ตั้งแต่วันที่ " + txtSelectDateBegin1.Text + "</td><td>ถึง " + txtSelectDateEnd1.Text + "</td></tr>"+
                               "</table>";
             List<int> HideColumns = new List<int>();
             PrintExcle.PrintLabel(DateTime.Now.ToString("dd_MM_yyyy_HH_mm") + ".xls", Haederhtml, this.Page, GridView1, HideColumns);
         }

         protected void btnClearSearch_Click(object sender, EventArgs e)
         {
             Response.Redirect("medicineReportType.aspx");
         }
       
    }
}