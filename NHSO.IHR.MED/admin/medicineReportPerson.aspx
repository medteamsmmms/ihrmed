﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Admin.Master" AutoEventWireup="true" CodeBehind="medicineReportPerson.aspx.cs" Inherits="NHSO.IHR.MED.admin.medicineReportPerson" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Usercontrols/ucTextDateValidate.ascx" TagPrefix="uc1" TagName="ucTextDateValidate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {
            $("#txtSelectDateBegins").datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: "linked",
                language: "th",
                todayHighlight: true,
                autoclose: true
            });
            $("#txtSelectDateEnds").datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: "linked",
                language: "th",
                todayHighlight: true,
                autoclose: true
            });
        });

        //---- Add active Class-----//
        document.getElementById("HasReport").className += " li-active active";
        document.getElementById("SReport").style.borderColor = "#4a8bc2";
        document.getElementById("SReport").style.background = "#314250";
        document.getElementById("SReport").style.color = "white";
        document.getElementById("iReport").className += " active";
        document.getElementById("Person").className += " active";
        document.getElementById("sidebar-report").className += " in";
    </script>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="MedDrugListPanel" runat="server">
                <div class="row row-app" id="MedDrugListDiv">
                    <div class="col-md-12 animated fadeInDown">
                        <div class="col-separator col-separator-first col-unscrollable">
                            <div class="col-table">
                                <div class="heading-buttons innerLR box">
                                    <h4 class="margin-none innerTB pull-left">รายงานรายบุคคล</h4>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-separator-h"></div>
                                <div class="col-table-row">
                                    <div class="col-app col-unscrollable">
                                        <div class="col-app hasNiceScroll animated fadeInDown" tabindex="10000" style="overflow-y: hidden; outline: none;">
                                            <div class="col-table">
                                                <div class="col-separator-h box"></div>
                                                <div class="col-table-row">
                                                    <div class="row-app">
                                                        <div>
                                                            <div class="col-separator box bg-gray hasNiceScroll">
                                                                <h5 class="innerAll margin-none bg-primary strong border-bottom"><i class="fa fa-fw fa-search"></i>&nbsp;ค้นหา</h5>
                                                                <asp:Panel ID="Panel1" DefaultButton="btnSearch" runat="server">
                                                                    <div class="innerAll margin-none bg-gray strong border-bottom">
                                                                        <div class="row innerT">
                                                                            <div class="col-md-12">
                                                                                <div class="col-md-6">
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon bg-purple">สำนัก&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                        <asp:DropDownList ID="ddDepSearch" runat="server" data-style="btn-default" CssClass="form-control selectpicker col-md-6">
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div class="row innerT">
                                                                            <div class="col-md-12">
                                                                                <div class="col-md-3">
                                                                                    <div class="input-group input-group date" id="txtSelectDateBegins">
                                                                                        <span class="input-group-addon bg-purple">วันเริ่มต้น</span>
                                                                                        <asp:TextBox ID="txtSdate" runat="server" class="form-control" disabled></asp:TextBox>
                                                                                        <span class="input-group-addon"><i class="fa fa-th"></i></span>
                                                                                    </div>                                                                                  
                                                                                </div>
                                                                                <div class="col-md-3">
                                                                                    <div class="input-group input-group date" id="txtSelectDateEnds">
                                                                                        <span class="input-group-addon bg-purple">วันสิ้นสุด</span>
                                                                                        <asp:TextBox ID="txtEdate" runat="server" class="form-control" disabled></asp:TextBox>
                                                                                        <span class="input-group-addon"><i class="fa fa-th"></i></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row innerT">
                                                                            <div class="col-md-12">
                                                                                <div class="col-md-6">
                                                                                    <div class="input-group input-group col-md-12">
                                                                                        <span class="input-group-addon bg-purple">ค้นหาจาก ชื่อ/สกุล/รหัสพนักงาน</span>
                                                                                        <div class="input-group input-group col-md-12">
                                                                                            <asp:TextBox ID="txtBoxSearchName" runat="server" CssClass="form-control" placeholder="คำค้นหา..."></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row innerTB">
                                                                            <div class="col-md-12">
                                                                                <div class="btn-group col-md-6">
                                                                                    <asp:LinkButton ID="btnSearch" CssClass="btn btn-success col-md-2" runat="server" OnClick="btnSearch_Click">ค้นหา</asp:LinkButton>

                                                                                    <asp:LinkButton ID="btnClearSearch" CssClass="btn btn-default col-md-2 btn" runat="server" OnClick="btnClearSearch_Click">ล้าง</asp:LinkButton>
                                                                                    <asp:LinkButton ID="btnExportExcel" CssClass="btn btn-success" runat="server" OnClick="btnExportExcel_Click">นำข้อมูลออก</asp:LinkButton>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>

                                                                <h5 class='innerAll margin-none bg-primary strong border-bottom'><i class='fa fa-fw fa-calendar'></i>&nbsp;&nbsp;ตารางข้อมูล</h5>
                                                                 <asp:Label ID="txtTotalMedicines" runat="server" Text=""></asp:Label>

                                                                <div class="innerAll bg-gray">
                                                                    <!-- Total bookings & sort by options -->
                                                                    <div class="separator bottom">
                                                                        <span class="pull-right"></span>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                    <!-- // Total bookings & sort by options END -->
                                                                    <!-- Table -->
                                                                    <asp:Panel ID="pnGridview1" runat="server">
                                                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs"
                                                                            PageSize="100" Width="100%" Height="0px">
                                                                            <Columns>
                                                                                <asp:BoundField DataField="DRUGBRING_ORDER" HeaderText="เลขที่เบิกยา" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="center" />
                                                                                </asp:BoundField>

                                                                                 <asp:BoundField DataField="EMP_ID" HeaderText="รหัสพนักงาน" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="left" />
                                                                                </asp:BoundField>

                                                                                 <asp:BoundField DataField="EMP_THNAME" HeaderText="ชื่อ- สกุล" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="left" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="EMP_BUREAU" HeaderText="สำนัก" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="left" />
                                                                                </asp:BoundField>

                                                                                 <asp:BoundField DataField="druglist_name" HeaderText="ชื่อยา" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="left" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="DRUGLIST_DESC" HeaderText="ชื่อยาสามัญ" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="left" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="DRUGTYPE_NAME" HeaderText="สรรพคุณ" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="left" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="DRUGGROUP_NAME" HeaderText="โรคตามระบบ" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="left" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="SYMPTOM_NAME" HeaderText="อาการ" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="left" />
                                                                                </asp:BoundField>

                                                                              <%--  <asp:BoundField DataField="druglist_id" HeaderText="รหัสยา" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="right" />
                                                                                </asp:BoundField>--%>

                                                                                <asp:BoundField DataField="stock_output" HeaderText="จำนวน" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="right" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="unitlist_name" HeaderText="หน่วย" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="center" />
                                                                                </asp:BoundField>

                                                                                 <asp:BoundField DataField="CREATE_DATE" HeaderText="วันที่เบิกยา" NullDisplayText="-" DataFormatString="{0:g}">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="center" />
                                                                                </asp:BoundField>

                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </div>

                                                                <div class="col-separator-h box"></div>
                                                            </div>
                                                        </div>
                                                        <div id="ascrail2000" class="nicescroll-rails" style="width: 5px; z-index: 2; cursor: default; position: absolute; top: 0px; left: 1073px; height: 432px; display: none; opacity: 0;">
                                                            <div style="position: relative; top: 0px; float: right; width: 5px; height: 432px; border: none; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px; background-color: rgb(235, 106, 90); background-clip: padding-box;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
