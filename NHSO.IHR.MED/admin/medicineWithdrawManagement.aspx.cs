﻿using NHSO.IHR.MED.DataController;
using NHSO.IHR.MED.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NHSO.IHR.MED.Utility;
using NHSO.IHR.MED.Data.Oracle.Common;
using System.Data.OracleClient;
namespace NHSO.IHR.MED.admin
{
    public partial class medicineWithdrawManagement : Page
    {
        private WithdrawMed _withdrawmed;
        protected void Page_Init(object sender, EventArgs e)
        {
            _withdrawmed = new WithdrawMed();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SearchEmployees.Add_Click += new EventHandler(SearchEmployees_Add_Click);
            if (!IsPostBack)
            {
                bindingGroup();
                bindingType();
                BindingDrugList();
                Session["tempgv"] = null;
                Bindingbutton();
            }
        }
        private void SearchEmployees_Add_Click(object sender, EventArgs e)
        {
            txtsearchdetail.Text = SearchEmployees.Name;
            txtsearch.Text = SearchEmployees.Id;
            txtsearchdetail.Enabled = false;
            txtsearch.Enabled = false;
            txtAllergic.Text = _withdrawmed.SetAllergicDetail(SearchEmployees.Id);
        }
        protected void rblGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindingGroup();
            bindingType();
        }
        private void Bindingbutton()
        {
            if (gvtemp != null && gvtemp.Rows.Count > 0)
            {
                LinkButton1.Visible = true;
            }
            else
            {
                LinkButton1.Visible = false;
            }
        }
        private void bindingGroup()
        {
            ddlmGroup.DataSource = _withdrawmed.GetDrugGroup();
            ddlmGroup.DataTextField = "DRUGGROUP_NAME";
            ddlmGroup.DataValueField = "DRUGGROUP_ID";
            ddlmGroup.DataBind();
            ddlmGroup.Items.Insert(0, new ListItem("ทั้งหมด", "", true));
        }
        private void bindingType()
        {
            ddlmType.DataSource = _withdrawmed.GetDrugType(ddlmGroup.SelectedValue);
            ddlmType.DataTextField = "DRUGTYPE_NAME";
            ddlmType.DataValueField = "DRUGTYPE_ID";
            ddlmType.DataBind();
            ddlmType.Items.Insert(0, new ListItem("ทั้งหมด", "", true));
        }
        protected void lblistsearch_Click(object sender, EventArgs e)
        {
            BindingDrugList();
        }
        protected void BindingDrugList()
        {
            WithDrawSearchEntity ent = new WithDrawSearchEntity();
            ent.Remain = ckremain.Checked;
            ent.MedName = txtmname.Text;
            ent.MedGroup = ddlmGroup.SelectedValue;
            ent.MedType = ddlmType.SelectedValue;

            gvdruglist.DataSource = _withdrawmed.GetDrugList(ent);
            gvdruglist.DataBind();
        }
        protected void gvdruglist_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string value = e.CommandName;
            if (value == "cmdadd")
            {
                if (txtamount.Text.Toint() > 0)
                {
                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    if (Session["tempgv"] != null)
                    {
                        List<string> data = (from c in (List<MedDrugBringEntity>)(Session["tempgv"])
                                             where c.DRUGLIST_ID == row.Cells[1].Text
                                             select c.DRUGLIST_ID).ToList();
                        if (data.Count == 0)
                        {
                            additemgridtemp(row);
                        }
                    }
                    else
                    {
                        additemgridtemp(row);
                    }
                }
                else
                {
                    PopupMessage.Show("", "กรุณาระบุจำนวนยาที่ต้องการเบิก");
                }
            }
        }
        private void additemgridtemp(GridViewRow row)
        {
            if (_withdrawmed.CheckBalance(gvdruglist.DataKeys[row.RowIndex].Values[1].ToString(), txtamount.Text.Toint()) == true)
            {
                MedDrugBringEntity ent = new MedDrugBringEntity();
                List<MedDrugBringEntity> list = new List<MedDrugBringEntity>();
                MedDrugSymptomEntity sym = _withdrawmed.GetDrugSym("")[0];
                ent.DRUGLIST_NAME = row.Cells[2].Text;
                ent.DRUGLIST_ID = row.Cells[1].Text;
                ent.UNITLIST_NAME = row.Cells[7].Text;
                ent.DRUGLIST_INDEX = gvdruglist.DataKeys[row.RowIndex].Values[1].ToString();
                ent.DRUGGROUP_ID = gvdruglist.DataKeys[row.RowIndex].Values[0].ToString();
                ent.STOCK_OUTPUT = (txtamount.Text).Toint().ToString();
                //ent.SYMTOM_NAME = sym.SYMPTOM_NAME;
                //ent.SYMTOM_NAME = "โปรดระบุอาการ";
                //ent.SYMTOM_ID = sym.SYMPTOM_ID;

                List<MedDrugSymptomEntity> listBingDesc = _withdrawmed.GetLastBingDesc(ent.DRUGLIST_INDEX.Toint());
                ent.SYMTOM_ID = listBingDesc.Count != 0 ? listBingDesc[0].SYMPTOM_ID : sym.SYMPTOM_ID;
                ent.SYMTOM_NAME = listBingDesc.Count != 0 ?  listBingDesc[0].SYMPTOM_NAME :  sym.SYMPTOM_NAME;

                if (Session["tempgv"] != null)
                {
                    list = (List<MedDrugBringEntity>)(Session["tempgv"]);
                }
                list.Add(ent);
                Session["tempgv"] = list;
                gvtemp.DataSource = list;
                gvtemp.DataBind();
                Bindingbutton();
            }
            else
            {
                PopupMessage.Show("", "จำนวนที่เบิกมากกว่าจำนวนใน สต๊อก");
            }
        }
        private void bindinggridtemp()
        {
            if (Session["tempgv"] != null)
            {
                List<MedDrugBringEntity> list = (List<MedDrugBringEntity>)(Session["tempgv"]);
                gvtemp.DataSource = list;
                gvtemp.DataBind();
            }
        }
        protected void gvtemp_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataRowView drv = e.Row.DataItem as DataRowView;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //Find the DropDownList in the Row
                DropDownList ddlh = (e.Row.FindControl("ddlbehaviour") as DropDownList);
                Label lb1 = (e.Row.FindControl("lbbehavior") as Label);
                if (ddlh != null)
                {
                    ddlh.DataSource = _withdrawmed.GetDrugSym("").OrderBy(x=>x.SYMPTOM_NAME);
                    ddlh.DataTextField = "SYMPTOM_NAME";
                    ddlh.DataValueField = "SYMPTOM_ID";
                    ddlh.DataBind();
                    ddlh.Items.Insert(0, new ListItem("กรุณาเลือกข้อมูล", "", true));
                    //ddlh.SelectedValue = gvtemp.DataKeys[e.Row.RowIndex].Values[1].ToString();
                    try
                    {
                        ddlh.Items.FindByText(gvtemp.DataKeys[e.Row.RowIndex].Values[0].ToString()).Selected = true;
                    }
                    catch (Exception)
                    {
                    }

                }
            }
        }
        protected void gvtemp_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvtemp.EditIndex = e.NewEditIndex;
            bindinggridtemp();
        }
        protected void gvtemp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvtemp.EditIndex = -1;
            bindinggridtemp();
        }
        protected void gvtemp_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            List<MedDrugBringEntity> list = (List<MedDrugBringEntity>)(Session["tempgv"]);
            TextBox tx1 = (TextBox)gvtemp.Rows[e.RowIndex].FindControl("txtamont");
            DropDownList ddl1 = (DropDownList)gvtemp.Rows[e.RowIndex].FindControl("ddlbehaviour");
            if (_withdrawmed.CheckBalance(list[e.RowIndex].DRUGLIST_INDEX, tx1.Text.Toint()) == true)
            {
                list[e.RowIndex].STOCK_OUTPUT = tx1.Text;
                list[e.RowIndex].SYMTOM_NAME = ddl1.SelectedItem.Text;
                list[e.RowIndex].SYMTOM_ID = ddl1.SelectedValue;
                Session["tempgv"] = list;
                gvtemp.EditIndex = -1;
                bindinggridtemp();
            }
            else
            {
                PopupMessage.Show("", "จำนวนที่เบิกมากกว่าจำนวนใน สต๊อก");
            }

        }
        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            CommandData cmm = new CommandData();
            cmm.SetCommandStoredProcedure("PKG_MED_EMPLOYEES.md_get_employees");
            cmm.AddInputParameter("o_emp_id", OracleType.VarChar, txtsearch.Text.ToStringOrNull());
            cmm.AddOutputParameter("o_result", OracleType.Cursor, 100000);

            System.Data.DataSet ds = cmm.ExecuteToDataset();
            int countrows = ds.Tables[0].Rows.Count;

            if (countrows !=0)
            {
                SearchEmployees.Show(txtsearch.Text, ds);
                
            }
            else
            {
                PopupMessage.Show("", "! ไม่พบข้อมูล");
            }      
        }
        protected void LinkButton3_Click(object sender, EventArgs e)
        {
            txtsearchdetail.Text = "";
            txtsearch.Text = "";
            txtsearchdetail.Enabled = false;
            txtsearch.Enabled = true;
            txtAllergic.Text = "";
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            if (Session["tempgv"] != null)
            {
                if (_withdrawmed.CheckEmployeeID(txtsearch.Text) == true)
                {
                    DateTime d = ucTextDateValidate.TextValue.ToDateUS();
                    List<MedDrugBringEntity> list = (List<MedDrugBringEntity>)(Session["tempgv"]);
                    _withdrawmed.InsertRecord(list, txtsearch.Text, d);
                    list.Clear();
                    Session["tempgv"] = null;
                    gvtemp.DataSource = list;
                    gvtemp.DataBind();
                    BindingDrugList();
                    Bindingbutton();
                }
                else
                {
                    PopupMessage.Show("", "รหัสสมาชิกไม่ถูกต้อง");
                }

            }
            else
            {
                PopupMessage.Show("", "กรุณาเลือกรายการยา");
            }

        }
        protected void gvtemp_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string value = e.CommandName;
            if (value == "CmdDelete")
            {
                try
                {
                    List<MedDrugBringEntity> list = (List<MedDrugBringEntity>)(Session["tempgv"]);
                    list = list.Where(x => x.DRUGLIST_ID.Trim() != e.CommandArgument.ToString().Trim()).ToList();
                    Session["tempgv"] = list;
                    gvtemp.DataSource = list;
                    gvtemp.DataBind();
                    Bindingbutton();
                }
                catch (Exception)
                {
                    
                }
            }
        }
        protected void gvdruglist_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvdruglist.PageIndex = e.NewPageIndex;
            BindingDrugList();
        }
        protected void gvdruglist_PreRender(object sender, EventArgs e)
        {
            if (gvdruglist.Rows.Count > 0)
            {
                gvdruglist.UseAccessibleHeader = true;
                gvdruglist.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
        protected void gvtemp_PreRender(object sender, EventArgs e)
        {

            if (gvtemp.Rows.Count > 0)
            {
                gvtemp.UseAccessibleHeader = true;
                gvtemp.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
        protected void ddlmGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindingType();
        }
        protected void LbAllergicSubmit_Click(object sender, EventArgs e)
        {
            if (txtsearch.Text == "" || txtsearchdetail.Text == "")
            {
                PopupMessage.Show("", "กรุณากรอกข้อมูลรหัสมาชิกให้ถูกต้อง");
            }
            else
            {
                if (_withdrawmed.SetAllergicRecord(txtsearch.Text, txtAllergic.Text))
                {
                    //PopupMessage.Show("", "บันทึกข้อมูลเรียบร้อย");
                    PopupMessage.ShowRespon("", "บันทึกข้อมูลเรียบร้อย", HttpContext.Current.Request.RawUrl);

                }
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            ddlmGroup.SelectedValue = "all";
            ddlmType.SelectedValue = "all";
            ckremain.Checked = true;
            BindingDrugList();
        } 
    }
}