﻿<%@ Page Title="" MaintainScrollPositionOnPostback="true" Language="C#" MasterPageFile="~/admin/Admin.Master" AutoEventWireup="true" CodeBehind="medicineWithdrawManagement.aspx.cs" Inherits="NHSO.IHR.MED.admin.medicineWithdrawManagement" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Usercontrols/SearchEmployees.ascx" TagPrefix="uc1" TagName="SearchEmployees" %>
<%@ Register Src="~/Usercontrols/PopupMessage.ascx" TagPrefix="uc1" TagName="PopupMessage" %>
<%@ Register Src="~/Usercontrols/ucTextDateValidate.ascx" TagPrefix="uc1" TagName="ucTextDateValidate" %>
<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1">
    <script type="text/javascript">
        $(document).ready(function () {
            var pressed = false;
            var chars = [];
            $(window).keypress(function (e) {
                if (e.which >= 48 && e.which <= 57) {
                    chars.push(String.fromCharCode(e.which));
                }
                if (pressed == false) {
                    setTimeout(function () {
                        if (chars.length >= 10) {
                            var barcode = chars.join("");
                            // assign value to some input (or do whatever you want)
                            $("#<%= txtmname.ClientID %>").val(barcode);
                            //$("#<%= lblistsearch.ClientID %>").focus();
                            $('#<%= Button1.ClientID %>').click();

                        }
                        chars = [];
                        pressed = false;
                    }, 500);
                }
                pressed = true;
            });
        });
        $("#<%= txtmname.ClientID %>").keypress(function (e) {
            if (e.which === 13) {
                e.preventDefault();
            }
        });

        //---- Add active Class-----//
        document.getElementById("Withdraw").className += " li-active active";
        document.getElementById("iWithdraw").className += " active";

    </script>
    <asp:Button ID="Button1" Style="display: none;" runat="server" Text="Button" OnClick="Button1_Click" />
    <ul class="main pull-left">

        <li class="dropdown">
            <a href="medicineWithdrawManagement.aspx"><i class="fa fa-fw fa-home"></i>หน้าหลัก </a>

        </li>

        <li class="dropdown">
            <i class="fa fa-fw fa-check-square-o"></i>รายการเบิกยา
			
        </li>

    </ul>
</asp:Content>


<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder2">
    <table border="1" style="border:1px solid #fff;font-weight:bold">
        <tr>
            <td colspan="12" style="text-align:center;background-color:#B0E2FF"></td>
        </tr>
    </table>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:SearchEmployees runat="server" Id="SearchEmployees" />
            <uc1:PopupMessage runat="server" ID="PopupMessage" />
            <asp:Panel ID="Panel3" runat="server" ScrollBars="Vertical">
                <div class="row row-app">
                    <div class="col-sm-12">
                        <div class="col-separator col-unscrollable box">
                            <div class="col-table">
                                <h4 class="margin-none innerAll border-bottom">รายงานการเบิกยา</h4>
                                <div class="col-table-row">
                                    <div class="col-app col-unscrollable">
                                        <div class="col-app">
                                            <div class="innerAll bg-white">
                                                <asp:Panel ID="Panel1" BorderWidth="1" BorderColor="#46A0C4" runat="server">
                                                    <fieldset>
                                                        <div>
                                                            <asp:Panel ID="Panel4" DefaultButton="LinkButton2" runat="server">
                                                                <table>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon bg-purple">วันที่&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                <uc1:ucTextDateValidate runat="server" ID="ucTextDateValidate"/>
                                                                            </div>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon bg-purple">รหัส/ชื่อ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                <asp:TextBox ID="txtsearch" Width="120px" CssClass="form-control" runat="server"></asp:TextBox>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtsearchdetail" Width="228px" runat="server" Enabled="False" CssClass="form-control"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <asp:LinkButton ID="LinkButton2" runat="server" CssClass=" pull-right btn btn-success" OnClick="LinkButton2_Click">ค้นหา</asp:LinkButton>
                                                                        </td>
                                                                        <td>
                                                                            <asp:LinkButton ID="LinkButton3" runat="server" CssClass=" pull-right btn btn-default" OnClick="LinkButton3_Click">ยกเลิก</asp:LinkButton>
                                                                        </td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon bg-purple">อาการแพ้ยา</span>
                                                                                <asp:TextBox ID="txtAllergic" Width="350" CssClass="form-control" runat="server"></asp:TextBox>
                                                                            </div>

                                                                        </td>
                                                                        <td>
                                                                            <asp:LinkButton ID="LbAllergicSubmit" runat="server" CssClass=" pull-right btn btn-success" OnClick="LbAllergicSubmit_Click">บันทึก</asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                            </asp:Panel>
                                                        </div>

                                                        <div>
                                                            <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </fieldset>
                                                    <asp:GridView ID="gvtemp" runat="server" Height="0" AutoGenerateColumns="False" CssClass="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs"
                                                        OnRowDataBound="gvtemp_RowDataBound" OnRowCancelingEdit="gvtemp_RowCancelingEdit"
                                                        OnRowEditing="gvtemp_RowEditing" OnRowUpdating="gvtemp_RowUpdating" DataKeyNames="DRUGGROUP_NAME,DRUGLIST_INDEX" OnRowCommand="gvtemp_RowCommand" Width="100%" OnPreRender="gvtemp_PreRender">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="รหัสยา" SortExpression="DRUGLIST_INDEX">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("DRUGLIST_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ชื่อเวชภัณฑ์ยา" SortExpression="DRUGLIST_INDEX" HeaderStyle-Width="20%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("DRUGLIST_NAME") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="center" />
                                                                <ItemStyle CssClass="left" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="จำนวน">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtamont" Width="80px" Text='<%#Eval("STOCK_OUTPUT") %>' runat="server"></asp:TextBox>
                                                                    <asp:MaskedEditExtender ID="txtamont_MaskedEditExtender" runat="server" AutoComplete="False" Century="2000" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" Mask="999" PromptCharacter="" TargetControlID="txtamont">
                                                                    </asp:MaskedEditExtender>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblamont" runat="server" Text='<%#Eval("STOCK_OUTPUT") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="center" />
                                                                <ItemStyle CssClass="right" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="หน่วยนับ" SortExpression="UNITLIST_NAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("UNITLIST_NAME") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="center" />
                                                                <ItemStyle CssClass="left" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ระบุอาการ" HeaderStyle-Width="50%">
                                                                <EditItemTemplate>
                                                                    <asp:DropDownList ID="ddlbehaviour" runat="server">
                                                                    </asp:DropDownList>
                                                                    <%--<asp:RequiredFieldValidator ID="rfvbehaviour" ValidationGroup="1" runat="server" ControlToValidate="ddlbehaviour" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                                                                    <asp:RequiredFieldValidator ValidationGroup="editGroupValidation" ID="rfvbehaviour" ControlToValidate="ddlbehaviour" InitialValue="" runat="server" ErrorMessage="กรุณาเลือกข้อมูล" CssClass="btn btn-danger btn-sm fa fa-arrow-circle-down"></asp:RequiredFieldValidator>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbbehavior" runat="server" Text='<%# Bind("SYMTOM_NAME") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <EditItemTemplate>
                                                                    <asp:LinkButton ID="lbUpdate" CommandName="Update" runat="server" ValidationGroup="editGroupValidation" CssClass="btn-xs btn btn-primary"><i class="fa fa-fw fa-check"></i>ยืนยัน</asp:LinkButton>
                                                                    <asp:LinkButton ID="lbCancle" CommandName="Cancel" runat="server"  CssClass="btn-xs btn btn-danger"><i class="fa fa-fw fa-times"></i>ยกเลิก</asp:LinkButton>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbEdit" CommandName="Edit" runat="server" CssClass="btn-xs btn btn-inverse"><i class="fa fa-fw fa-edit"></i>แก้ไข</asp:LinkButton>
                                                                    <asp:LinkButton ID="lbDel" CommandName="CmdDelete" CommandArgument='<%# Bind("DRUGLIST_ID") %>'
                                                                        OnClientClick="return confirm('ยืนยันการลบ');" runat="server" CssClass="btn-xs btn btn-default"><i class="fa fa-fw fa-minus"></i>ลบ</asp:LinkButton>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="center" />
                                                                <ItemStyle CssClass="center" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    <div class="row innerT">
                                                        <div class="col-md-12">
                                                                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="pull-right btn btn-success" OnClick="LinkButton1_Click" ValidationGroup="editGroupValidation">บันทึก</asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                                <h4 class="margin-none innerAll border-bottom">รายงานการค้นหาเวชภัณฑ์ยา</h4>

                                                <asp:Panel ID="Panel2" BorderWidth="1" BorderColor="#46A0C4" DefaultButton="lblistsearch" runat="server">
                                                    <fieldset>
                                                        <legend>ค้นหา :
                            <asp:CheckBox ID="ckremain" runat="server" Text="แสดงเฉพาะเวชภัณฑ์ที่มีคงเหลือ" Checked="True" /></legend>
                                                        <div class="row innerT">
                                                            <div class="col-md-12">
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-purple">โรคตามระบบ&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                        <asp:DropDownList ID="ddlmGroup" runat="server" data-style="btn-default" CssClass="form-control selectpicker col-md-6" AutoPostBack="True" OnSelectedIndexChanged="ddlmGroup_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row innerT">
                                                            <div class="col-md-12">
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-purple">สรรพคุณ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                        <asp:DropDownList ID="ddlmType" runat="server" data-style="btn-default" CssClass="form-control selectpicker col-md-6">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row innerT">
                                                            <div class="col-md-12">
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-purple">ชื่อ หรือบาร์โค๊ด</span>
                                                                        <asp:TextBox ID="txtmname"  runat="server" CssClass="form-control  col-md-6"></asp:TextBox></td>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row innerT">
                                                            <div class="col-md-12">
                                                                <div class="col-md-4">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon bg-purple">จำนวน&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                        <asp:TextBox ID="txtamount" runat="server" CssClass="form-control ">1</asp:TextBox>
                                                                        <asp:MaskedEditExtender ID="txtamount_MaskedEditExtender" runat="server" Century="2000" 
                                                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                                                            CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" 
                                                                            CultureTimePlaceholder="" Enabled="True" Mask="999999" 
                                                                            TargetControlID="txtamount" AutoComplete="False" 
                                                                            ClearMaskOnLostFocus="False" PromptCharacter="">
                                                                        </asp:MaskedEditExtender>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row innerTB">
                                                            <div class="col-md-12">
                                                                <div class="btn-group col-md-4">
                                                                    <asp:LinkButton ID="lblistsearch" runat="server" CssClass="btn btn-success col-md-2" OnClick="lblistsearch_Click">ค้นหา</asp:LinkButton>
                                                                </div>
                                                            </div>
                                                            <!-- /input-group -->
                                                        </div>


                                                        <asp:GridView ID="gvdruglist" runat="server" Height="0" AutoGenerateColumns="False" CssClass="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs"
                                                            AllowPaging="True" PageSize="100" OnRowCommand="gvdruglist_RowCommand" DataKeyNames="DRUGGROUP_ID,DRUGLIST_INDEX" OnPageIndexChanging="gvdruglist_PageIndexChanging" Width="100%" OnPreRender="gvdruglist_PreRender" EmptyDataText="ไม่พบข้อมูลยา">
                                                            <Columns>
                                                                <asp:BoundField DataField="NUM" Visible="false" HeaderText="ลำดับ" SortExpression="NUM" NullDisplayText="-">
                                                                    <HeaderStyle CssClass="center" />
                                                                    <ItemStyle CssClass="center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="DRUGLIST_ID" HeaderText="รหัสยา" SortExpression="DRUGLIST_ID" NullDisplayText="-">
                                                                    <HeaderStyle CssClass="center" />
                                                                    <ItemStyle CssClass="left" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="DRUGLIST_NAME" HeaderText="ชื่อยา" SortExpression="DRUGLIST_NAME" NullDisplayText="-">
                                                                    <HeaderStyle CssClass="center" />
                                                                    <ItemStyle CssClass="left" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="DRUGLIST_DESC" HeaderText="ชื่อสามัญ" SortExpression="DRUGLIST_DESC" NullDisplayText="-">
                                                                    <HeaderStyle CssClass="center" />
                                                                    <ItemStyle CssClass="left" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="DRUGGROUP_NAME" HeaderText="โรคตามระบบ" SortExpression="DRUGGROUP_NAME" NullDisplayText="-">
                                                                    <HeaderStyle CssClass="center" />
                                                                    <ItemStyle CssClass="left" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="DRUGTYPE_NAME" HeaderText="สรรพคุณ" SortExpression="DRUGTYPE_NAME" NullDisplayText="-">
                                                                    <HeaderStyle CssClass="center" />
                                                                    <ItemStyle CssClass="left" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="STOCK_BALANCE" HeaderText="คงเหลือ" SortExpression="STOCK_BALANCE" NullDisplayText="-">
                                                                    <HeaderStyle CssClass="center" />
                                                                    <ItemStyle CssClass="right" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="UNITLIST_NAME" HeaderText="หน่วยนับ" SortExpression="UNITLIST_NAME" NullDisplayText="-">
                                                                    <HeaderStyle CssClass="center" />
                                                                    <ItemStyle CssClass="left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="DRUGGROUP_ID" HeaderText="groupid" Visible="false" SortExpression="DRUGGROUP_ID" NullDisplayText="-"></asp:BoundField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btnAdd" runat="server" CommandName="cmdadd" CommandArgument='<%#Eval("DRUGLIST_ID")%>' CssClass="btn-xs pull-right btn btn-primary"><i class="fa fa-fw fa-plus"></i>เบิก</asp:LinkButton></td>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="center" />
                                                                    <ItemStyle CssClass="center" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <EmptyDataRowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:GridView>
                                                    </fieldset>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlmGroup" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>




