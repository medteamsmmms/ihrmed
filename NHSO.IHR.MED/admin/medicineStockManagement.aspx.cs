﻿using NHSO.IHR.MED.DataController;
using NHSO.IHR.MED.DataController.DataControllerModel;
using NHSO.IHR.MED.Entity;
using NHSO.IHR.MED.Entity.DataController;
using NHSO.IHR.MED.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;

namespace NHSO.IHR.MED.admin
{
    public partial class medicineStockManagement : Page
    {

        MedDrugStockCtr medCtr;
        public virtual string NullDisplayText { get; set; }
        private string editFunc = "cmdEdit";
        private string addFunc = "cmdAdd";
        private string listFunc = "cmdList";
        private string delFunc = "cmdDel";
        private string viewFunc = "cmdView";
        private string func = "cmdList";
        private string errorFunc = "cmdError";


        protected void Page_Init(object sender, EventArgs e)
        {
            medCtr = new MedDrugStockCtr();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                try
                {
                    hdfFunc.Value = listFunc;
                    //hdfFunc.Value = errorFunc;
                    bindSearch();
                    showPanel();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    hdfFunc.Value = errorFunc;
                    showPanel();
                }
            }
            else
            {
                if (GridView1.Rows.Count != 0)
                {
                    GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                
            }
        }

        //Page-Data-Action

        protected void bindToListPage(List<MedDrugStockEntity> medStockList)
        {
            //bindSearch();
            bindGridView(medStockList);
        }

        protected void bindSearch()
        {
            bindDrugGroupDropdown();
            bindDrugTypeDropdown();
            bindCancelListDropdown();
        }

        public void bindDrugGroupDropdown()
        {
            MedDrugGroupCtr dgCtr = new MedDrugGroupCtr();
            dgCtr.getDropdown(ddMedDrugGroupSearch, dgCtr.DRUGGROUP_NAME, dgCtr.DRUGGROUP_ID, "-- ทั้งหมด --");
        }

        protected void bindDrugTypeDropdown()
        {
            string gValue = ddMedDrugGroupSearch.SelectedValue;
            MedDrugTypeCtr dtCtr = new MedDrugTypeCtr();
            dtCtr.getDropdown(ddMedDrugTypeSearch, dtCtr.DRUGTYPE_NAME, dtCtr.DRUGTYPE_ID, "-- ทั้งหมด --", gValue);
        }
        protected void bindCancelListDropdown()
        {
            MedDrugCancelListCtr clCtr = new MedDrugCancelListCtr();
            clCtr.getDropdown(ddCancelDrugSearch, clCtr.CANCEL_CAPTION, clCtr.CANCEL_ID, "-- ทั้งหมด --");
        }


        protected void bindGridView(List<MedDrugStockEntity> medStockList)
        {
            GridView1.DataSource = medStockList;
            GridView1.DataBind();
            if (GridView1.Rows.Count != 0)
            {
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            //txtTotalMedicines.Text = "<h5 class='innerAll margin-none bg-white strong border-bottom'>จำนวนยา:&nbsp;&nbsp;" + medCtr.medAmount + "&nbsp;แบบ</h5>";
            txtTotalMedicines.Text = "<h5 class='innerAll margin-none bg-white strong border-bottom'>จำนวนยา:&nbsp;&nbsp;" +medStockList.Count + "&nbsp;แบบ</h5>";

            txtLastUpdate.Text = "<h5 class='innerAll margin-none bg-white strong border-bottom'>ข้อมูลล่าสุดเมื่อ:&nbsp;&nbsp;" + medCtr.lastUpdate + "</h5>";
       
        }

        protected void bindToeditPage()
        {
            fieldClear();

            MedDrugStockEntity medDrug = new MedDrugStockEntity();
            medDrug = medCtr.getMedStock(hdfDrugID.Value);
            lbSectionTitle.Text = "<h4 class='margin-none strong innerTB bg-primary'>แก้ไขสต๊อกยา: &nbsp;" + medDrug.DRUGLIST_NAME;
            txtBoxAmount.Text = medDrug.STOCK_BALANCE;
            txtBoxUnitList.Text = medDrug.UNITLIST_NAME;
            hdfDrugID.Value = medDrug.DRUGLIST_ID;
            hdfDrugIndex.Value = medDrug.DRUGLIST_INDEX;

            rdoIn.Checked = true;
            //Environment
            fieldFix();
            buttonForPage();
        }


        protected void bindToViewPage()
        {
            MedDrugStockEntity medDrug = new MedDrugStockEntity();
            medDrug = medCtr.getMedStock(hdfDrugID.Value);
            lbSectionTitle.Text = "<h4 class='margin-none strong innerTB bg-primary'>จำนวนคงเหลือของ: &nbsp;" + medDrug.DRUGLIST_NAME;
            txtBoxAmount.Text = medDrug.STOCK_BALANCE;
            txtBoxUnitList.Text = medDrug.UNITLIST_NAME;
            hdfDrugID.Value = medDrug.DRUGLIST_ID;
            hdfDrugIndex.Value = medDrug.DRUGLIST_INDEX;

            //Environment
            buttonForPage();
            fieldFix();
        }


        //GridView-Control
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            MedDrugStockEntity drugLabel = new MedDrugStockEntity();
            drugLabel.DRUGLIST_NAME = txtBoxSearchName.Text;
            drugLabel.DRUGGROUP_ID = ddMedDrugGroupSearch.SelectedItem.Value;
            drugLabel.DRUGTYPE_ID = ddMedDrugTypeSearch.SelectedItem.Value;
            drugLabel.FLAG = ddCancelDrugSearch.SelectedItem.Value;
            drugLabel.STOCK_MIN = txtBoxLowerNumber.Text;
            drugLabel.STOCK_MAX = txtBoxGreaterNumber.Text;
            hdfDDGroupSearch.Value = ddMedDrugGroupSearch.SelectedItem.Value;
            hdfDDTypeSearch.Value = ddMedDrugTypeSearch.SelectedItem.Value;
            hdfLowerNumber.Value = txtBoxLowerNumber.Text;
            hdfGreaterNumber.Value = txtBoxGreaterNumber.Text;


            bindToListPage(medCtr.searhFor(drugLabel));
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string value = e.CommandArgument.ToString();
            hdfFunc.Value = e.CommandName;

            if (hdfFunc.Value == editFunc)
            {
                hdfDrugID.Value = value;
                showPanel();
            }
            else if (hdfFunc.Value == viewFunc)
            {
                hdfDrugID.Value = value;
                showPanel();
            }
            else if (hdfFunc.Value == delFunc)
            {
                hdfDrugID.Value = value;
                showPanel();
            }

        }

        //Utilities (To show panel)
        protected void showPanel()
        {

            if (hdfFunc.Value == listFunc)
            {
                hdfClear();
                MedDrugStockEntity drugLabel = new MedDrugStockEntity();
                drugLabel.DRUGLIST_NAME = txtBoxSearchName.Text;
                drugLabel.DRUGGROUP_ID = ddMedDrugGroupSearch.SelectedItem.Value;
                drugLabel.DRUGTYPE_ID = ddMedDrugTypeSearch.SelectedItem.Value;
                drugLabel.STOCK_MIN = txtBoxLowerNumber.Text;
                drugLabel.STOCK_MAX = txtBoxGreaterNumber.Text;
                drugLabel.FLAG = ddCancelDrugSearch.SelectedItem.Value;
                hdfDDGroupSearch.Value = ddMedDrugGroupSearch.SelectedItem.Value;
                hdfDDTypeSearch.Value = ddMedDrugTypeSearch.SelectedItem.Value;
                hdfLowerNumber.Value = txtBoxLowerNumber.Text;
                hdfGreaterNumber.Value = txtBoxGreaterNumber.Text;

                bindToListPage(medCtr.searhFor(drugLabel));
                MedDrugListPanel.Visible = true;
                MedDrugOptionPanel.Visible = false;
                MedDrugErrorPanel.Visible = false;
            }
            else if (hdfFunc.Value == editFunc)
            {
                hdfClear();
                bindToeditPage();
                MedDrugListPanel.Visible = false;
                MedDrugOptionPanel.Visible = true;
                MedDrugErrorPanel.Visible = false;
            }
            else if (hdfFunc.Value == errorFunc)
            {
                hdfClear();
                MedDrugListPanel.Visible = false;
                MedDrugOptionPanel.Visible = false;
                MedDrugErrorPanel.Visible = true;
            }
            else if (hdfFunc.Value == delFunc)
            {
                //bindGridView();
                MedDrugListPanel.Visible = true;
                MedDrugOptionPanel.Visible = false;
                MedDrugErrorPanel.Visible = false;
            }
            else if (hdfFunc.Value == viewFunc)
            {
                hdfClear();
                bindToViewPage();
                MedDrugListPanel.Visible = false;
                MedDrugOptionPanel.Visible = true;
                MedDrugErrorPanel.Visible = false;
            }

        }

        protected void fieldClear()
        {
            txtBoxValue.Text = "";
        }

        protected void hdfClear()
        {
            hdfDDGroupSearch.Value = "";
            hdfDDTypeSearch.Value = "";
            hdfRdoSearch.Value = "";
            hdfLowerNumber.Value = "";
            hdfGreaterNumber.Value = "";
        }

        protected void fieldFix()
        {
            if (hdfFunc.Value == viewFunc)
            {
                txtBoxAmount.Enabled = false;
                txtBoxValue.Visible = false;
                pnShowAddedValue.Visible = false;
                txtBoxUnitList.Enabled = false;
                pnShowAmountValue.Visible = false;
            }
            else
            {
                txtBoxAmount.Enabled = false;
                pnShowAddedValue.Visible = true;
                pnShowAmountValue.Visible = true;
                txtBoxValue.Visible = true;
                txtBoxUnitList.Enabled = false;
            }
        }

        protected void buttonForPage()
        {
            if (hdfFunc.Value == viewFunc)
            {
                pnButtonViewPage.Visible = true;
                pnButtonEditPage.Visible = false;
            }
            else
            {
                pnButtonViewPage.Visible = false;
                pnButtonEditPage.Visible = true;
            }
        }



        //Navigation 
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = addFunc;
            showPanel();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            fieldClear();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int successful = 0;
            MedDrugStockEntity m1 = new MedDrugStockEntity();
            int balanceMed = Convert.ToInt32(txtBoxAmount.Text);

            if (hdfFunc.Value == editFunc)
            {

                m1.DRUGLIST_ID = hdfDrugID.Value;
                m1.DRUGLIST_INDEX = hdfDrugIndex.Value;
                if (rdoIn.Checked)
                {
                    m1.STOCK_INPUT = txtBoxValue.Text;
                    m1.STOCK_OUTPUT = "0";
                    balanceMed = balanceMed + Convert.ToInt32(m1.STOCK_INPUT);
                }
                else if (rdoOut.Checked)
                {
                    m1.STOCK_INPUT = "0";
                    m1.STOCK_OUTPUT = txtBoxValue.Text;
                    balanceMed = balanceMed - Convert.ToInt32(m1.STOCK_OUTPUT);
                }
                m1.STOCK_BALANCE = "" + balanceMed;
                m1.CREATE_BY = "0";
                m1.UPDATE_BY = "0";
                successful = medCtr.insertData(m1);
            }
            else if (hdfFunc.Value == viewFunc)
            {
                successful = 1;
            }
            //Insert

            if (successful == 1)
            {
                hdfFunc.Value = listFunc;
                showPanel();
            }
        }

        protected void btnDiscard_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = listFunc;
            showPanel();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            MedDrugStockEntity drugLabel = new MedDrugStockEntity();
            drugLabel.DRUGLIST_NAME = txtBoxSearchName.Text;
            drugLabel.DRUGGROUP_ID = ddMedDrugGroupSearch.SelectedItem.Value;
            drugLabel.DRUGTYPE_ID = ddMedDrugTypeSearch.SelectedItem.Value;
            drugLabel.STOCK_MIN = txtBoxLowerNumber.Text;
            drugLabel.STOCK_MAX = txtBoxGreaterNumber.Text;
            drugLabel.FLAG = ddCancelDrugSearch.SelectedItem.Value;
            hdfDDGroupSearch.Value = ddMedDrugGroupSearch.SelectedItem.Value;
            hdfDDTypeSearch.Value = ddMedDrugTypeSearch.SelectedItem.Value;
            hdfLowerNumber.Value = txtBoxLowerNumber.Text;
            hdfGreaterNumber.Value = txtBoxGreaterNumber.Text;           
            bindToListPage(medCtr.searhFor(drugLabel));
        }

        protected void btnClearSearch_Click(object sender, EventArgs e)
        {
            //txtBoxGreaterNumber.Text = "";
            //txtBoxLowerNumber.Text = "";
            //txtBoxSearchName.Text = "";
            //ddMedDrugGroupSearch.SelectedIndex = 0;
            //ddMedDrugTypeSearch.SelectedIndex = 0;
            //ddCancelDrugSearch.SelectedIndex = 0;
            //hdfRdoSearch.Value = "true";
            //hdfDDGroupSearch.Value = "";
            //hdfDDTypeSearch.Value = "";
            Response.Redirect("medicineStockManagement.aspx");
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = listFunc;
            showPanel();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = editFunc;
            showPanel();
        }

        protected void ddMedDrugGroupSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindDrugTypeDropdown();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ddMedDrugGroupSearch.SelectedIndex = 0;
            ddMedDrugTypeSearch.SelectedIndex = 0;
            showPanel();
        }

        protected void lbToExcel_Click(object sender, EventArgs e)
        {
            //MedDrugStockEntity drugLabel = new MedDrugStockEntity();
            //drugLabel.DRUGLIST_NAME = txtBoxSearchName.Text;
            //drugLabel.DRUGGROUP_ID = ddMedDrugGroupSearch.SelectedItem.Value;
            //drugLabel.DRUGTYPE_ID = ddMedDrugTypeSearch.SelectedItem.Value;
            //drugLabel.STOCK_MIN = txtBoxLowerNumber.Text;
            //drugLabel.STOCK_MAX = txtBoxGreaterNumber.Text;
            //drugLabel.FLAG = ddCancelDrugSearch.SelectedItem.Value;
            //hdfDDGroupSearch.Value = ddMedDrugGroupSearch.SelectedItem.Value;
            //hdfDDTypeSearch.Value = ddMedDrugTypeSearch.SelectedItem.Value;
            //hdfLowerNumber.Value = txtBoxLowerNumber.Text;
            //hdfGreaterNumber.Value = txtBoxGreaterNumber.Text;
            //System.Data.DataSet ds = medCtr.searhFords(drugLabel);
            //string reportpath = Server.MapPath("../Reports/rptDrugStock.rpt");
            //PrintRpt.ExportExcel(ds, reportpath, this.Page);
            
            List<int> HideColumns = new List<int>();
            HideColumns.Add(7);
            PrintExcle.PrintLabel("rptDrugStock.xls", "<h4 style='width:700px;'>รายงานสต๊อก</h4>", this.Page, GridView1, HideColumns);

        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
    }
    
}