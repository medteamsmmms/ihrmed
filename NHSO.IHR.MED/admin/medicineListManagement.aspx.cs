﻿using NHSO.IHR.MED.DataController;
using NHSO.IHR.MED.DataController.DataControllerModel;
using NHSO.IHR.MED.Entity;
using NHSO.IHR.MED.Entity.DataController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NHSO.IHR.MED.admin
{
    public partial class medicineListManagement : Page
    {

        MedDrugListCtr medCtr;
        public virtual string NullDisplayText { get; set; }
        private string editFunc = "cmdEdit";
        private string addFunc = "cmdAdd";
        private string listFunc = "cmdList";
        private string delFunc = "cmdDel";
        private string viewFunc = "cmdView";
        private string func = "cmdList";
        private string errorFunc = "cmdError";
        

        protected void Page_Init(object sender, EventArgs e)
        {
            medCtr = new MedDrugListCtr();
        }

        protected void Page_Load(object sender, EventArgs e)        {
            
            if (!IsPostBack)
            {
                try
                {
                    hdfFunc.Value = listFunc;
                    bindSearch();
                    showPanel();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    hdfFunc.Value = errorFunc;
                    showPanel();
                }
            }
            else
            {
                if (GridView1.Rows.Count != 0)
                {
                    GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
                }             
            }
        }

        //Page-Data-Action

        protected void bindToListPage(List<MedDrugListEntity> medDrugList)
        {
            //bindSearch();
            bindGridView(medDrugList);
        }

        protected void bindSearch()
        {
            bindDrugGroupDropdown();
            bindDrugTypeDropdown();
            bindCancelListDropdown();
        }

        public void bindDrugGroupDropdown()
        {
            MedDrugGroupCtr dgCtr = new MedDrugGroupCtr();
            dgCtr.getDropdown(ddMedDrugGroupSearch, dgCtr.DRUGGROUP_NAME, dgCtr.DRUGGROUP_ID, "-- ทั้งหมด --");
        }

        protected void bindDrugTypeDropdown()
        {
            string gValue = ddMedDrugGroupSearch.SelectedValue;
            MedDrugTypeCtr dtCtr = new MedDrugTypeCtr();
            dtCtr.getDropdown(ddMedDrugTypeSearch, dtCtr.DRUGTYPE_NAME, dtCtr.DRUGTYPE_ID, "-- ทั้งหมด --", gValue);
        }
        protected void bindCancelListDropdown()
        {
            MedDrugCancelListCtr clCtr = new MedDrugCancelListCtr();
            clCtr.getDropdown(ddCancelDrugSearch, clCtr.CANCEL_CAPTION, clCtr.CANCEL_ID, "-- ทั้งหมด --");
        }

        protected void bindGridView(List<MedDrugListEntity> medDrugList)
        {
            GridView1.DataSource = medDrugList;
            GridView1.DataBind();
            if (GridView1.Rows.Count != 0)
            {
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            txtTotalMedicines.Text = "<h5 class='innerAll margin-none bg-white strong border-bottom'>จำนวนยา:&nbsp;&nbsp;" + medCtr.medAmount + "&nbsp;แบบ</h5>";
            txtLastUpdate.Text = "<h5 class='innerAll margin-none bg-white strong border-bottom'>ข้อมูลล่าสุดเมื่อ:&nbsp;&nbsp;" + medCtr.lastUpdate + "</h5>";
        }

        protected void bindToEditPage()
        {
            
            MedDrugListEntity medDrug = new MedDrugListEntity();
            medDrug = medCtr.getMedDrug(hdfDrugID.Value);
            lbSectionTitle.Text = "<h4 class='margin-none strong innerTB bg-primary'>แก้ไขรายการยา: &nbsp;" + medDrug.DRUGLIST_NAME;
            if (medDrug.BARCODE_STANDARD != null || medDrug.BARCODE_STANDARD != "")
                txtBoxBarcode.Text = medDrug.BARCODE_STANDARD;
            else
                txtBoxBarcode.Text = medDrug.BARCODE_TEMP;
            txtBoxDrugDose.Text = medDrug.DRUGLIST_DOSE;
            txtBoxMedName.Text = medDrug.DRUGLIST_NAME;
            txtBoxMedDesc.Text = medDrug.DRUGLIST_DESC;
            
            MedDrugGroupCtr mdg = new MedDrugGroupCtr();
            mdg.getDropdown(ddMedGroup, mdg.DRUGGROUP_NAME, mdg.DRUGGROUP_ID, "โรคตามระบบ...");
            ddMedGroup.SelectedIndex = ddMedGroup.Items.IndexOf(ddMedGroup.Items.FindByValue(medDrug.DRUGGROUP_ID));
            
            MedDrugTypeCtr mdt = new MedDrugTypeCtr();
            mdt.getDropdown(ddMedType, mdt.DRUGTYPE_NAME, mdt.DRUGTYPE_ID, "สรรพคุณ...");
            ddMedType.SelectedIndex = ddMedType.Items.IndexOf(ddMedType.Items.FindByValue(medDrug.DRUGTYPE_ID));
            
            MedDrugUnitTypeCtr mdut = new MedDrugUnitTypeCtr();
            mdut.getDropdown(ddMedUnitType, mdut.UNITTYPE_NAME, mdut.UNITTYPE_ID, "ขนาด...");
            ddMedUnitType.SelectedIndex = ddMedUnitType.Items.IndexOf(ddMedUnitType.Items.FindByValue(medDrug.UNITTYPE_ID));

            MedDrugUnitListCtr mdul = new MedDrugUnitListCtr();
            mdul.getDropdown(ddMedUnitList, mdul.UNITLIST_NAME, mdul.UNITLIST_ID, "หน่วยนับ...");
            ddMedUnitList.SelectedIndex = ddMedUnitList.Items.IndexOf(ddMedUnitList.Items.FindByValue(medDrug.UNITLIST_ID));
            
            MedDrugCancelListCtr mdcl = new MedDrugCancelListCtr();
            mdcl.getDropdown(ddMedCancelList, mdcl.CANCEL_CAPTION, mdcl.CANCEL_ID, "การใช้งาน...");
            if (medDrug.FLAG == null || medDrug.FLAG == "")
                medDrug.FLAG = "U";            
            ddMedCancelList.SelectedIndex = ddMedCancelList.Items.IndexOf(ddMedCancelList.Items.FindByValue(medDrug.FLAG));

            hdfDrugID.Value = medDrug.DRUGLIST_ID;

            //Environment
            fieldFix();
            buttonForPage();
        }

        protected void bindToAddPage()
        {
            lbSectionTitle.Text = "<h4 class='margin-none strong innerTB bg-primary'>เพิ่มรายการยา";
            MedDrugGroupCtr mdg = new MedDrugGroupCtr();
            mdg.getDropdown(ddMedGroup, mdg.DRUGGROUP_NAME, mdg.DRUGGROUP_ID, "โรคตามระบบ...");
            MedDrugTypeCtr mdt = new MedDrugTypeCtr();
            mdt.getDropdown(ddMedType, mdt.DRUGTYPE_NAME, mdt.DRUGTYPE_ID, "สรรพคุณ...");
            MedDrugUnitTypeCtr mdut = new MedDrugUnitTypeCtr();
            mdut.getDropdown(ddMedUnitType, mdut.UNITTYPE_NAME, mdut.UNITTYPE_ID, "ขนาด...");
            MedDrugUnitListCtr mdul = new MedDrugUnitListCtr();
            mdul.getDropdown(ddMedUnitList, mdul.UNITLIST_NAME, mdul.UNITLIST_ID, "หน่วยนับ...");
            MedDrugCancelListCtr mdcl = new MedDrugCancelListCtr();
            mdcl.getDropdown(ddMedCancelList, mdcl.CANCEL_CAPTION, mdcl.CANCEL_ID, "การใช้งาน...");      
      
            //Environment
            fieldClear();
            fieldFix();
            buttonForPage();
        }

        protected void bindToViewPage()
        {
            MedDrugListEntity medDrug = new MedDrugListEntity();
            medDrug = medCtr.getMedDrug(hdfDrugID.Value);
            lbSectionTitle.Text = "<h4 class='margin-none strong innerTB bg-primary'>รายละเอียดยา: &nbsp;" + medDrug.DRUGLIST_NAME;
            if (medDrug.BARCODE_STANDARD != null || medDrug.BARCODE_STANDARD != "")
                txtBoxBarcode.Text = medDrug.BARCODE_STANDARD;
            else
                txtBoxBarcode.Text = medDrug.BARCODE_TEMP;
            txtBoxDrugDose.Text = medDrug.DRUGLIST_DOSE;
            txtBoxMedName.Text = medDrug.DRUGLIST_NAME;
            txtBoxMedDesc.Text = medDrug.DRUGLIST_DESC;

            MedDrugGroupCtr mdg = new MedDrugGroupCtr();
            mdg.getDropdown(ddMedGroup, mdg.DRUGGROUP_NAME, mdg.DRUGGROUP_ID, "โรคตามระบบ...");
            ddMedGroup.SelectedIndex = ddMedGroup.Items.IndexOf(ddMedGroup.Items.FindByValue(medDrug.DRUGGROUP_ID));

            MedDrugTypeCtr mdt = new MedDrugTypeCtr();
            mdt.getDropdown(ddMedType, mdt.DRUGTYPE_NAME, mdt.DRUGTYPE_ID, "สรรพคุณ...");
            ddMedType.SelectedIndex = ddMedType.Items.IndexOf(ddMedType.Items.FindByValue(medDrug.DRUGTYPE_ID));

            MedDrugUnitTypeCtr mdut = new MedDrugUnitTypeCtr();
            mdut.getDropdown(ddMedUnitType, mdut.UNITTYPE_NAME, mdut.UNITTYPE_ID, "ขนาด...");
            ddMedUnitType.SelectedIndex = ddMedUnitType.Items.IndexOf(ddMedUnitType.Items.FindByValue(medDrug.UNITTYPE_ID));

            MedDrugUnitListCtr mdul = new MedDrugUnitListCtr();
            mdul.getDropdown(ddMedUnitList, mdul.UNITLIST_NAME, mdul.UNITLIST_ID, "หน่วยนับ...");
            ddMedUnitList.SelectedIndex = ddMedUnitList.Items.IndexOf(ddMedUnitList.Items.FindByValue(medDrug.UNITLIST_ID));

            MedDrugCancelListCtr mdcl = new MedDrugCancelListCtr();
            mdcl.getDropdown(ddMedCancelList, mdcl.CANCEL_CAPTION, mdcl.CANCEL_ID, "การใช้งาน...");
            if (medDrug.FLAG == null || medDrug.FLAG == "")
                medDrug.FLAG = "U";
            ddMedCancelList.SelectedIndex = ddMedCancelList.Items.IndexOf(ddMedCancelList.Items.FindByValue(medDrug.FLAG));
            hdfDrugID.Value = medDrug.DRUGLIST_ID;

            //Environment
            buttonForPage();
            fieldFix();
        }

        public void deleteRow(string id)
        {
            MedDrugListEntity m = new MedDrugListEntity();
            m.DRUGLIST_ID = id;
            int successful = 0;
            successful = medCtr.deleteDataByFlag(m);
            if (successful == 1)
            {
                hdfFunc.Value = listFunc;
                showPanel();
            }
            
        }
        
        //GridView-Control
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            MedDrugListEntity drugList = new MedDrugListEntity();
            drugList.DRUGLIST_NAME = txtBoxSearchName.Text;
            drugList.DRUGGROUP_ID = ddMedDrugGroupSearch.SelectedItem.Value;
            drugList.DRUGTYPE_ID = ddMedDrugTypeSearch.SelectedItem.Value;
            hdfDDGroupSearch.Value = ddMedDrugGroupSearch.SelectedItem.Value;
            hdfDDTypeSearch.Value = ddMedDrugTypeSearch.SelectedItem.Value;
            drugList.FLAG = ddCancelDrugSearch.SelectedItem.Value;
            bindToListPage(medCtr.searhFor(drugList));
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string value = e.CommandArgument.ToString();
            hdfFunc.Value = e.CommandName;

            if (hdfFunc.Value == editFunc)
            {
                hdfDrugID.Value = value;
                showPanel();
            }
            else if (hdfFunc.Value == viewFunc)
            {
                hdfDrugID.Value = value;
                showPanel();
            }
            else if (hdfFunc.Value == delFunc)
            {
                hdfDrugID.Value = value;
                deleteRow(hdfDrugID.Value);
                showPanel();
            }

        }

        //Utilities (To show panel)
        protected void showPanel()
        {

            if (hdfFunc.Value == listFunc)
            {
                MedDrugListEntity drugList = new MedDrugListEntity();
                drugList.DRUGLIST_NAME = txtBoxSearchName.Text;
                drugList.DRUGGROUP_ID = ddMedDrugGroupSearch.SelectedItem.Value;
                drugList.DRUGTYPE_ID = ddMedDrugTypeSearch.SelectedItem.Value;
                hdfDDGroupSearch.Value = ddMedDrugGroupSearch.SelectedItem.Value;
                hdfDDTypeSearch.Value = ddMedDrugTypeSearch.SelectedItem.Value;
                drugList.FLAG = ddCancelDrugSearch.SelectedItem.Value;
                bindToListPage(medCtr.searhFor(drugList));
                MedDrugListPanel.Visible = true;
                MedDrugOptionPanel.Visible = false;
                MedDrugErrorPanel.Visible = false;
                
                
            }
            else if (hdfFunc.Value == addFunc)
            {
                
                bindToAddPage();
                hdfClear();
                MedDrugListPanel.Visible = false;
                MedDrugOptionPanel.Visible = true;
                MedDrugErrorPanel.Visible = false;
            }
            else if (hdfFunc.Value == editFunc)
            {
                hdfClear();
                bindToEditPage();
                MedDrugListPanel.Visible = false;
                MedDrugOptionPanel.Visible = true;
                MedDrugErrorPanel.Visible = false;
            }
            else if (hdfFunc.Value == viewFunc)
            {
                hdfClear();
                bindToViewPage();
                MedDrugListPanel.Visible = false;
                MedDrugOptionPanel.Visible = true;
                MedDrugErrorPanel.Visible = false;
                
            }
            else if (hdfFunc.Value == delFunc)
            {
                //bindGridView();
                MedDrugListPanel.Visible = true;
                MedDrugOptionPanel.Visible = false;
                MedDrugErrorPanel.Visible = false;
            }
            else if (hdfFunc.Value == errorFunc)
            {
                hdfClear();
                MedDrugListPanel.Visible = false;
                MedDrugOptionPanel.Visible = false;
                MedDrugErrorPanel.Visible = true;
            }

        }

        protected void fieldClear()
        {
            ddMedGroup.SelectedIndex = 0;
            ddMedType.SelectedIndex = 0;
            ddMedUnitList.SelectedIndex = 0;
            txtBoxMedName.Text = "";
            txtBoxMedDesc.Text = "";
            txtBoxBarcode.Text = "";
            txtBoxDrugDose.Text = "";
            ddMedUnitType.SelectedIndex = 0;
            ddMedCancelList.SelectedIndex = 0;
            
        }

        protected void hdfClear()
        {
            hdfDDGroupSearch.Value = "";
            hdfDDTypeSearch.Value = "";
            hdfRdoSearch.Value = "";
        }
        protected void fieldFix()
        {
            if (hdfFunc.Value == viewFunc)
            {
                ddMedGroup.Enabled = false;
                ddMedType.Enabled = false;
                ddMedUnitList.Enabled = false;
                ddMedUnitType.Enabled = false;
                ddMedCancelList.Enabled = false;
                txtBoxMedName.Enabled = false;
                txtBoxMedDesc.Enabled = false;
                txtBoxBarcode.Enabled = false;
                txtBoxDrugDose.Enabled = false;
            }
            else
            {
                ddMedGroup.Enabled = true;
                ddMedType.Enabled = true;
                ddMedUnitList.Enabled = true;
                ddMedUnitType.Enabled = true;
                ddMedCancelList.Enabled = true;
                txtBoxMedName.Enabled = true;
                txtBoxMedDesc.Enabled = true;
                txtBoxBarcode.Enabled = true;
                txtBoxDrugDose.Enabled = true;
            }
        }

        protected void buttonForPage()
        {
            if (hdfFunc.Value == viewFunc)
            {
                pnButtonViewPage.Visible = true;
                pnButtonEditPage.Visible = false;
            }
            else
            {
                pnButtonViewPage.Visible = false;
                pnButtonEditPage.Visible = true;
            }
        }
        //Navigation 
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = addFunc;
            showPanel();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            fieldClear();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int successful = 0;
            MedDrugListEntity m1 = new MedDrugListEntity();

            if (hdfFunc.Value == addFunc)
            {
                m1.DRUGLIST_ID = "";
                m1.DRUGGROUP_ID = ddMedGroup.SelectedValue;
                m1.DRUGTYPE_ID = ddMedType.SelectedValue;
                m1.UNITLIST_ID = ddMedUnitList.SelectedValue;
                m1.DRUGLIST_NAME = txtBoxMedName.Text;
                m1.DRUGLIST_DESC = txtBoxMedDesc.Text;
                m1.CREATE_BY = "0";
                m1.UPDATE_BY = "0";
                m1.BARCODE_STANDARD = txtBoxBarcode.Text;
                m1.BARCODE_TEMP = "";
                m1.DRUGLIST_DOSE = txtBoxDrugDose.Text;
                m1.UNITTYPE_ID = ddMedUnitType.SelectedValue;
                m1.FLAG = ddMedCancelList.SelectedValue;

                successful = medCtr.isBarcodeDup(m1); //Check Dup Barcode
                if (successful == 1)
                {
                    if (Convert.ToInt32(medCtr.barAmount) == 0)
                    {
                        successful = medCtr.insertData(m1);
                        rqfTxtBoxBarcode.Text = " ใส่บาร์โค๊ด";
                    }
                    else
                    {
                        successful = 0;
                        txtBoxBarcode.Text = "";
                        rqfTxtBoxBarcode.Text = " บาร์โค๊ดซ้ำ";
                        rqfTxtBoxBarcode.Validate();
                    }
                }
                
                
            }
            else if (hdfFunc.Value == editFunc)
            {
                m1.DRUGLIST_ID = hdfDrugID.Value;
                m1.DRUGGROUP_ID = ddMedGroup.SelectedValue;
                m1.DRUGTYPE_ID = ddMedType.SelectedValue;
                m1.UNITLIST_ID = ddMedUnitList.SelectedValue;
                m1.DRUGLIST_NAME = txtBoxMedName.Text;
                m1.DRUGLIST_DESC = txtBoxMedDesc.Text;
                m1.UPDATE_BY = "0";
                m1.BARCODE_STANDARD = txtBoxBarcode.Text;
                m1.BARCODE_TEMP = "";
                m1.DRUGLIST_DOSE = txtBoxDrugDose.Text;
                m1.UNITTYPE_ID = ddMedUnitType.SelectedValue;
                m1.FLAG = ddMedCancelList.SelectedValue;
                
                successful = medCtr.isBarcodeDup(m1); //Check Dup Barcode
                if (successful == 1)
                {
                    if (Convert.ToInt32(medCtr.barAmount) == 0)
                    {
                        successful = medCtr.updateData(m1);
                        rqfTxtBoxBarcode.Text = " ใส่บาร์โค๊ด";
                    }
                    else
                    {
                        successful = 0;
                        txtBoxBarcode.Text = "";
                        rqfTxtBoxBarcode.Text = " บาร์โค๊ดซ้ำ";
                        rqfTxtBoxBarcode.Validate();
                    }
                }
            }
            else if (hdfFunc.Value == viewFunc)
            {
                successful = 1;
            }
            
            
            if (successful == 1)
            {
                hdfFunc.Value = listFunc;                
                showPanel();
            }
        }

        protected void btnDiscard_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = listFunc;
            showPanel();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            MedDrugListEntity drugList = new MedDrugListEntity();
            drugList.DRUGLIST_NAME = txtBoxSearchName.Text;
            drugList.DRUGGROUP_ID = ddMedDrugGroupSearch.SelectedItem.Value;
            drugList.DRUGTYPE_ID = ddMedDrugTypeSearch.SelectedItem.Value;
            hdfDDGroupSearch.Value = ddMedDrugGroupSearch.SelectedItem.Value;
            hdfDDTypeSearch.Value = ddMedDrugTypeSearch.SelectedItem.Value;
            drugList.FLAG = ddCancelDrugSearch.SelectedItem.Value;
            bindToListPage(medCtr.searhFor(drugList));
        }

        protected void btnClearSearch_Click(object sender, EventArgs e)
        {
            //txtBoxSearchName.Text = "";
            //ddCancelDrugSearch.SelectedIndex = 0;
            //ddMedDrugTypeSearch.SelectedIndex = 0;
            //ddMedDrugGroupSearch.SelectedIndex = 0;
            //hdfDDGroupSearch.Value = "";
            //hdfDDTypeSearch.Value = "";
            //hdfRdoSearch.Value = "true";
            Response.Redirect("medicineListManagement.aspx");
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = listFunc;
            showPanel();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = editFunc;
            showPanel();
        }
        protected void ddMedDrugGroupSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindDrugTypeDropdown();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ddMedDrugGroupSearch.SelectedIndex = 0;
            ddMedDrugTypeSearch.SelectedIndex = 0;
            showPanel();
        }
    }
}