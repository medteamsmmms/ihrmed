﻿using NHSO.IHR.MED.DataController;
using NHSO.IHR.MED.Entity;
using NHSO.IHR.MED.Entity.DataController;
using NHSO.IHR.MED.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NHSO.IHR.MED.admin
{
    public partial class reportAccessHistory : Page
    {
        ReportAccessHistoryCtr medCtr;
        public virtual string NullDisplayText { get; set; }
        private string editFunc = "cmdEdit";
        private string addFunc = "cmdAdd";
        private string listFunc = "cmdList";
        private string delFunc = "cmdDel";
        private string printFunc = "cmdPrint";
        private string viewDrugBringFunc = "viewDrugBringFunc";
        private string viewEmpBringFunc = "viewEmpBringFunc";
        private string func = "cmdList";
        private string errorFunc = "cmdError";
        protected void Page_Init(object sender, EventArgs e)
        {
            medCtr = new ReportAccessHistoryCtr();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    bindSearch();
                    hdfFunc.Value = listFunc;
                    showPanel();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    hdfFunc.Value = errorFunc;
                    showPanel();
                }
            }
            else
            {
                if (GridView1.Rows.Count != 0)
                {
                    GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                //if (GridView2.Rows.Count != 0)
                //{
                //    GridView2.HeaderRow.TableSection = TableRowSection.TableHeader;
                //}
                //if (GridView3.Rows.Count != 0)
                //{
                //    GridView3.HeaderRow.TableSection = TableRowSection.TableHeader;
                //}
            }
        }
        protected void bindSearch()
        {
            ListItem i1 = new ListItem();
            ListItem i2 = new ListItem();
            ListItem i3 = new ListItem();
            ListItem i4 = new ListItem();
            ListItem i5 = new ListItem();
            i1.Text = "ชื่อพนักงาน";
            i1.Value = "1";
            i2.Text = "รหัสเบิก";
            i2.Value = "2";
            i3.Text = "เลขที่เบิก";
            i3.Value = "3";
            i4.Text = "รหัสพนักงาน";
            i4.Value = "4";
            ddSearchBy.Items.Insert(0, i4);
            ddSearchBy.Items.Insert(1, i1);
            ddSearchBy.Items.Insert(2, i2);
            ddSearchBy.Items.Insert(3, i3);
            txtSelectDateBegin1.Text = DateTime.Now.AddDays(-7).ToStringUS();//medCtr.minDate;
            txtSelectDateEnd1.Text = DateTime.Now.ToStringUS() ;
            hdfDateBegin.Value = txtSelectDateBegin1.Text;
            hdfDateEnd.Value = txtSelectDateEnd1.Text;
            medCtr.getDropdown(ddDepSearch, "EMP_BUREAU", "EMP_BUREAUID", "--ทั้งหมด--");
        }
        protected void bindToListPage(List<ReportAccessHistoryEntity> searchList)
        {
            bindGridView(GridView1, searchList);

            //Environment
            buttonForPage();
        }
        protected void bindTopEmpBox(List<ReportAccessHistoryEntity> topEmp)
        {
            string txtForLabel = "<ul class='list-group list-group-1 borders-none'>";
            int i = 1;
            foreach (ReportAccessHistoryEntity e in topEmp)
            {
                if (i == 1)
                {
                    txtForLabel += "<li class='list-group-item active'><a href=''><span class='badge badge-primary badge-primary pull-right'>" + e.AMOUNT + "</span>" + e.EMP_THNAME + "</a></li>";
                    i++;
                }
                else
                {
                    txtForLabel += "<li class='list-group-item'><a href=''><span class='badge badge-primary pull-right'>" + e.AMOUNT + "</span>" + e.EMP_THNAME + "</a></li>";
                }
            }
            txtForLabel += "</ul>";
            //lbTopEmp.Text = txtForLabel;
            //stringbuilder html_text = new stringbuilder();
            //string html_div = @"<ul class='list-group list-group-1 borders-none'>";
            //for (int i = 0; i < length; i++)
            //{
            //    html_div += "<li class='list-group-item active'><a href='{0}'><span class='badge badge-primary pull-right'>29</span>{1} contacts</a></li>";
            //    string.format(html_div, "eee", "");
            //}

            //html_div += "</ul>";
            //html_text.append(html_div);
            //html_text.appendline();
            //ltadmin.text = html_text.tostring();
            //datetime dt = new datetime(2014, 4, 10);
            //string s = "03-08-2556";
            //dt = datetime.parseexact(s, "dd-mm-yyyy", system.globalization.cultureinfo.getcultureinfo("th-th"));
            //string s1 = dt.ToString("dd/MMMM/yyyy", System.Globalization.CultureInfo.GetCultureInfo("en-US"));
            //string s2 = dt.ToString("dd/MMMM/yyyy", System.Globalization.CultureInfo.GetCultureInfo("th-TH""));
        }
        protected void bindTopMedBox(List<ReportAccessHistoryEntity> topMed)
        {
            string txtForLabel = "<ul class='list-group list-group-1 borders-none'>";
            int i = 1;
            foreach (ReportAccessHistoryEntity e in topMed)
            {
                if (i == 1)
                {
                    txtForLabel += "<li class='list-group-item active'><a href=''><span class='badge badge-primary badge-primary pull-right'>" + e.AMOUNT + "</span>" + e.DRUGLIST_NAME + "</a></li>";
                    i++;
                }
                else
                {
                    txtForLabel += "<li class='list-group-item'><a href=''><span class='badge badge-primary pull-right'>" + e.AMOUNT + "</span>" + e.DRUGLIST_NAME + "</a></li>";
                }
            }
            //lbTopMed.Text = txtForLabel;
        }
        protected void bindToViewDrugBringPage()
        {
            ReportAccessHistoryEntity r = new ReportAccessHistoryEntity();
            r.DRUGBRING_ID = hdfBringID.Value;
            List<ReportAccessHistoryEntity> rList = medCtr.getDrugBringEntity(r);
            bindGridView(GridView2, rList);
            foreach (ReportAccessHistoryEntity re in rList)
            {
                hdfEmpID.Value = re.EMP_ID;
                txtBoxSearchName.Text = re.DRUGBRING_ID;
                ddSearchBy.SelectedIndex = 3;
                break;
            }
            //Environment
            buttonForPage();
        }
        protected void bindToViewEmpBringPage()
        {
            ReportAccessHistoryEntity r = new ReportAccessHistoryEntity();
            r.EMP_ID = hdfEmpID.Value;
            List<ReportAccessHistoryEntity> rList = medCtr.getEmpBringEntity(r);
            bindGridView(GridView3, rList);
            foreach (ReportAccessHistoryEntity re in rList)
            {
                hdfEmpID.Value = re.EMP_ID;
                txtBoxSearchName.Text = re.EMP_THNAME;
                ddSearchBy.SelectedIndex = 1;
                break;
            }
            buttonForPage();
        }
        protected void bindGridView(GridView g, List<ReportAccessHistoryEntity> searchList)
        {
            g.DataSource = searchList;
            g.DataBind();
            if (g.Rows.Count != 0)
            {
                g.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            txtLastUpdate.Text = "<h5 class='innerAll margin-none bg-white strong border-bottom'>ข้อมูลล่าสุด:&nbsp;&nbsp;" + medCtr.lastUpdate + "</h5>";
            lbResult.Text = "<h5 class='innerAll margin-none bg-white strong border-bottom'>จำนวนที่แสดงผล:&nbsp;&nbsp;" + medCtr.amount + "&nbsp;รายการ</h5>";
        }
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            bindToListPage(searchForData());
        }
        protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            bindToViewDrugBringPage();
        }
        protected void GridView3_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            bindToViewEmpBringPage();
        }
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string value = e.CommandArgument.ToString();
            hdfFunc.Value = e.CommandName;

            if (hdfFunc.Value == printFunc)
            {
                hdfBringID.Value = value;
                ReportAccessHistoryEntity rep = new ReportAccessHistoryEntity();
                rep.DRUGBRING_ID = hdfBringID.Value;
                DataSet LabelPrint = medCtr.getLabelDescAll(rep);
                string reportpath = Server.MapPath("../Reports/rptDrugLabel.rpt");
                PrintRpt.PrintLabel(LabelPrint, reportpath, this.Page);
            }
            else if (hdfFunc.Value == viewDrugBringFunc)
            {
                hdfBringID.Value = value;
                showPanel();

            }
        }
        protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string value = e.CommandArgument.ToString();
            hdfFunc.Value = e.CommandName;

            if (hdfFunc.Value == printFunc)
            {
                hdfDrugID.Value = value;
                ReportAccessHistoryEntity rep = new ReportAccessHistoryEntity();
                rep.EMP_ID = hdfEmpID.Value;
                rep.DRUGBRING_ID = hdfBringID.Value;
                rep.DRUGLIST_INDEX = hdfDrugID.Value;
                DataSet LabelPrint = medCtr.getLabelDesc(rep);
                string reportpath = Server.MapPath("../Reports/rptDrugLabel.rpt");
                PrintRpt.PrintLabel(LabelPrint, reportpath, this.Page);
            }
        }
        protected void GridView3_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string value = e.CommandArgument.ToString();
            hdfFunc.Value = e.CommandName;

            if (hdfFunc.Value == printFunc)
            {
                hdfDrugID.Value = value;
                ReportAccessHistoryEntity rep = new ReportAccessHistoryEntity();
                rep.EMP_ID = hdfEmpID.Value;
                rep.DRUGBRING_ID = hdfBringID.Value;
                rep.DRUGLIST_INDEX = hdfDrugID.Value;
                DataSet LabelPrint = medCtr.getLabelDesc(rep);
                string reportpath = Server.MapPath("../Reports/rptDrugLabel.rpt");
                PrintRpt.PrintLabel(LabelPrint, reportpath, this.Page);
            }
        }
        protected void showPanel()
        {
            if (hdfFunc.Value == listFunc)
            {
                bindToListPage(searchForData());
                //bindTopEmpBox(medCtr.getListTopEmp(6));
                //bindTopMedBox(medCtr.getListTopMed(6));
                MedDrugListPanel.Visible = true;
                MedDrugErrorPanel.Visible = false;
                pnGridview1.Visible = true;
                pnGridView2.Visible = false;
                pnGridView3.Visible = false;
            }
            else if (hdfFunc.Value == errorFunc)
            {
                MedDrugListPanel.Visible = false;
                MedDrugErrorPanel.Visible = true;
            }
            else if (hdfFunc.Value == viewDrugBringFunc)
            {
                bindToViewDrugBringPage();
                MedDrugListPanel.Visible = true;
                MedDrugErrorPanel.Visible = false;
                pnGridview1.Visible = false;
                pnGridView2.Visible = true;
                pnGridView3.Visible = false;
            }
            else if (hdfFunc.Value == viewEmpBringFunc)
            {
                bindToViewEmpBringPage();
                MedDrugListPanel.Visible = true;
                MedDrugErrorPanel.Visible = false;
                pnGridview1.Visible = false;
                pnGridView2.Visible = false;
                pnGridView3.Visible = true;
            }
        }
        protected void clearSearch()
        {
            //txtBoxSearchName.Text = "";
            //ddSearchBy.SelectedIndex = 0;
            //txtSelectDateBegin1.Text = DateTime.Now.AddDays(-7).ToStringUS();//medCtr.minDate;
            //txtSelectDateEnd1.Text = DateTime.Now.ToStringUS();
            Response.Redirect("reportAccessHistory.aspx");
        }
        protected void buttonForPage()
        {
            if (hdfFunc.Value == viewDrugBringFunc)
            {
                pnButtonViewDrugBringPage.Visible = true;
                pnButtonViewEmpBringPage.Visible = false;
            }
            else if (hdfFunc.Value == viewEmpBringFunc)
            {
                pnButtonViewDrugBringPage.Visible = false;
                pnButtonViewEmpBringPage.Visible = true;
            }
            else
            {
                pnButtonViewDrugBringPage.Visible = false;
                pnButtonViewEmpBringPage.Visible = false;
            }
        }
        protected List<ReportAccessHistoryEntity> searchForData()
        {
            ReportAccessHistoryEntity re = new ReportAccessHistoryEntity();
            re.DRUGLIST_NAME = txtBoxSearchName.Text;
            re.BEGIN_DATE = txtSelectDateBegin1.Text;
            re.END_DATE = txtSelectDateEnd1.Text;
            re.EMP_BUREAUID = ddDepSearch.SelectedItem.Value;
            List<ReportAccessHistoryEntity> rl = new List<ReportAccessHistoryEntity>();
            if (ddSearchBy.SelectedIndex == 0)
            {
                rl = medCtr.searchByEmpID(re);
            }
            else if (ddSearchBy.SelectedIndex == 1)
            {
                rl = medCtr.searchEmpName(re);
            }
            else if (ddSearchBy.SelectedIndex == 2)
            {
                rl = medCtr.searchByBringID(re);
            }
            else if (ddSearchBy.SelectedIndex == 3)
            {
                rl = medCtr.searchByOrderNo(re);
            }
            return rl;
        }
        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = listFunc;
            clearSearch();
            showPanel();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            showPanel();
        }
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = viewEmpBringFunc;
            showPanel();
        }
        protected void btnClearSearch_Click(object sender, EventArgs e)
        {
            clearSearch();
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton btn = (LinkButton)e.Row.FindControl("btnRowPrint");
            if (((btn != null)))
            {
                //btn.CommandArgument = e.Row.RowIndex.ToString();
                ContentPlaceHolder cph = (ContentPlaceHolder)Page.Master.FindControl("ContentPlaceHolder2");
                AjaxControlToolkit.ToolkitScriptManager sm = (AjaxControlToolkit.ToolkitScriptManager)cph.FindControl("ToolkitScriptManager1");
                sm.RegisterPostBackControl(btn);
            }
        }
    }
}