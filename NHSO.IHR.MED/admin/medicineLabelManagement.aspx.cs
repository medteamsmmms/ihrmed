﻿using NHSO.IHR.MED.DataController;
using NHSO.IHR.MED.DataController.DataControllerModel;
using NHSO.IHR.MED.Entity;
using NHSO.IHR.MED.Entity.DataController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NHSO.IHR.MED.admin
{
    public partial class medicineLabelManagement : Page
    {

        MedDrugLabelCtr medCtr;
        public virtual string NullDisplayText { get; set; }
        private string editFunc = "cmdEdit";
        private string addFunc = "cmdAdd";
        private string listFunc = "cmdList";
        private string delFunc = "cmdDel";
        private string viewFunc = "cmdView";
        private string func = "cmdList";
        private string errorFunc = "cmdError";


        protected void Page_Init(object sender, EventArgs e)
        {
            medCtr = new MedDrugLabelCtr();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                try
                {
                    hdfFunc.Value = listFunc;
                    //hdfFunc.Value = errorFunc;
                    bindDruggroupDropdown();
                    bindDrugTypeDropdown();
                    showPanel();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    hdfFunc.Value = errorFunc;
                    showPanel();
                }
            }
            else
            {
                if (GridView1.Rows.Count != 0)
                {                    
                    GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
        }


        //Page-Data-Action
        protected void bindToListPage(List<MedDrugLabelEntity> medLabelList)
        {
            //bindSearch();
            bindGridView(medLabelList);

        }

        protected void bindDruggroupDropdown()
        {
           // MedDrugGroupCtr dgCtr = new MedDrugGroupCtr();
            //dgCtr.getDropdown(ddMedDrugGroupSearch,dgCtr.DRUGGROUP_NAME,dgCtr.DRUGGROUP_ID,"-- ทั้งหมด --");
            string firstItem = "-- ทั้งหมด --";
            List<MedDrugLabelEntity> lst = new MedDrugLabelCtr().getListAll();

            ddMedDrugGroupSearch.DataSource = lst;
            ddMedDrugGroupSearch.DataTextField = "DRUGGROUP_NAME";
            ddMedDrugGroupSearch.DataValueField = "DRUGGROUP_ID";
            ddMedDrugGroupSearch.DataBind();
            if (firstItem != null || firstItem != " ")
            {
                ListItem i = new ListItem();
                i.Text = firstItem;
                i.Value = "";
                ddMedDrugGroupSearch.Items.Insert(0, i);
            }

            //if (hdfDDGroupSearch.Value != null || hdfDDGroupSearch.Value != " ")
            //{
            //    ddMedDrugGroupSearch.SelectedIndex = ddMedDrugGroupSearch.Items.IndexOf(ddMedDrugGroupSearch.Items.FindByValue(hdfDDGroupSearch.Value));
            //}
            //if (hdfDDTypeSearch.Value != null || hdfDDTypeSearch.Value != " ")
            //{
            //    ddMedDrugTypeSearch.SelectedIndex = ddMedDrugTypeSearch.Items.IndexOf(ddMedDrugTypeSearch.Items.FindByValue(hdfDDTypeSearch.Value));
            //}

        }

        protected void bindDrugTypeDropdown()
        {
            string gValue = ddMedDrugGroupSearch.SelectedValue;
            MedDrugTypeCtr dtCtr = new MedDrugTypeCtr();
            dtCtr.getDropdown(ddMedDrugTypeSearch, dtCtr.DRUGTYPE_NAME, dtCtr.DRUGTYPE_ID, "-- ทั้งหมด --", gValue);
        }

        protected void bindGridView(List<MedDrugLabelEntity> medLabelList)
        {
            GridView1.DataSource = medLabelList;
            GridView1.DataBind();
            if (GridView1.Rows.Count != 0)
            {
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            //txtTotalMedicines.Text = "จำนวนยา:  "+medCtr.medAmount + " แบบ";
            txtTotalMedicines.Text = "<h5 class='innerAll margin-none bg-white strong border-bottom'>จำนวนยา:&nbsp;&nbsp;" + medCtr.medAmount + "&nbsp;แบบ</h5>";

            txtLastUpdate.Text = "<h5 class='innerAll margin-none bg-white strong border-bottom'>ข้อมูลล่าสุดเมื่อ:&nbsp;&nbsp;" + medCtr.lastUpdate + "</h5>";
        }

        protected void bindToeditPage()
        {
            fieldClear();
            MedDrugLabelEntity medDrug = new MedDrugLabelEntity();
            medDrug = medCtr.getLabel(hdfDrugIndex.Value);
            lbSectionTitle.Text = "<h4 class='margin-none strong innerTB bg-primary'>แก้ไขฉลากยา: &nbsp;" + medDrug.DRUGLIST_NAME;
            txtBoxAmount.Text = medDrug.DRUG_USETIME;
            txtBoxLabelDesc.Text = medDrug.DRUG_USEFOR;
            txtBoxUnitList.Text = medDrug.UNITLIST_NAME;

            if (medDrug.DRUG_MEALTIME0 == "1")
            {
                ckbBeforeMeal.Checked = true;
                ckbBeforeMeal.CssClass = "btn btn-primary btn-stroke active";
            }
            else
            {
                ckbBeforeMeal.Checked = false;
                ckbBeforeMeal.CssClass = "btn btn-primary btn-stroke";
            }

            if (medDrug.DRUG_MEALTIME1 == "1")
            {
                ckbAfterMeal.Checked = true;
                ckbAfterMeal.CssClass = "btn btn-primary btn-stroke active"; 
            }
            else
            {
                ckbAfterMeal.Checked = false;
                ckbAfterMeal.CssClass = "btn btn-primary btn-stroke";
            }

            if (medDrug.DRUG_TIME0 == "1")
            {
                ckbMorning.Checked = true;
                ckbMorning.CssClass = "btn btn-primary btn-stroke active";
            }
            else
            {
                ckbMorning.Checked = false;
                ckbMorning.CssClass = "btn btn-primary btn-stroke";
            }

            if (medDrug.DRUG_TIME1 == "1")
            {
                ckbNoon.Checked = true;
                ckbNoon.CssClass = "btn btn-primary btn-stroke active";
            }
            else
            {
                ckbNoon.Checked = false;
                ckbNoon.CssClass = "btn btn-primary btn-stroke";
            }

            if (medDrug.DRUG_TIME2 == "1")
            {
                ckbEve.Checked = true;
                ckbEve.CssClass = "btn btn-primary btn-stroke active";
            }
            else
            {
                ckbEve.Checked = false;
                ckbEve.CssClass = "btn btn-primary btn-stroke";
            }

            if (medDrug.DRUG_TIME3 == "1")
            {
                ckbSlept.Checked = true;
                ckbSlept.CssClass = "btn btn-primary btn-stroke active";
            }
            else
            {
                ckbSlept.Checked = false;
                ckbSlept.CssClass = "btn btn-primary btn-stroke";
            }


            hdfDrugUseID.Value = medDrug.DRUGUSE_ID;
            hdfDrugIndex.Value = medDrug.DRUGLIST_INDEX;

            //Environment
            fieldFix();
            buttonForPage();
        }


        protected void bindToViewPage()
        {
            MedDrugLabelEntity medDrug = new MedDrugLabelEntity();
            medDrug = medCtr.getLabel(hdfDrugIndex.Value);
            lbSectionTitle.Text = "<h4 class='margin-none strong innerTB bg-primary'>แก้ไขฉลากยา: &nbsp;" + medDrug.DRUGLIST_NAME;
            txtBoxAmount.Text = medDrug.DRUG_USETIME;
            txtBoxLabelDesc.Text = medDrug.DRUG_USEFOR;
            txtBoxBeforeAfterMeal.Text = medDrug.MEALTIME_DESC;
            txtBoxTimeToUse.Text = medDrug.DRUG_TIME_DESC;
            hdfDrugUseID.Value = medDrug.DRUGUSE_ID;
            hdfDrugIndex.Value = medDrug.DRUGLIST_INDEX;

            //Environment
            fieldFix();
            buttonForPage();
        }


        //GridView-Control
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            MedDrugLabelEntity drugLabel = new MedDrugLabelEntity();
            drugLabel.DRUGLIST_NAME = txtBoxSearchName.Text;
            drugLabel.DRUGGROUP_ID = ddMedDrugGroupSearch.SelectedItem.Value;
            drugLabel.DRUGTYPE_ID = ddMedDrugTypeSearch.SelectedItem.Value;
            bindToListPage(medCtr.searhFor(drugLabel)); 
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string value = e.CommandArgument.ToString();
            hdfFunc.Value = e.CommandName;

            if (hdfFunc.Value == editFunc)
            {
                hdfDrugIndex.Value = value;
                showPanel();
            }
            else if (hdfFunc.Value == viewFunc)
            {
                hdfDrugIndex.Value = value;
                showPanel();
            }
            else if (hdfFunc.Value == delFunc)
            {
                hdfDrugIndex.Value = value;
                showPanel();
            }

        }

        //Utilities (To show panel)
        protected void showPanel()
        {

            if (hdfFunc.Value == listFunc)
            {
                MedDrugLabelEntity drugLabel = new MedDrugLabelEntity();
                drugLabel.DRUGLIST_NAME = txtBoxSearchName.Text;
                drugLabel.DRUGGROUP_ID = ddMedDrugGroupSearch.SelectedItem.Value;
                drugLabel.DRUGTYPE_ID = ddMedDrugTypeSearch.SelectedItem.Value;
                bindToListPage(medCtr.searhFor(drugLabel)); 
                MedDrugListPanel.Visible = true;
                MedDrugOptionPanel.Visible = false;
                MedDrugErrorPanel.Visible = false;
            }
            else if (hdfFunc.Value == editFunc)
            {
                hdfClear();
                bindToeditPage();
                MedDrugListPanel.Visible = false;
                MedDrugOptionPanel.Visible = true;
                MedDrugErrorPanel.Visible = false;
            }
            else if (hdfFunc.Value == errorFunc)
            {
                hdfClear();
                MedDrugListPanel.Visible = false;
                MedDrugOptionPanel.Visible = false;
                MedDrugErrorPanel.Visible = true;
            }
            else if (hdfFunc.Value == delFunc)
            {
                //bindGridView();
                MedDrugListPanel.Visible = true;
                MedDrugOptionPanel.Visible = false;
                MedDrugErrorPanel.Visible = false;
            }
            else if (hdfFunc.Value == viewFunc)
            {
                hdfClear();
                bindToViewPage();
                MedDrugListPanel.Visible = false;
                MedDrugOptionPanel.Visible = true;
                MedDrugErrorPanel.Visible = false;
            }

        }

        protected void fieldClear()
        {
            txtBoxLabelDesc.Text = "";
            txtBoxAmount.Text = "";
                ckbBeforeMeal.Checked = false;
                ckbBeforeMeal.CssClass = "btn btn-primary btn-stroke";
                ckbAfterMeal.Checked = false;
                ckbAfterMeal.CssClass = "btn btn-primary btn-stroke";           
                ckbMorning.Checked = false;
                ckbMorning.CssClass = "btn btn-primary btn-stroke";
            ckbNoon.Checked = false;
                ckbNoon.CssClass = "btn btn-primary btn-stroke";
            ckbEve.Checked = false;
                ckbEve.CssClass = "btn btn-primary btn-stroke";
                ckbSlept.Checked = false;
                ckbSlept.CssClass = "btn btn-primary btn-stroke";
            
        }

        protected void fieldFix()
        {
            if (hdfFunc.Value == viewFunc)
            {
                pnCkbBeforeAfterMeal.Visible = false;
                pntxtBoxBeforeAfterMeal.Visible = true;
                pnCkbTimeToUse.Visible = false;
                pnTxtBoxTimeToUse.Visible = true;
                txtBoxUnitList.Enabled = false;
                txtBoxLabelDesc.Enabled = false;
                txtBoxAmount.Enabled = false;
                ckbAfterMeal.Enabled = false;
                ckbBeforeMeal.Enabled = false;
                txtBoxBeforeAfterMeal.Enabled = false;
                txtBoxTimeToUse.Enabled = false;
                ckbEve.Enabled = false;
                ckbMorning.Enabled = false;
                ckbSlept.Enabled = false;
                ckbNoon.Enabled = false;
                
            }
            else
            {
                pnCkbBeforeAfterMeal.Visible = true;
                pntxtBoxBeforeAfterMeal.Visible = false;
                pnCkbTimeToUse.Visible = true;
                pnTxtBoxTimeToUse.Visible = false;
                txtBoxLabelDesc.Enabled = true;
                txtBoxBeforeAfterMeal.Enabled = true;
                txtBoxTimeToUse.Enabled = true;
                txtBoxUnitList.Enabled = false;
                txtBoxAmount.Enabled = true;
                ckbAfterMeal.Enabled = true;
                ckbBeforeMeal.Enabled = true;
                ckbEve.Enabled = true;
                ckbMorning.Enabled = true;
                ckbSlept.Enabled = true;
                ckbNoon.Enabled = true;
            }
        }

        protected void hdfClear()
        {
            hdfDDGroupSearch.Value = "";
            hdfDDTypeSearch.Value = "";
        }

        protected void buttonForPage()
        {
            if (hdfFunc.Value == viewFunc)
            {
                pnButtonViewPage.Visible = true;
                pnButtonEditPage.Visible = false;
            }
            else
            {
                pnButtonViewPage.Visible = false;
                pnButtonEditPage.Visible = true;
            }
        }

        //Navigation 
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = addFunc;
            showPanel();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            fieldClear();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int successful = 0;
            MedDrugLabelEntity m1 = new MedDrugLabelEntity();

            if (hdfFunc.Value == editFunc)
            {

                m1.DRUGLIST_INDEX = hdfDrugIndex.Value;
                m1.DRUG_USETIME = txtBoxAmount.Text;
                if (ckbBeforeMeal.Checked)
                {
                    m1.DRUG_MEALTIME0 = "1";
                }
                else
                {
                    m1.DRUG_MEALTIME0 = "0";
                }

                if (ckbAfterMeal.Checked)
                {
                    m1.DRUG_MEALTIME1 = "1";
                }
                else
                {
                    m1.DRUG_MEALTIME1 = "0";
                }

                if (ckbMorning.Checked)
                {
                    m1.DRUG_TIME0 = "1";
                }
                else
                {
                    m1.DRUG_TIME0 = "0";
                }
                if (ckbNoon.Checked)
                {
                    m1.DRUG_TIME1 = "1";
                }
                else
                {
                    m1.DRUG_TIME1 = "0";
                }
                if (ckbEve.Checked)
                {
                    m1.DRUG_TIME2 = "1";
                }
                else
                {
                    m1.DRUG_TIME2 = "0";
                }
                if (ckbSlept.Checked)
                {
                    m1.DRUG_TIME3 = "1";
                }
                else
                {
                    m1.DRUG_TIME3 = "0";
                }
                m1.DRUG_USEFOR = txtBoxLabelDesc.Text;
                m1.CREATE_BY = "0";
                m1.UPDATE_BY = "0";

                if (medCtr.isDuplicated(hdfDrugIndex.Value))
                {
                    successful = medCtr.updateData(m1);
                }
                else
                {
                    successful = medCtr.insertData(m1);
                }
                
            }
            else if (hdfFunc.Value == viewFunc)
            {
                successful = 1;
            }
            //Insert

            if (successful == 1)
            {
                hdfFunc.Value = listFunc;
                showPanel();
            }
        }

        protected void btnDiscard_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = listFunc;
            showPanel();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            MedDrugLabelEntity drugLabel = new MedDrugLabelEntity();
            drugLabel.DRUGLIST_NAME = txtBoxSearchName.Text;
            drugLabel.DRUGGROUP_ID = ddMedDrugGroupSearch.SelectedItem.Value;
            drugLabel.DRUGTYPE_ID = ddMedDrugTypeSearch.SelectedItem.Value;
            bindToListPage(medCtr.searhFor(drugLabel)); 
        }

        protected void btnClearSearch_Click(object sender, EventArgs e)
        {
            //txtBoxSearchName.Text = "";
            //ddMedDrugGroupSearch.SelectedIndex = 0;
            //ddMedDrugTypeSearch.SelectedIndex = 0;
            //hdfDDGroupSearch.Value = "";
            //hdfDDTypeSearch.Value = "";
            Response.Redirect("medicineLabelManagement.aspx");
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = listFunc;
            showPanel();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = editFunc;
            showPanel();
        }

        protected void ddMedDrugGroupSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindDrugTypeDropdown();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ddMedDrugGroupSearch.SelectedIndex = 0;
            ddMedDrugTypeSearch.SelectedIndex = 0;
            showPanel();
        }
    }
}