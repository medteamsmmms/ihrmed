﻿using NHSO.IHR.MED.DataController;
using NHSO.IHR.MED.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NHSO.IHR.MED.admin
{
    public partial class medicineUnitListManagement : Page
    {

        MedDrugUnitListCtr medCtr;
        public virtual string NullDisplayText { get; set; }
        private string editFunc = "cmdEdit";
        private string addFunc = "cmdAdd";
        private string listFunc = "cmdList";
        private string delFunc = "cmdDel";
        private string viewFunc = "cmdView";
        private string func = "cmdList";
        private string errorFunc = "cmdError";


        protected void Page_Init(object sender, EventArgs e)
        {
            medCtr = new MedDrugUnitListCtr();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                try
                {
                    hdfFunc.Value = listFunc;
                    showPanel();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    hdfFunc.Value = errorFunc;
                    showPanel();
                }
            }
        }

        //Page-Data-Action
        protected void bindGridView()
        {
            GridView1.DataSource = medCtr.getList();
            GridView1.DataBind();
            if (GridView1.Rows.Count > 0)
            {
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            }   
            txtTotalMedicines.Text = "<h5 class='innerAll margin-none bg-white strong border-bottom'>จำนวนหน่วยนับ:&nbsp;&nbsp;" + medCtr.medAmount + "&nbsp;แบบ</h5>";
            txtLastUpdate.Text = "<h5 class='innerAll margin-none bg-white strong border-bottom'>ข้อมูลล่าสุดเมื่อ:&nbsp;&nbsp;" + medCtr.lastUpdate + "</h5>";
        }

        protected void bindToeditPage()
        {
            MedDrugUnitListEntity medDrug = new MedDrugUnitListEntity();
            medDrug = medCtr.getMedDrug(hdfDrugID.Value);
            lbSectionTitle.Text = "<h4 class='margin-none strong innerTB bg-primary' >แก้ไขหน่วยนับ: &nbsp;" + medDrug.UNITLIST_NAME;
            txtBoxMedName.Text = medDrug.UNITLIST_NAME;
            txtBoxMedDesc.Text = medDrug.UNITLIST_DESC;
            hdfDrugID.Value = medDrug.UNITLIST_ID;

            //Environment
            fieldFix();
            buttonForPage();
        }

        protected void bindToAddPage()
        {
            lbSectionTitle.Text = "<h4 class='margin-none strong innerTB bg-primary' >เพิ่มหน่วยนับ";

            //Environment
            fieldClear();
            fieldFix();
            buttonForPage();
        }

        protected void bindToViewPage()
        {
            MedDrugUnitListEntity medDrug = new MedDrugUnitListEntity();
            medDrug = medCtr.getMedDrug(hdfDrugID.Value);
            lbSectionTitle.Text = "<h4 class='margin-none strong innerTB bg-primary'>รายละเอียดหน่วยนับ: &nbsp;" + medDrug.UNITLIST_NAME;
            txtBoxMedName.Text = medDrug.UNITLIST_NAME;
            txtBoxMedDesc.Text = medDrug.UNITLIST_DESC;
            hdfDrugID.Value = medDrug.UNITLIST_ID;

            //Environment
            buttonForPage();
            fieldFix();
        }

        public void deleteRow(string id)
        {
            MedDrugUnitListEntity m = new MedDrugUnitListEntity();
            m.UNITLIST_ID = id;
            int successful = 0;
            successful = medCtr.canBeDeleted(m);
            if (successful == 1)
            {
                if (Convert.ToInt32(medCtr.medDeleted) == 0)
                {
                    successful = medCtr.deleteDataByFlag(m);
                    if (successful == 1)
                    {
                        hdfFunc.Value = listFunc;
                        showPanel();
                    }
                }
                else
                {
                    PopupMessage.Show("", "ไม่สามารถลบข้อมูลได้");
                }
            }

        }

        //GridView-Control
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            bindGridView();
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string value = e.CommandArgument.ToString();
            hdfFunc.Value = e.CommandName;

            if (hdfFunc.Value == editFunc)
            {
                hdfDrugID.Value = value;
                showPanel();
            }
            else if (hdfFunc.Value == viewFunc)
            {
                hdfDrugID.Value = value;
                showPanel();
            }
            else if (hdfFunc.Value == delFunc)
            {
                hdfDrugID.Value = value;
                deleteRow(hdfDrugID.Value);
                showPanel();
            }

        }

        //Utilities (To show panel)
        protected void showPanel()
        {

            if (hdfFunc.Value == listFunc)
            {
                bindGridView();
                MedDrugListPanel.Visible = true;
                MedDrugOptionPanel.Visible = false;
                MedDrugErrorPanel.Visible = false;
            }
            else if (hdfFunc.Value == addFunc)
            {
                bindToAddPage();
                MedDrugListPanel.Visible = false;
                MedDrugOptionPanel.Visible = true;
                MedDrugErrorPanel.Visible = false;
            }
            else if (hdfFunc.Value == editFunc)
            {
                bindToeditPage();
                MedDrugListPanel.Visible = false;
                MedDrugOptionPanel.Visible = true;
                MedDrugErrorPanel.Visible = false;
            }
            else if (hdfFunc.Value == errorFunc)
            {
                MedDrugListPanel.Visible = false;
                MedDrugOptionPanel.Visible = false;
                MedDrugErrorPanel.Visible = true;
            }
            else if (hdfFunc.Value == delFunc)
            {
                bindGridView();
                MedDrugListPanel.Visible = true;
                MedDrugOptionPanel.Visible = false;
                MedDrugErrorPanel.Visible = false;
            }
            else if (hdfFunc.Value == viewFunc)
            {
                bindToViewPage();
                MedDrugListPanel.Visible = false;
                MedDrugOptionPanel.Visible = true;
                MedDrugErrorPanel.Visible = false;
            }

        }

        protected void fieldClear()
        {
            txtBoxMedName.Text = "";
            txtBoxMedDesc.Text = "";
        }

        protected void fieldFix()
        {
            if (hdfFunc.Value == viewFunc)
            {
                txtBoxMedName.Enabled = false;
                txtBoxMedDesc.Enabled = false;
            }
            else
            {
                txtBoxMedName.Enabled = true;
                txtBoxMedDesc.Enabled = true;
            }
        }

        protected void buttonForPage()
        {
            if (hdfFunc.Value == viewFunc)
            {
                pnButtonViewPage.Visible = true;
                pnButtonEditPage.Visible = false;
            }
            else
            {
                pnButtonViewPage.Visible = false;
                pnButtonEditPage.Visible = true;
            }
        }

        //Navigation 
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = addFunc;
            showPanel();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            fieldClear();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int successful = 0;
            MedDrugUnitListEntity m1 = new MedDrugUnitListEntity();

            if (hdfFunc.Value == addFunc)
            {
                m1.UNITLIST_NAME = txtBoxMedName.Text;
                m1.UNITLIST_DESC = txtBoxMedDesc.Text;
                m1.CREATE_BY = "0";
                m1.UPDATE_BY = "0";
                m1.FLAG = "Y";
                successful = medCtr.insertData(m1);
            }
            else if (hdfFunc.Value == editFunc)
            {
                m1.UNITLIST_ID = hdfDrugID.Value;
                m1.UNITLIST_NAME = txtBoxMedName.Text;
                m1.UNITLIST_DESC = txtBoxMedDesc.Text;
                m1.UPDATE_BY = "0";
                m1.FLAG = "Y";
                successful = medCtr.updateData(m1);
            }
            else if (hdfFunc.Value == viewFunc)
            {
                successful = 1;
            }
            //Insert

            if (successful == 1)
            {
                hdfFunc.Value = listFunc;
                showPanel();
            }
        }

        protected void btnDiscard_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = listFunc;
            showPanel();
        }

         protected void btnConfirm_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = listFunc;
            showPanel();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = editFunc;
            showPanel();
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string Namecolumnvalue = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "COUNTER"));
                LinkButton lnk2 = (LinkButton)e.Row.FindControl("btnRowDelete");
                if (Namecolumnvalue == "0" || string.IsNullOrEmpty(Namecolumnvalue))
                {
                    lnk2.Visible = true;
                }
                else
                {
                    lnk2.Visible = false;
                }
            }
        }
    
    }
}