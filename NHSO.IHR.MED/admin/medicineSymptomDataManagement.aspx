﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Admin.Master" AutoEventWireup="true" CodeBehind="medicineSymptomDataManagement.aspx.cs" Inherits="NHSO.IHR.MED.admin.medicineSymptomDataManagement" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Usercontrols/PopupMessage.ascx" TagPrefix="uc1" TagName="PopupMessage" %>
<%@ Register Src="~/Usercontrols/ucTextDateEmpty.ascx" TagPrefix="uc1" TagName="ucTextDateEmpty" %>



<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1">
    <ul class="main pull-left">
        <li class="dropdown">
            <a href="medicineWithdrawManagement.aspx"><i class="fa fa-fw fa-home"></i>หน้าหลัก </a>
        </li>
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle dropdown-hover"><i class="fa fa-fw fa-suitcase"></i>ข้อมูลหลัก <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="medicineGroupManagement.aspx">ข้อมูลโรคตามระบบของยา</a></li>
                <li><a href="medicineSymptomDataManagement.aspx">ข้อมูลอาการ</a></li>
                <li><a href="medicineTypeManagement.aspx">ข้อมูลสรรพคุณของยา</a></li>
                <li><a href="medicineUnitListManagement.aspx">ข้อมูลหน่วยนับ</a></li>
                <li><a href="medicineUnitTypeManagement.aspx">ข้อมูลขนาดยา</a></li>
            </ul>
        </li>
    </ul>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder2">

    <script type="text/javascript">

        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {
            $("#txtSelectDateBegins").datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: "linked",
                language: "th",
                todayHighlight: true,
                autoclose: true
            });
            $("#txtSelectDateEnds").datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: "linked",
                language: "th",
                todayHighlight: true,
                autoclose: true
            });
        });

        function ClearSearch() {

            document.getElementById('<%=ddMedDrugGroupSearch.ClientID %>').selectedIndex = "0";
            document.getElementById('<%=ddMedDrugSymptom.ClientID %>').selectedIndex = "0";
            document.getElementById("ContentPlaceHolder2_ucTextDateEmpty_txtdate").value = "";
        }
        //---- Add active Class-----//
        document.getElementById("HasMainData").className += " li-active active";
        document.getElementById("SMainData").style.borderColor = "#4a8bc2";
        document.getElementById("SMainData").style.background = "#314250";
        document.getElementById("SMainData").style.color = "white";
        document.getElementById("iMainData").className += " active";
        document.getElementById("MedSymptomData").className += " active";
        document.getElementById("sidebar-main-data").className += " in";
    </script>

    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:PopupMessage runat="server" ID="PopupMessage" />
            <asp:Panel ID="MedDrugListPanel" runat="server">
                <div class="row row-app" id="MedDrugListDiv">
                    <div class="col-md-12 animated fadeInDown">
                        <div class="col-separator col-separator-first col-unscrollable">
                            <div class="col-table">
                                <div class="heading-buttons innerLR box">
                                    <h4 class="margin-none innerTB pull-left">ข้อมูลอาการ</h4>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn-xs pull-right btn btn-primary" OnClick="btnAdd_Click"><i class="fa fa-fw fa-plus"></i>เพิ่มข้อมูล</asp:LinkButton>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-separator-h"></div>
                                <div class="col-table-row">
                                    <div class="col-app col-unscrollable">
                                        <div class="col-app hasNiceScroll animated fadeInDown" tabindex="10000" style="overflow-y: hidden; outline: none;">
                                            <div class="col-table">
                                                <div class="col-separator-h box"></div>
                                                <div class="col-table-row">
                                                    <div class="row-app">
                                                        <div>
                                                            <div class="col-separator box bg-gray hasNiceScroll">

                                                                <h5 class="innerAll margin-none bg-primary strong border-bottom"><i class="fa fa-fw fa-search"></i>&nbsp;ค้นหา</h5>
                                                                <asp:Panel ID="Panel1" DefaultButton="btnSearch" runat="server">
                                                                    <div class="innerAll margin-none bg-gray strong border-bottom">

                                                                        <div class="row innerT">

                                                                            <div class="col-md-12">
                                                                                <div class="col-md-4">
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon bg-purple">โรคตามระบบ&nbsp;&nbsp;&nbsp;</span>
                                                                                        <asp:DropDownList ID="ddMedDrugGroupSearch" runat="server" data-style="btn-default" CssClass="form-control selectpicker col-md-6" OnSelectedIndexChanged="ddMedDrugGroupSearch_SelectedIndexChanged" AutoPostBack="true">
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div class="row innerT">

                                                                            <div class="col-md-12">
                                                                                <div class="col-md-4">
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon bg-purple">อาการ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                        <asp:DropDownList ID="ddMedDrugSymptom" runat="server" data-style="btn-default" CssClass="form-control selectpicker col-md-6">
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>


                                                                            </div>
                                                                        </div>
                                                                        <div class="row innerT">
                                                                            <div class="col-md-12">
                                                                                <div class="col-md-3">
                                                                                    <div class="input-group input-group date" id="txtSelectDateBegins">
                                                                                        <span class="input-group-addon bg-purple">วันเริ่มต้น&nbsp;</span>
                                                                                        <asp:TextBox ID="txtSelectDateBegin1" runat="server" class="form-control" disabled></asp:TextBox>
                                                                                        <span class="input-group-addon"><i class="fa fa-th"></i></span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3">
                                                                                    <div class="input-group input-group date" id="txtSelectDateEnds">
                                                                                        <span class="input-group-addon bg-purple">วันสิ้นสุด&nbsp;</span>
                                                                                        <asp:TextBox ID="txtSelectDateEnd1" runat="server" class="form-control" disabled></asp:TextBox>
                                                                                        <span class="input-group-addon"><i class="fa fa-th"></i></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <%--<div class="col-md-12">
                                                                                <div class="col-md-4">
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon bg-purple">วันที่สร้างข้อมูล</span>
                                                                                        <uc1:ucTextDateEmpty runat="server" ID="ucTextDateEmpty" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>--%>
                                                                            <!-- /input-group -->
                                                                        </div>
                                                                        <div class="row innerTB">
                                                                            <div class="col-md-12">
                                                                                <div class="btn-group col-md-4">
                                                                                    <asp:LinkButton ID="btnSearch" CssClass="btn btn-success col-md-2" runat="server" OnClick="btnSearch_Click">ค้นหา</asp:LinkButton>

                                                                                    <asp:LinkButton ID="btnClearSearch" CssClass="btn btn-default col-md-2 btn" runat="server" OnClick="btnClearSearch_Click">ล้าง</asp:LinkButton>


                                                                                </div>

                                                                            </div>
                                                                            <!-- /input-group -->
                                                                        </div>

                                                                    </div>
                                                                </asp:Panel>
                                                                <!------------end------------>
                                                                <asp:Label ID="txtLastUpdate" runat="server" Text="Label"></asp:Label>
                                                                <h5 class='innerAll margin-none bg-primary strong border-bottom'><i class='fa fa-fw fa-calendar'></i>&nbsp;&nbsp;ตารางข้อมูล</h5>
                                                                <asp:Label ID="txtTotalMedicines" runat="server" Text="Label"></asp:Label>
                                                                <div class="innerAll bg-gray">
                                                                    <div class="separator bottom">
                                                                        <span class="pull-right"></span>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                    <asp:GridView ID="GridView1" runat="server" OnRowCommand="GridView1_RowCommand" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs"
                                                                        PageSize="100" AllowPaging="True" Width="100%" Height="0px"
                                                                        OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="SYMPTOM_ID" HeaderText="รหัส" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" Visible="false">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="center" />
                                                                            </asp:BoundField>

                                                                            <asp:BoundField DataField="SYMPTOM_NAME" HeaderText="อาการ" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" HeaderStyle-Width="20%">
                                                                                <HeaderStyle CssClass="center" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="DRUGGROUP_NAME" HeaderText="โรคตามระบบ" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" HeaderStyle-Width="20%">
                                                                                <HeaderStyle CssClass="center" />
                                                                            </asp:BoundField>

                                                                            <asp:BoundField DataField="SYMPTOM_DESC" HeaderText="คำอธิบาย" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                <HeaderStyle CssClass="center" />
                                                                            </asp:BoundField>

                                                                            <asp:BoundField DataField="CREATE_BY" HeaderText="ผู้สร้าง" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" Visible="false">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="center" />
                                                                            </asp:BoundField>

                                                                            <asp:BoundField DataField="CREATE_DATE" HeaderText="สร้างเมื่อ" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" HeaderStyle-Width="10%">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="center" />
                                                                            </asp:BoundField>

                                                                            <asp:BoundField DataField="UPDATE_BY" HeaderText="ผู้เปลี่ยนแปลง" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" Visible="false">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="center" />
                                                                            </asp:BoundField>

                                                                            <asp:BoundField DataField="UPDATE_DATE" HeaderText="เปลี่ยนแปลงเมื่อ" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" HeaderStyle-Width="10%">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="center" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="COUNTER" HeaderText="ตัวนับ" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" HeaderStyle-Width="10%" Visible="false">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="center" />
                                                                            </asp:BoundField>

                                                                            <asp:TemplateField HeaderStyle-CssClass="center" HeaderText="" HeaderStyle-Width="15%">
                                                                                <ItemStyle CssClass="left" />
                                                                                <ItemTemplate>
                                                                                    <div class="btn-group btn-group-sm">
                                                                                        <asp:LinkButton ID="btnRowView" data-toggle="tooltip" data-original-title="ดูรายละเอียด" data-placement="top" runat="server" CommandName="cmdView" CommandArgument='<%#Eval("SYMPTOM_ID") %>' CssClass="btn btn-default fa fa-eye" />
                                                                                        <asp:LinkButton ID="btnRowEdit" data-toggle="tooltip" data-original-title="แก้ไข" data-placement="top" runat="server" CommandName="cmdEdit" CommandArgument='<%#Eval("SYMPTOM_ID") %>' CssClass="btn btn-success fa fa-pencil" />
                                                                                        <asp:LinkButton ID="btnRowDelete" data-toggle="tooltip" data-original-title="ลบ" data-placement="top" runat="server" CommandName="cmdDel" OnClientClick="if (!confirm('ยืนยันบันทึก !')) { return false; }" CommandArgument='<%#Eval("SYMPTOM_ID") %>' CssClass="btn btn-danger fa fa-trash-o" />
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                <div class="col-separator-h box"></div>
                                                            </div>
                                                        </div>
                                                        <div id="ascrail2000" class="nicescroll-rails" style="width: 5px; z-index: 2; cursor: default; position: absolute; top: 0px; left: 1073px; height: 432px; display: none; opacity: 0;">
                                                            <div style="position: relative; top: 0px; float: right; width: 5px; height: 432px; border: none; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px; background-color: rgb(235, 106, 90); background-clip: padding-box;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:HiddenField ID="hdfFunc" runat="server" />
            <asp:HiddenField ID="hdfDrugID" runat="server" />
            <asp:Panel ID="MedDrugOptionPanel" runat="server">
                <div class="row row-app">
                    <div class="col-md-12">
                        <div class="col-separator box col-separator-first col-unscrollable">
                            <div class="col-table">
                                <div class="heading-buttons border-bottom innerLR bg-primary">
                                    <asp:Label ID="lbSectionTitle" runat="server" Text="Label" CssClass="margin-none innerTB"></asp:Label>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-table-row">
                                    <div class="col-app col-unscrollable">
                                        <div class="col-app">
                                            <div class="widget widget-tabs border-none">
                                                <div class="widget-head">
                                                    <ul>
                                                        <li class="active"><a href="#" data-toggle="tab" class="glyphicons font"><i></i>ข้อมูล</a></li>
                                                    </ul>
                                                </div>
                                                <div class="widget-body">
                                                    <div class="tab-content">

                                                        <div class="tab-pane active" id="productDescriptionTab">
                                                            <br />
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <strong>ชื่อประเภทอาการ</strong>
                                                                    <p class="muted">ใช้แบ่งอาการ</p>
                                                                </div>
                                                                <div class="col-md-6">

                                                                    <asp:TextBox ID="txtBoxMedName" runat="server" CssClass="form-control" placeholder="ชื่ออาการ..."></asp:TextBox>

                                                                </div>
                                                                <div class="col-md-3">
                                                                    <asp:RequiredFieldValidator ValidationGroup="editGroupValidation" ID="rqfTxtBoxMedName" ControlToValidate="txtBoxMedName" runat="server" ErrorMessage=" ใส่อาการ" CssClass="btn btn-danger btn-sm fa fa-arrow-circle-down"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <hr class="separator bottom" />
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <strong>คำอธิบายอาการ</strong>
                                                                    <p class="muted">เพื่อใช้อธิบายอาการ</p>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <asp:TextBox ID="txtBoxMedDesc" runat="server" CssClass="form-control" placeholder="คำอธิบายอาการ..."></asp:TextBox>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <asp:RequiredFieldValidator ValidationGroup="editGroupValidation" ID="rqfTxtBoxMedDesc" ControlToValidate="txtBoxMedDesc" runat="server" ErrorMessage=" ใส่คำอธิบายอาการ" CssClass="btn btn-danger btn-sm fa fa-arrow-circle-down"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                            <hr class="separator bottom" />
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <strong>ชื่อโรคตามระบบ</strong>
                                                                    <p class="muted">เพื่อใช้ระบุบนตัวยา</p>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <asp:DropDownList ID="ddMedGroup" runat="server"  data-style="btn-default" CssClass="form-control selectpicker col-md-6">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <asp:RequiredFieldValidator ValidationGroup="editGroupValidation" ID="rqfDdMedGroup" ControlToValidate="ddMedGroup" InitialValue="" runat="server" ErrorMessage=" เลือกโรคตามระบบ" CssClass="btn btn-danger btn-sm fa fa-arrow-circle-down"></asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- // Widget END -->
                                            <div class="text-right innerAll border-top">
                                                <asp:Panel ID="pnButtonEditPage" runat="server">
                                                    <div class="btn-group btn-group">
                                                        <asp:LinkButton ID="btnClear" runat="server" CssClass="btn btn-default" OnClick="btnClear_Click"><i class="fa fa-fw fa-edit"></i> ล้าง</asp:LinkButton>
                                                        <asp:LinkButton ID="btnDiscard" runat="server" CssClass="btn btn-primary" OnClick="btnDiscard_Click"><i class="fa fa-fw fa-trash-o"></i> ยกเลิก</asp:LinkButton>
                                                        <asp:LinkButton ID="btnSave" runat="server" OnClientClick="if (!confirm('ยืนยันบันทึก !')) { return false; }" ValidationGroup="editGroupValidation" CssClass="btn btn-success" OnClick="btnSave_Click"><i class="fa fa-fw fa-save"></i> บันทึก</asp:LinkButton>
                                                    </div>
                                                </asp:Panel>
                                                <asp:Panel ID="pnButtonViewPage" runat="server">
                                                    <div class="btn-group btn-group">
                                                        <asp:LinkButton ID="btnEdit" runat="server" CssClass="btn btn-primary" OnClick="btnEdit_Click"><i class="fa fa-fw fa-pencil"></i> แก้ไข</asp:LinkButton>
                                                        <asp:LinkButton ID="btnConfirm" runat="server" CssClass="btn btn-success" OnClick="btnConfirm_Click"><i class="fa fa-fw fa-check-circle-o"></i> ตกลง</asp:LinkButton>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </asp:Panel>

            <asp:Panel ID="MedDrugErrorPanel" runat="server">
                <div class="row error">
                    <div class="col-md-4 col-md-offset-1 center">
                        <div class="center">
                            <img src="../assets//images/error-icon-bucket.png " class="error-icon" />
                        </div>
                    </div>
                    <div class="col-md-5 content center">
                        <h1 class="strong">Oups!</h1>
                        <h4 class="innerB">This page does not exist.</h4>
                        <div class="well">
                            ERROR MESSAGE GOES HERE!
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

