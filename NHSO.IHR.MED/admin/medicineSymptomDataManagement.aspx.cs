﻿using NHSO.IHR.MED.DataController;
using NHSO.IHR.MED.DataController.DataControllerModel;
using NHSO.IHR.MED.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace NHSO.IHR.MED.admin
{
    public partial class medicineSymptomDataManagement : Page
    {

        MedDrugSymptomCtr medCtr;
        public virtual string NullDisplayText { get; set; }
        private string editFunc = "cmdEdit";
        private string addFunc = "cmdAdd";
        private string listFunc = "cmdList";
        private string delFunc = "cmdDel";
        private string viewFunc = "cmdView";
        private string func = "cmdList";
        private string errorFunc = "cmdError";


        protected void Page_Init(object sender, EventArgs e)
        {
            medCtr = new MedDrugSymptomCtr();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                try
                {
                    hdfFunc.Value = listFunc;
                    BindDropDown();
                    showPanel();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    hdfFunc.Value = errorFunc;
                    showPanel();
                }
            }
        }

        //Page-Data-Action
        protected void bindGridView( MedDrugSymptomEntity _pObj)
        {
           // GridView1.DataSource = medCtr.getList();
            List<MedDrugSymptomEntity> lstdata = medCtr.getListSearch(_pObj);

            GridView1.DataSource = lstdata;
            GridView1.DataBind();
            if (GridView1.Rows.Count > 0)
            {
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            }   
            //txtTotalMedicines.Text = "<h5 class='innerAll margin-none bg-white strong border-bottom'>จำนวนอาการ:&nbsp;&nbsp;" + medCtr.medAmount + "&nbsp;แบบ</h5>";
            txtTotalMedicines.Text = "<h5 class='innerAll margin-none bg-white strong border-bottom'>จำนวนอาการ:&nbsp;&nbsp;" + lstdata.Count +"&nbsp;แบบ</h5>";
            txtLastUpdate.Text = "<h5 class='innerAll margin-none bg-white strong border-bottom'>ข้อมูลล่าสุดเมื่อ:&nbsp;&nbsp;" + medCtr.lastUpdate + "</h5>";
        }

        protected void bindToeditPage()
        {
            MedDrugSymptomEntity medDrug = new MedDrugSymptomEntity();
            medDrug = medCtr.getMedSymptom(hdfDrugID.Value);
            lbSectionTitle.Text = "<h4 class='margin-none strong innerTB bg-primary' >แก้ไขอาการ: &nbsp;" + medDrug.SYMPTOM_NAME;
            txtBoxMedName.Text = medDrug.SYMPTOM_NAME;
            txtBoxMedDesc.Text = medDrug.SYMPTOM_DESC;

            MedDrugGroupCtr medGroupCtr = new MedDrugGroupCtr();
            medGroupCtr.getDropdown(ddMedGroup,medGroupCtr.DRUGGROUP_NAME,medGroupCtr.DRUGGROUP_ID,"--โรคตามระบบ--");
            ddMedGroup.SelectedIndex = ddMedGroup.Items.IndexOf(ddMedGroup.Items.FindByValue(medDrug.DRUGGROUP_ID));

            hdfDrugID.Value = medDrug.SYMPTOM_ID;

            //Environment
            fieldFix();
            buttonForPage();
        }

        protected void bindToAddPage()
        {
            lbSectionTitle.Text = "<h4 class='margin-none strong innerTB bg-primary' >เพิ่มอาการ";
            MedDrugGroupCtr medGroupCtr = new MedDrugGroupCtr();
            medGroupCtr.getDropdown(ddMedGroup, medGroupCtr.DRUGGROUP_NAME, medGroupCtr.DRUGGROUP_ID, "--โรคตามระบบ--");

            //Environment
            fieldClear();
            fieldFix();
            buttonForPage();
        }

        protected void bindToViewPage()
        {
            MedDrugSymptomEntity medDrug = new MedDrugSymptomEntity();
            medDrug = medCtr.getMedSymptom(hdfDrugID.Value);
            lbSectionTitle.Text = "<h4 class='margin-none strong innerTB bg-primary'>รายละเอียดอาการ: &nbsp;" + medDrug.SYMPTOM_NAME;
            txtBoxMedName.Text = medDrug.SYMPTOM_NAME;
            txtBoxMedDesc.Text = medDrug.SYMPTOM_DESC;

            MedDrugGroupCtr medGroupCtr = new MedDrugGroupCtr();
            medGroupCtr.getDropdown(ddMedGroup, medGroupCtr.DRUGGROUP_NAME, medGroupCtr.DRUGGROUP_ID, "--โรคตามระบบ--");
            ddMedGroup.SelectedIndex = ddMedGroup.Items.IndexOf(ddMedGroup.Items.FindByValue(medDrug.DRUGGROUP_ID));

            hdfDrugID.Value = medDrug.SYMPTOM_ID;

            //Environment
            buttonForPage();
            fieldFix();
        }

        public void deleteRow(string id)
        {
            MedDrugSymptomEntity m = new MedDrugSymptomEntity();
            m.SYMPTOM_ID = id;
            int successful = 0;
            successful = medCtr.canBeDeleted(m);
            if (successful == 1)
            {
                //New ให้สามารถลบได้ โดยไม่ต้อง check ว่ามีอยู่ในโรคตามระบบ
                 successful = medCtr.deleteDataByFlag(m);
                 if (successful == 1)
                 {
                     hdfFunc.Value = listFunc;
                     showPanel();
                 }
                //if (Convert.ToInt32(medCtr.medDeleted) == 0)
                //{
                //    successful = medCtr.deleteDataByFlag(m);
                //    if (successful == 1)
                //    {
                //        hdfFunc.Value = listFunc;
                //        showPanel();
                //    }
                //}
                //else
                //{
                //    PopupMessage.Show("", "ไม่สามารถลบข้อมูลได้");
                //}
               
            }

        }

        //GridView-Control
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            //bindGridView();
            bindGridView(SearchCondition());
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string value = e.CommandArgument.ToString();
            hdfFunc.Value = e.CommandName;

            if (hdfFunc.Value == editFunc)
            {
                hdfDrugID.Value = value;
                showPanel();
            }
            else if (hdfFunc.Value == viewFunc)
            {
                hdfDrugID.Value = value;
                showPanel();
            }
            else if (hdfFunc.Value == delFunc)
            {
                hdfDrugID.Value = value;
                deleteRow(hdfDrugID.Value);
                showPanel();
            }

        }

        //Utilities (To show panel)
        protected void showPanel()
        {

            if (hdfFunc.Value == listFunc)
            {
                //bindGridView();
                bindGridView(SearchCondition());
                MedDrugListPanel.Visible = true;
                MedDrugOptionPanel.Visible = false;
                MedDrugErrorPanel.Visible = false;
            }
            else if (hdfFunc.Value == addFunc)
            {
                bindToAddPage();
                MedDrugListPanel.Visible = false;
                MedDrugOptionPanel.Visible = true;
                MedDrugErrorPanel.Visible = false;
            }
            else if (hdfFunc.Value == editFunc)
            {
                bindToeditPage();
                MedDrugListPanel.Visible = false;
                MedDrugOptionPanel.Visible = true;
                MedDrugErrorPanel.Visible = false;
            }
            else if (hdfFunc.Value == errorFunc)
            {
                MedDrugListPanel.Visible = false;
                MedDrugOptionPanel.Visible = false;
                MedDrugErrorPanel.Visible = true;
            }
            else if (hdfFunc.Value == delFunc)
            {
                //bindGridView();
                bindGridView(SearchCondition());
                MedDrugListPanel.Visible = true;
                MedDrugOptionPanel.Visible = false;
                MedDrugErrorPanel.Visible = false;
            }
            else if (hdfFunc.Value == viewFunc)
            {
                bindToViewPage();
                MedDrugListPanel.Visible = false;
                MedDrugOptionPanel.Visible = true;
                MedDrugErrorPanel.Visible = false;
            }

        }

        protected void fieldClear()
        {
            txtBoxMedName.Text = "";
            txtBoxMedDesc.Text = "";
        }

        protected void fieldFix()
        {
            if (hdfFunc.Value == viewFunc)
            {
                txtBoxMedName.Enabled = false;
                txtBoxMedDesc.Enabled = false;
                ddMedGroup.Enabled = false;
            }
            else
            {
                txtBoxMedName.Enabled = true;
                txtBoxMedDesc.Enabled = true;
                ddMedGroup.Enabled = true;
            }
        }

        protected void buttonForPage()
        {
            if (hdfFunc.Value == viewFunc)
            {
                pnButtonViewPage.Visible = true;
                pnButtonEditPage.Visible = false;
            }
            else
            {
                pnButtonViewPage.Visible = false;
                pnButtonEditPage.Visible = true;
            }
        }


        //Navigation 
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = addFunc;
            showPanel();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            fieldClear();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int successful = 0;
            MedDrugSymptomEntity m1 = new MedDrugSymptomEntity();

            if (hdfFunc.Value == addFunc)
            {
                m1.SYMPTOM_NAME = txtBoxMedName.Text;
                m1.SYMPTOM_DESC= txtBoxMedDesc.Text;
                m1.DRUGGROUP_ID = ddMedGroup.SelectedValue;
                m1.CREATE_BY = "0";
                m1.UPDATE_BY = "0";
                m1.FLAG = "Y";

                //Check DRUGTYPE_NAME ซ้ำ หรือป่าว
                if (!CheckCharacter(m1.SYMPTOM_NAME.Trim(), addFunc, ""))
                {
                    successful = medCtr.insertData(m1);
                }
                else
                {
                    PopupMessage.Show("", "มีชื่ออาการนี้อยู่แล้ว");
                }
            }
            else if (hdfFunc.Value == editFunc)
            {
                m1.SYMPTOM_ID = hdfDrugID.Value;
                m1.SYMPTOM_NAME = txtBoxMedName.Text;
                m1.SYMPTOM_DESC = txtBoxMedDesc.Text;
                m1.DRUGGROUP_ID = ddMedGroup.SelectedValue;
                m1.UPDATE_BY = "0";
                m1.FLAG = "Y";

                //Check DRUGTYPE_NAME ซ้ำ หรือป่าว
                if (!CheckCharacter(m1.SYMPTOM_NAME.Trim(), editFunc, m1.SYMPTOM_ID))
                {
                    successful = medCtr.updateData(m1);
                }
                else
                {
                    PopupMessage.Show("", "มีชื่ออาการนี้อยู่แล้ว");
                }
            }
            else if (hdfFunc.Value == viewFunc)
            {
                successful = 1;
            }
            //Insert

            if (successful == 1)
            {
                hdfFunc.Value = listFunc;
                showPanel();
            }
        }

        protected void btnDiscard_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = listFunc;
            showPanel();
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = listFunc;
            showPanel();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            hdfFunc.Value = editFunc;
            showPanel();
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string Namecolumnvalue = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "COUNTER"));
                LinkButton lnk2 = (LinkButton)e.Row.FindControl("btnRowDelete");
                if (Namecolumnvalue == "0" || string.IsNullOrEmpty(Namecolumnvalue))
                {
                    lnk2.Visible = true;
                }
                else
                {
                    lnk2.Visible = false;
                }
            }
        }
        // Search panel
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            bindGridView(SearchCondition());
        }

        protected void ddMedDrugGroupSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<MedDrugSymptomEntity> lstdata = medCtr.getListSymptomName();

            if ( !String.IsNullOrEmpty(ddMedDrugGroupSearch.SelectedValue) )
            {
                string GroupID = ddMedDrugGroupSearch.SelectedValue.Trim();

                ddMedDrugSymptom.Items.Clear();
                ddMedDrugSymptom.DataSource = lstdata.Where(x => x.DRUGGROUP_ID.Equals(GroupID));
            }
            else
            {
                ddMedDrugSymptom.DataSource = lstdata;
            }

            ddMedDrugSymptom.DataTextField = "symptom_name";
            //ddMedDrugSymptom.DataValueField = "symptom_id";
            ddMedDrugSymptom.DataBind();
            ddMedDrugSymptom.Items.Insert(0, new ListItem("ทั้งหมด", "", true));

        }

        private void BindDropDown()
        {

            List<MedDrugSymptomEntity> lstdata = medCtr.getListGroup();
            ddMedDrugGroupSearch.DataSource = lstdata;
            ddMedDrugGroupSearch.DataTextField = "DRUGGROUP_NAME";
            ddMedDrugGroupSearch.DataValueField = "druggroup_id";
            ddMedDrugGroupSearch.DataBind();
            ddMedDrugGroupSearch.Items.Insert(0, new ListItem("ทั้งหมด", "", true));

            ddMedDrugSymptom.DataSource = medCtr.getListSymptomName();
            ddMedDrugSymptom.DataTextField = "symptom_name";
            //ddMedDrugSymptom.DataValueField = "symptom_id";
            ddMedDrugSymptom.DataBind();
            ddMedDrugSymptom.Items.Insert(0, new ListItem("ทั้งหมด", "", true));

        }

        private MedDrugSymptomEntity SearchCondition()
        {
            string StartDate = txtSelectDateBegin1.Text;
            string EndDate = txtSelectDateEnd1.Text;

            MedDrugSymptomEntity lstSearch = new MedDrugSymptomEntity();
            lstSearch.DRUGGROUP_ID = ddMedDrugGroupSearch.SelectedValue;
            lstSearch.SYMPTOM_NAME = ddMedDrugSymptom.SelectedValue;

            if (!StartDate.Equals("") && !EndDate.Equals(""))
            {
                DateTime Start_Date = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));
                lstSearch.START_DATE = Start_Date.ToString("dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));
                DateTime End_Date = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));
                End_Date = End_Date.AddDays(+1);
                lstSearch.END_DATE = End_Date.ToString("dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));
            }
            else if (!StartDate.Equals("") && EndDate.Equals(""))
            {
                DateTime Start_Date = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));
                lstSearch.START_DATE = Start_Date.ToString("dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));
                Start_Date = Start_Date.AddDays(+1);
                lstSearch.END_DATE = Start_Date.ToString("dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US"));
            }
            else
            {
                lstSearch.START_DATE = txtSelectDateBegin1.Text;
                lstSearch.END_DATE = txtSelectDateEnd1.Text;
            }
           
            lstSearch.FLAG = "Y";

            return lstSearch;

        }

        protected void btnClearSearch_Click(object sender, EventArgs e)
        {
            //ddMedDrugGroupSearch.SelectedIndex = 0;
            //ddMedDrugSymptom.SelectedIndex = 0;
            //txtSelectDateBegin1.Text = "";
            //txtSelectDateEnd1.Text = "";
            Response.Redirect("medicineSymptomDataManagement.aspx");
        }

        private bool CheckCharacter(string Symptom_Name, string Mode, string SYMPTOM_ID)
        {
            bool ISHave = false;

            List<MedDrugSymptomEntity> lsttype = medCtr.Check_Chr(Symptom_Name);

            if (Mode == addFunc)
            {
                if (lsttype.Count != 0)
                {
                    ISHave = true;
                }
            }
            else
            {
                lsttype = lsttype.Where(x => x.SYMPTOM_ID != SYMPTOM_ID).ToList();

                if (lsttype.Count != 0)
                {
                    ISHave = true;
                }
            }

            return ISHave;
        }
    }
}