﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NHSO.IHR.MED.Utility;
using System.IO;
namespace NHSO.IHR.MED.admin
{
    public partial class medicineReportPerson : Page
    {
        private ReportPerson _reportperson;
        protected void Page_Init(object sender, EventArgs e)
        {
            _reportperson = new ReportPerson();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtSdate.Text = DateTime.Now.AddDays(-7).ToStringUS();
                txtEdate.Text = DateTime.Now.ToStringUS();
                BindingDDL();
                BindingGV();
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            //base.VerifyRenderingInServerForm(control);
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindingGV();
        }
        private void BindingDDL()
        {
            ddDepSearch.DataSource = _reportperson.GetBurea();
            ddDepSearch.DataTextField = "EMP_BUREAU";
            ddDepSearch.DataValueField = "EMP_BUREAUID";
            ddDepSearch.DataBind();
            ddDepSearch.Items.Insert(0, new ListItem("ทั้งหมด", "", true));
        }
        private void BindingGV()
        {

            GridView1.DataSource = _reportperson.GetReportDetail(txtBoxSearchName.Text, ddDepSearch.SelectedValue
                                                                 , txtSdate.Text, txtEdate.Text);
            GridView1.DataBind();
            if (GridView1.Rows.Count != 0)
            {
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            txtTotalMedicines.Text = "<h5 class='innerAll margin-none bg-white strong border-bottom'>จำนวนที่แสดงผล:&nbsp;&nbsp;" + GridView1.Rows.Count +"&nbsp;รายการ</h5>";

        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            //Response.ClearContent();
            //Response.ClearHeaders();
            //Response.Buffer = true;
            //Response.AddHeader("content-disposition", "attachment;filename=" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm") + ".xls");
            //Response.Charset = "";
            //Response.ContentType = "application/vnd.ms-excel";
            //StringWriter sw = new StringWriter();
            //HtmlTextWriter hw = new HtmlTextWriter(sw);
            //GridView1.RenderControl(hw);
            //Response.Write("<table>");
            //Response.Write("<tr><td>รายงานรายบุคคล</td></tr>");
            //Response.Write("<tr><td>สำนัก</td><td>" + ddDepSearch.SelectedItem.ToString() + "<td></tr>");
            //Response.Write("<tr><td>ตั้งแต่วันที่ " + txtSdate.Text + "</td><td>ถึง " + txtEdate.Text + "</td></tr>");
            //Response.Write("</table>");
            //Response.Output.Write(sw.ToString());
            //Response.Flush();
            //Response.End();
            string Haederhtml = "<table style='border:1px solid #000;font-weight:bold;'>" +
                                "<tr><td colspan='12' style='text-align:center;background-color:#B0E2FF;'>รายงานการเบิกยารายบุคคล</td></tr>" +
                                "<tr><td colspan='12' style='border:1px solid #000;'>จำวนวนข้อมูล " + GridView1.Rows.Count + " รายการ</td></tr>" +
                                //"<tr><td>ตั้งแต่วันที่ " + txtSdate.Text + "</td><td>ถึง " + txtEdate.Text + "</td></tr>" +
                                "</table>";
            List<int> HideColumns = new List<int>();
            PrintExcle.PrintLabel(DateTime.Now.ToString("dd_MM_yyyy_HH_mm") + ".xls", Haederhtml, this.Page, GridView1, HideColumns);
        }

        protected void btnClearSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("medicineReportPerson.aspx");
        }
    }
}