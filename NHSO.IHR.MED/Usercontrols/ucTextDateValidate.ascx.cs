﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NHSO.IHR.MED.Usercontrols
{
    public partial class ucTextDateValidate : System.Web.UI.UserControl
    {
        //public string TextValue { get; set; }
        private string textValue;

        public string TextValue
        {
            get { return txtdate.Text; }
            set { textValue = value; }
        }
        public string ValidateGroupText { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {           
            if (!IsPostBack)
            {
                txtdate.Text = DateTime.Now.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.GetCultureInfo("en-US")); 
            }
            //RegularExpressionValidator1.ValidationExpression = "^(0[1-9]|[12][0-9]|3[01])[-/.](0[1-9]|1[012])[-/.](19|20|25)\\d\\d$";
            //RegularExpressionValidator1.ErrorMessage = "วันที่ไม่ถูกต้อง Format DD/MM/YYYY";
            //RegularExpressionValidator1.ValidationGroup = ValidateGroupText;
            //RequiredFieldValidator1.ValidationGroup = ValidateGroupText;
        }
    }
}