﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucTextDateValidate.ascx.cs" Inherits="NHSO.IHR.MED.Usercontrols.ucTextDateValidate" %>
<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {

        //$('input[class="calendars"]').mask("99/99/9999");
        //$('input[class="calendars"]').calendarsPicker({
        //    calendar: $.calendars.instance('thai', 'th'),
        //    showOnFocus: false, dateFormat: 'dd/mm/yyyy', showTrigger: '<img src="../Images/calendar.gif" alt="Popup" class="trigger" />'
        //});

        $("#txtSelectDateBegins").datepicker({
            format: 'dd/mm/yyyy',
            todayBtn: "linked",
            language: "th",
            todayHighlight: true,
            autoclose: true
        });
    });
</script>
<div class="input-group input-group date" id="txtSelectDateBegins">
    <asp:TextBox ID="txtdate" runat="server" class="form-control"  disabled></asp:TextBox>
    <span class="input-group-addon"><i class="fa fa-th"></i></span>
</div>
<%--<asp:TextBox ID="txtdate" Height="30px" runat="server" class="calendars"></asp:TextBox>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
    ControlToValidate="txtdate" ErrorMessage="*"></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtdate" ErrorMessage="RegularExpressionValidator"></asp:RegularExpressionValidator>--%>
