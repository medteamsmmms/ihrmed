﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NHSO.IHR.MED.Data.Oracle.Common;
using System.Data.OracleClient;
using NHSO.IHR.MED.Utility;
using System.Data;
namespace NHSO.IHR.MED.Usercontrols
{
    public partial class SearchEmployees : System.Web.UI.UserControl
    {
        public event EventHandler Add_Click;
        public string Id { get; set; }
        public string Name { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
           
            
        }

        public void Show(string msg, DataSet dsemp)
        {
            try
            {
                lblHeader.Text = msg;
                BindEmployee(dsemp);
                btnCancel.Focus();
                this.ModalPopupExtender1.Show();
            }
            catch (Exception)
            {

            }
        }
        public void BindEmployee(DataSet dsemp)
        {
            //CommandData cmm = new CommandData();
            //cmm.SetCommandStoredProcedure("PKG_MED_EMPLOYEES.md_get_employees");
            //cmm.AddInputParameter("o_emp_id", OracleType.VarChar, msg.ToStringOrNull());
            //cmm.AddOutputParameter("o_result", OracleType.Cursor, 100000);
 
            //System.Data.DataSet ds = cmm.ExecuteToDataset();
            GridView1.DataSource = dsemp;
            GridView1.DataBind();
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string value = e.CommandName;
            if (value == "cmdadd")
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                Id = row.Cells[0].Text;
                Name = row.Cells[1].Text;
                Add_Click(sender, e);
            }
        }

        protected void GridView1_PreRender(object sender, EventArgs e)
        {
            if (GridView1.Rows.Count > 0)
            {
                GridView1.UseAccessibleHeader = true;
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
        
    }
}