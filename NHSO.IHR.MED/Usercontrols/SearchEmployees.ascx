﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchEmployees.ascx.cs" Inherits="NHSO.IHR.MED.Usercontrols.SearchEmployees" %>
<link href="../Content/ModalPopup.css" rel="stylesheet" />
<script type="text/javascript">
    function HideModalExportStock() {
        $find("<%=ModalPopupExtender1.ClientID%>").hide();
    }
    function ShowModalExportStock() {
        $find("<%=ModalPopupExtender1.ClientID%>").show();
    }
</script>

<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server"
    PopupControlID="Panel1"
    TargetControlID="Button1"
    CancelControlID="btnCancel"
    BackgroundCssClass="ModalPopupBG">
</ajaxToolkit:ModalPopupExtender>
<asp:Button ID="Button1" runat="server" Text="Button" Style="display: none" />
<asp:Panel ID="Panel1" runat="server" align="center" Style="display: none" CssClass="popupPanal" Height="500px" Width="700px">
    <div class="popup_Container ">
        <div class="popup_Titlebar" id="PopupHeader">
            <div class="TitlebarLeft">
                <asp:Label ID="lblHeader" runat="server" Text="Header"></asp:Label>
            </div>
            <div class="TitlebarRight" onclick="HideModalExportStock()"></div>
        </div>
        <div class="popup_Body">
            <asp:Panel ID="Panel2" runat="server" Height="400px" ScrollBars="Vertical">
                <asp:GridView ID="GridView1"  Style="height:auto"  runat="server" Width="600px" AutoGenerateColumns="False"  CssClass="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs" OnRowCommand="GridView1_RowCommand" OnPreRender="GridView1_PreRender">
                    <Columns>
                        <asp:BoundField DataField="emp_id" HeaderText="รหัส"></asp:BoundField>
                        <asp:BoundField DataField="emp_thname" HeaderText="ชื่อ"></asp:BoundField>
                        <asp:BoundField DataField="EMP_BUREAU" HeaderText="สำนัก"></asp:BoundField>
                        <asp:TemplateField>
                            <ItemTemplate>
                               <%-- <asp:Button ID="btnchoose" runat="server" CommandArgument='<%#Eval("emp_id") %>' CommandName="cmdadd" Text="เลือก" />--%>
                                <asp:LinkButton ID="btnchoose" runat="server" CssClass="btn btn-primary" CommandArgument='<%#Eval("emp_id") %>' CommandName="cmdadd">เลือก</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
        </div>
        <div class="popup_Buttons">
            <%-- <asp:Button ID="btnSave" runat="server" Text="บันทึก" 
                    OnClientClick="if (!confirm('ยืนยันบันทึก !')) { return false; }" 
                    onclick="btnSave_Click" />&nbsp;&nbsp;--%>
            <%--<asp:Button ID="btnCancel" runat="server" Text="ยกเลิก" />--%>
            <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-danger" style="margin-top:6px;">ยกเลิก</asp:LinkButton>
        </div>
    </div>
</asp:Panel>
