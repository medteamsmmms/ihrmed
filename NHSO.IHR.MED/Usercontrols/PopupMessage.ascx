﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PopupMessage.ascx.cs" Inherits="NHSO.IHR.MED.Usercontrols.PopupMessage" %>
<%--<link href="../Content/ModalPopup.css" rel="stylesheet" />--%>
 <script type="text/javascript">
      $(document).ready(function () {   
          $(document).keydown(function (e) {
              var code = (e.keyCode ? e.keyCode : e.which);
              if (code == 27 || code == 13)// ESC, Enter key hit
              {
                  $find("<%=ModalPopupExtender1.ClientID%>").hide();
              }
          });    
      });
    </script>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server"
    PopupControlID="Panel1"
    TargetControlID="vButton1"
    CancelControlID="btnCancel"
    BackgroundCssClass="ModalPopupBG">
</ajaxToolkit:ModalPopupExtender>

<asp:Panel ID="Panel1" runat="server" align="center" Style="display: none" CssClass="popupPanal" >
    <div class="popup_Container_MES">
        <div class="popup_Titlebar_MES" id="PopupHeader" >
            <div class="TitlebarLeft_MES">
               <h4> <i class="fa fa-bullhorn">&nbsp;&nbsp;แจ้งเตือน</i> <asp:Label ID="lblHeader" runat="server" Text="Header" ForeColor="#4d4d4d"></asp:Label></h4></div>
            <div class="TitlebarRight_MES" onclick="HideModalExportStock()"></div>
        </div>
        <div class="popup_Body_MES">
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </div>
        <div class="popup_Buttons_MES">
            <%-- <asp:Button ID="btnSave" runat="server" Text="บันทึก" 
                    OnClientClick="if (!confirm('ยืนยันบันทึก !')) { return false; }" 
                    onclick="btnSave_Click" />&nbsp;&nbsp;--%>
            <%--<asp:Button ID="btnCancel" runat="server" Text="ตกลง" />--%>
            <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn" style="margin-top:5px;background-color:#fa8564;color:white">ตกลง</asp:LinkButton>
        </div>
    </div>
    <asp:Button ID="vButton1" runat="server" Text="Button" Enabled="false" Style="display:none;" />
</asp:Panel>

<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server"
    PopupControlID="Panel2"
    TargetControlID="vButton2"
    BackgroundCssClass="ModalPopupBG">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="Panel2" runat="server" align="center" Style="display: none" CssClass="popupPanal" >
    <div class="popup_Container_MES">
        <div class="popup_Titlebar_MES" id="PopupHeaderRe" >
            <div class="TitlebarLeft_MES">
               <h4> <i class="fa fa-bullhorn">&nbsp;&nbsp;แจ้งเตือน</i> <asp:Label ID="lblHeaderRe" runat="server" Text="Header" ForeColor="#4d4d4d"></asp:Label></h4></div>
            <div class="TitlebarRight_MES" onclick="HideModalExportStock()"></div>
        </div>
        <div class="popup_Body_MES">
            <asp:Literal ID="Literal2" runat="server"></asp:Literal>
        </div>
        <div class="popup_Buttons_MES">
            <asp:LinkButton ID="btnRefresh" runat="server" CssClass="btn" style="margin-top:5px;background-color:#fa8564;color:white">ตกลง</asp:LinkButton>
        </div>
    </div>
    <asp:Button ID="vButton2" runat="server" Text="Button" Enabled="false" Style="display:none;" />
</asp:Panel>