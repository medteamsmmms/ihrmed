﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NHSO.IHR.MED.Usercontrols
{
    public partial class ucTextDateEmpty : System.Web.UI.UserControl
    {
        private string textValue;

        public string TextValue
        {
            get { return txtdate.Text; }
            set { textValue = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtdate.Text = "";
            }
        }
    }
}