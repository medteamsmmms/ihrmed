﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NHSO.IHR.MED.Usercontrols
{
    public partial class PopupMessage : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void Show(string Head, String Body)
        {
            try
            {
                lblHeader.Text = Head;
                Literal1.Text = @"<h4>" + Body + "</h4>";
                btnCancel.Focus();
                this.ModalPopupExtender1.Show();
            }
            catch (Exception)
            {

            }
        }
        public void ShowRespon(string Head, String Body,string URL)
        {
            try
            {
                lblHeaderRe.Text = Head;
                Literal2.Text = @"<h4>" + Body + "</h4>";
                btnRefresh.Focus();
                btnRefresh.Attributes["onclick"] = "window.location = '" + URL + "'";
                this.ModalPopupExtender2.Show();

            }
            catch (Exception)
            {

            }
        }
    }
}