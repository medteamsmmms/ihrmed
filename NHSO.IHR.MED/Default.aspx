﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits=" NHSO.IHR.MED._Default" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1">

    <ul class="main pull-left">
        <li class="dropdown">
            <a href="#"><i class="fa fa-fw fa-home"></i>หน้าหลัก </a>

        </li>
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle dropdown-hover"><i class="fa fa-fw fa-suitcase"></i>ข้อมูลหลัก <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="ui.html?lang=en">ข้อมูลยา</a></li>
                <li><a href="icons.html?lang=en">ข้อมูลกลุ่มของยา</a></li>
                <li><a href="typography.html?lang=en">ข้อมูลอาการ</a></li>
                <li><a href="widgets.html?lang=en">ข้อมูลประเภทของยา</a></li>
                <li><a href="calendar.html?lang=en">ข้อมูลหน่วยนับ</a></li>
                <li><a href="tabs.html?lang=en">ข้อมูลขนาดยา</a></li>

            </ul>
        </li>
        <li class="dropdown">
            <i class="fa fa-fw fa-check-square-o"></i>รายการยา 
			
        </li>

    </ul>
</asp:Content>


<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder2">
    <!--<style>
    table thead tr th:nth-child(n+1){
                text-align: center;
                /*background: #000;*/
            }
</style>-->
    <script src="assets/components/library/jquery/bootpag.min.js"></script>
    <div class="row row-app">
        <div class="col-md-12 animated fadeInDown">
            <div class="col-separator col-separator-first col-unscrollable">
                <div class="col-table">
                    <div class="heading-buttons innerLR box">
                        <h4 class="margin-none innerTB pull-left">รายการยา</h4>
                        <a href="medicine_list_add.html" class="btn-xs pull-right btn btn-primary"><i class="fa fa-fw fa-plus"></i>Add record</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-separator-h"></div>
                    <div class="col-table-row">
                        <div class="col-app col-unscrollable">
                            <div class="col-app hasNiceScroll animated fadeInDown" tabindex="10000" style="overflow-y: hidden; outline: none;">

                                <div class="col-table">

                                    <div class="col-separator-h box"></div>

                                    <div class="col-table-row">
                                        <div class="row-app">
                                            <div>
                                                <div class="col-separator box bg-gray hasNiceScroll">

                                                    <asp:Label ID="txtLastUpdate" runat="server" Text="Label"></asp:Label>

                                                    <div class="innerAll bg-white">
                                                        <!-- Total bookings & sort by options -->
                                                        <div class="separator bottom">
                                                            Total Medcines:
                                                            <asp:Label ID="txtTotalMedicines" runat="server" Text="Label"></asp:Label>
                                                            <span class="pull-right"></span>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <!-- // Total bookings & sort by options END -->

                                                        <!-- Table -->
                                                        <asp:ScriptManager ID="ScriptManager1" runat="server">
                                                        </asp:ScriptManager>

                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                            <ContentTemplate>


                                                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs"
                                                                    PageSize="10" AllowPaging="True"
                                                                    OnPageIndexChanging="GridView1_PageIndexChanging">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="DRUGLIST_ID" HeaderText="รหัส" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                            <HeaderStyle CssClass="center" />
                                                                            <ItemStyle Width="10%" CssClass="center" />
                                                                        </asp:BoundField>

                                                                        <asp:BoundField DataField="DRUGLIST_NAME" HeaderText="ชื่อยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                            <HeaderStyle CssClass="center" />
                                                                            <ItemStyle Width="10%" CssClass="center" />
                                                                        </asp:BoundField>

                                                                        <asp:BoundField DataField="DRUGLIST_DESC" HeaderText="ชื่อสามัญทางยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                            <HeaderStyle CssClass="center" />
                                                                            <ItemStyle Width="10%" CssClass="center" />
                                                                        </asp:BoundField>

                                                                        <asp:BoundField DataField="DRUGGROUP_NAME" HeaderText="กลุ่มยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                            <HeaderStyle CssClass="center" />
                                                                            <ItemStyle Width="10%" CssClass="center" />
                                                                        </asp:BoundField>

                                                                        <asp:BoundField DataField="GRUGTYPE_NAME" HeaderText="ชนิดยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                            <HeaderStyle CssClass="center" />
                                                                            <ItemStyle Width="10%" CssClass="center" />
                                                                        </asp:BoundField>

                                                                        <asp:BoundField DataField="UNLIST_NAME" HeaderText="หน่วยนับ" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                            <HeaderStyle CssClass="center" />
                                                                            <ItemStyle Width="10%" CssClass="center" />
                                                                        </asp:BoundField>

                                                                        <asp:BoundField DataField="DRUGLIST_DOSE" HeaderText="ขนาด" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                            <HeaderStyle CssClass="center" />
                                                                            <ItemStyle Width="10%" CssClass="center" />
                                                                        </asp:BoundField>

                                                                        <asp:BoundField DataField="UNITTYPE_NAME" HeaderText="น้ำหนัก" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                            <HeaderStyle CssClass="center" />
                                                                            <ItemStyle Width="10%" CssClass="center" />
                                                                        </asp:BoundField>

                                                                        <asp:BoundField DataField="BARCODE_STANDARD" HeaderText="บาร์โค๊ด" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                            <HeaderStyle CssClass="center" />
                                                                            <ItemStyle Width="10%" CssClass="center" />
                                                                        </asp:BoundField>




                                                                    </Columns>




                                                                </asp:GridView>
                                                            </ContentTemplate>

                                                        </asp:UpdatePanel>




                                                        <!-- // Table END -->

                                                        <!-- With selected actions -->

                                                        <!-- // With selected actions END -->

                                                        <!-- Pagination -->
                                                        <ul class="pagination pull-right margin-none">
                                                            <li class="disabled"><a href="#">«</a></li>
                                                            <li class="active"><a href="#">1</a></li>
                                                            <li><a href="#">2</a></li>
                                                            <li><a href="#">3</a></li>
                                                            <li><a href="#">»</a></li>
                                                        </ul>
                                                        <div class="clearfix"></div>
                                                        <!-- // Pagination END -->
                                                    </div>
                                                    <div class="col-separator-h box"></div>

                                                </div>
                                            </div>

                                            <div id="ascrail2000" class="nicescroll-rails" style="width: 5px; z-index: 2; cursor: default; position: absolute; top: 0px; left: 1073px; height: 432px; display: none; opacity: 0;">
                                                <div style="position: relative; top: 0px; float: right; width: 5px; height: 432px; border: none; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px; background-color: rgb(235, 106, 90); background-clip: padding-box;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>


