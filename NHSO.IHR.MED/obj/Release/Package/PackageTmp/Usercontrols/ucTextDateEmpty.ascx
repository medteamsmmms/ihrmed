﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucTextDateEmpty.ascx.cs" Inherits="NHSO.IHR.MED.Usercontrols.ucTextDateEmpty" %>
<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {

        $("#txtSelectDateBegins").datepicker({
            format: 'dd/mm/yyyy',
            todayBtn: "linked",
            language: "th",
            todayHighlight: true,
            autoclose: true
        });
    });
</script>
<div class="input-group input-group date" id="txtSelectDateBegins">
    <asp:TextBox ID="txtdate" runat="server" class="form-control"  disabled></asp:TextBox>
    <span class="input-group-addon"><i class="fa fa-th"></i></span>
</div>