﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="NHSO.IHR.MED.Login" %>

<%@ Register Src="~/Usercontrols/PopupMessage.ascx" TagPrefix="uc1" TagName="PopupMessage" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="assets/components/library/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/components/library/icons/fontawesome/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="Styles/Signin.css" rel="stylesheet" />
    <script src="assets/components/library/jquery/jquery.min.js"></script>
    <script src="assets/components/library/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="assets/css/admin/module.admin.stylesheet-complete.sidebar_type.fusion.min.css" />
    <link href="Content/ModalPopup.css" rel="stylesheet" />
</head>
<body>

    <div style="margin-top:12%;" class="container">
        <form id="form1" class="form-signin" defaultbutton="btnsubmit" defaultfocus="txtusername" role="form" runat="server">
            <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></ajaxToolkit:ToolkitScriptManager>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <h2 class="form-signin-heading">เข้าสู่ระบบห้องยา</h2>
                    <div class="form-group">
                        <asp:TextBox ID="txtusername" class="form-control" placeholder="Username" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="txtpassword" TextMode="Password" class="form-control" placeholder="Password" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Button ID="btnsubmit" class="btn btn-primary btn-lg btn-block" runat="server" Text="เข้าสู่ระบบ" OnClick="btnsubmit_Click" />
                    </div>
                    <uc1:PopupMessage runat="server" ID="PopupMessage" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </form>
    </div>
</body>
</html>
