﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Admin.Master" AutoEventWireup="true" CodeBehind="medicineListManagement.aspx.cs" Inherits="NHSO.IHR.MED.admin.medicineListManagement" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1">
    
        <script type="text/javascript">
            $(document).ready(function () {
                var pressed = false;
                var chars = [];
                $(window).keypress(function (e) {
                    if (e.which >= 48 && e.which <= 57) {
                        chars.push(String.fromCharCode(e.which));
                    }
                    if (pressed == false) {
                        setTimeout(function () {
                            if (chars.length >= 10) {
                                var barcode = chars.join("");
                                // assign value to some input (or do whatever you want)
                                $("#<%= txtBoxSearchName.ClientID %>").val(barcode);
                            $('#<%= Button1.ClientID %>').click();

                        }
                        chars = [];
                        pressed = false;
                    }, 500);
                }
                pressed = true;
                });

                $("#<%= txtBoxSearchName.ClientID %>").keypress(function (e) {
                    if (e.which === 13) {
                        e.preventDefault();
                    }
                });

                $("#<%= txtBoxDrugDose.ClientID %>").keydown(function (e) {
                    // Allow: backspace, delete, tab, escape, enter and .
                    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                        // Allow: Ctrl+A
                        (e.keyCode == 65 && e.ctrlKey === true) ||
                        // Allow: home, end, left, right, down, up
                        (e.keyCode >= 35 && e.keyCode <= 40)) {
                        // let it happen, don't do anything
                        return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
                });

            });

      

        //---- Add active Class-----//
            document.getElementById("List").className += " li-active active";
            document.getElementById("iList").className += " active";

    </script>
     <asp:Button ID="Button1"  Style="display: none;" runat="server" Text="Button" OnClick="Button1_Click" />
    <ul class="main pull-left">
        <li class="dropdown">
            <a href="medicineWithdrawManagement.aspx"><i class="fa fa-fw fa-home"></i>หน้าหลัก </a>
        </li>
    </ul>
</asp:Content>


<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder2">
   <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>        
            <asp:Panel ID="MedDrugListPanel" runat="server">
                <div class="row row-app" id="MedDrugListDiv">
                    <div class="col-md-12 animated fadeInDown">
                        <div class="col-separator col-separator-first col-unscrollable">
                            <div class="col-table">
                                <div class="heading-buttons innerLR box">
                                    <h4 class="margin-none innerTB pull-left">รายการยา</h4>

                                    <asp:LinkButton runat="server" CssClass="btn-xs pull-right btn btn-primary" OnClick="btnAdd_Click"><i class="fa fa-fw fa-plus"></i>เพิ่มข้อมูล</asp:LinkButton>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-separator-h"></div>
                                <div class="col-table-row">
                                    <div class="col-app col-unscrollable">
                                        <div class="col-app hasNiceScroll animated fadeInDown" tabindex="10000" style="overflow-y: hidden; outline: none;">

                                            <div class="col-table">

                                                <div class="col-separator-h box"></div>

                                                <div class="col-table-row">
                                                    <div class="row-app">
                                                        <div>
                                                            <div class="col-separator box bg-gray hasNiceScroll">
                                                                <h5 class="innerAll margin-none bg-primary strong border-bottom"><i class="fa fa-fw fa-search"></i>ค้นหา</h5>
                                                                <asp:Panel ID="Panel1" DefaultButton="btnSearch" runat="server">
                                                                    <div class="innerAll margin-none bg-gray strong border-bottom">
                                                                        <div class="row innerT">

                                                                            <div class="col-md-12">
                                                                                <div class="col-md-4">
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon bg-purple">โรคตามระบบ&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                        <asp:DropDownList ID="ddMedDrugGroupSearch" runat="server"  data-style="btn-default" CssClass="form-control selectpicker col-md-6" OnSelectedIndexChanged="ddMedDrugGroupSearch_SelectedIndexChanged" AutoPostBack="True">
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>





                                                                            </div>
                                                                        </div>

                                                                        <div class="row innerT">

                                                                            <div class="col-md-12">
                                                                                <div class="col-md-4">
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon bg-purple">สรรพคุณ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                        <asp:DropDownList ID="ddMedDrugTypeSearch" runat="server"  data-style="btn-default" CssClass="form-control selectpicker col-md-6">
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>


                                                                            </div>
                                                                        </div>

                                                                        <div class="row innerT">

                                                                            <div class="col-md-12">
                                                                                <div class="col-md-4">
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon bg-purple">รายการใช้งาน&nbsp;&nbsp;&nbsp;</span>
                                                                                        <asp:DropDownList ID="ddCancelDrugSearch" runat="server"  data-style="btn-default" CssClass="form-control selectpicker col-md-6">
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>


                                                                            </div>
                                                                        </div>

                                                                        <div class="row innerT">
                                                                            <div class="col-md-12">
                                                                                <div class="col-md-4">
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon bg-purple">ชื่อ หรือบาร์โค๊ด</span>
                                                                                        <asp:TextBox ID="txtBoxSearchName" runat="server" CssClass="form-control col-md-6 " placeholder="ค้นหาชื่อ หรือบาร์โค๊ด"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- /input-group -->
                                                                        </div>
                                                                        <div class="row innerTB">
                                                                            <div class="col-md-12">
                                                                                <div class="btn-group col-md-4">
                                                                                    <asp:LinkButton ID="btnSearch" CssClass="btn btn-success col-md-2" runat="server" OnClick="btnSearch_Click">ค้นหา</asp:LinkButton>

                                                                                    <asp:LinkButton ID="btnClearSearch" CssClass="btn btn-default col-md-2 btn" runat="server" OnClick="btnClearSearch_Click">ล้าง</asp:LinkButton>


                                                                                </div>

                                                                            </div>
                                                                            <!-- /input-group -->
                                                                        </div>







                                                                    </div>
                                                                </asp:Panel>
                                                                <asp:Label ID="txtLastUpdate" runat="server" Text="Label"></asp:Label>
                                                                <h5 class='innerAll margin-none bg-primary strong border-bottom'><i class='fa fa-fw fa-calendar'></i>&nbsp;&nbsp;ตารางข้อมูล</h5>
                                                                <asp:Label ID="txtTotalMedicines" runat="server" Text="Label"></asp:Label>


                                                                <div class="innerAll bg-gray">
                                                                    <!-- Total bookings & sort by options -->
                                                                    <div class="separator bottom">

                                                                        <span class="pull-right"></span>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                    <!-- // Total bookings & sort by options END -->

                                                                    <!-- Table -->



                                                                    <asp:GridView ID="GridView1" runat="server" OnRowCommand="GridView1_RowCommand" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs"
                                                                        PageSize="100" AllowPaging="True" Width="100%" Height="0px"
                                                                        OnPageIndexChanging="GridView1_PageIndexChanging">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="DRUGLIST_ID" HeaderText="รหัส" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" Visible="false">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="center" />
                                                                            </asp:BoundField>

                                                                            <asp:BoundField DataField="DRUGLIST_NAME" HeaderText="ชื่อยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="left" />
                                                                            </asp:BoundField>

                                                                            <asp:BoundField DataField="DRUGLIST_DESC" HeaderText="ชื่อสามัญ" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="left" />
                                                                            </asp:BoundField>

                                                                            <asp:BoundField DataField="DRUGGROUP_NAME" HeaderText="โรคตามระบบ" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="left" />
                                                                            </asp:BoundField>

                                                                            <asp:BoundField DataField="DRUGTYPE_NAME" HeaderText="สรรพคุณ" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="left" />
                                                                            </asp:BoundField>

                                                                            <asp:BoundField DataField="UNITLIST_NAME" HeaderText="หน่วยนับ" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="center" />
                                                                            </asp:BoundField>

                                                                            <asp:BoundField DataField="DRUGLIST_DOSE" HeaderText="ขนาด" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="right" />
                                                                            </asp:BoundField>

                                                                            <asp:BoundField DataField="UNITTYPE_NAME" HeaderText="น้ำหนัก" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="center" />
                                                                            </asp:BoundField>

                                                                            <asp:BoundField DataField="BARCODE_STANDARD" HeaderText="บาร์โค๊ด" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="center" />
                                                                            </asp:BoundField>


                                                                            <asp:TemplateField HeaderStyle-CssClass="center" HeaderText="" HeaderStyle-Width="15%">
                                                                                <ItemStyle CssClass="center" />
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemTemplate>
                                                                                    <div class="btn-group btn-group-sm">
                                                                                        <asp:LinkButton ID="btnRowView" runat="server" data-toggle="tooltip" data-original-title="ดูรายละเอียด" data-placement="top" CommandName="cmdView" CommandArgument='<%#Eval("DRUGLIST_ID") %>' CssClass="btn btn-default fa fa-eye" />
                                                                                        <asp:LinkButton ID="btnRowEdit" runat="server" data-toggle="tooltip" data-original-title="แก้ไข" data-placement="top" CommandName="cmdEdit" CommandArgument='<%#Eval("DRUGLIST_ID") %>' CssClass="btn btn-success fa fa-pencil" />
                                                                                        <asp:LinkButton ID="btnRowDelete" runat="server" data-toggle="tooltip" data-original-title="ลบ" data-placement="top" OnClientClick="if (!confirm('ยืนยันบันทึก !')) { return false; }" CommandName="cmdDel" CommandArgument='<%#Eval("DRUGLIST_ID") %>' CssClass="btn btn-danger fa fa-trash-o" />
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                        </Columns>




                                                                    </asp:GridView>






                                                                    <!-- // Table END -->

                                                                    <!-- With selected actions -->

                                                                    <!-- // With selected actions END -->

                                                                    <!-- Pagination -->

                                                                    <div class="clearfix"></div>
                                                                    <!-- // Pagination END -->
                                                                </div>
                                                                <div class="col-separator-h box"></div>

                                                            </div>
                                                        </div>

                                                        <div id="ascrail2000" class="nicescroll-rails" style="width: 5px; z-index: 2; cursor: default; position: absolute; top: 0px; left: 1073px; height: 432px; display: none; opacity: 0;">
                                                            <div style="position: relative; top: 0px; float: right; width: 5px; height: 432px; border: none; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px; background-color: rgb(235, 106, 90); background-clip: padding-box;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:HiddenField ID="hdfFunc" runat="server" />
            <asp:HiddenField ID="hdfDrugID" runat="server" />
            <asp:HiddenField ID="hdfRdoSearch" runat="server" />
            <asp:HiddenField ID="hdfDDGroupSearch" runat="server" />
            <asp:HiddenField ID="hdfDDTypeSearch" runat="server" />
            <asp:Panel ID="MedDrugOptionPanel" runat="server">
                <div class="row row-app">
                    <div class="col-md-12">
                        <div class="col-separator box col-separator-first col-unscrollable">
                            <div class="col-table">

                                <!-- Heading -->
                                <div class="heading-buttons border-bottom innerLR bg-primary">

                                    <asp:Label ID="lbSectionTitle" runat="server" Text="Label" CssClass="margin-none innerTB"></asp:Label>

                                    <div class="clearfix"></div>
                                </div>
                                <!-- // Heading END -->

                                <div class="col-table-row">
                                    <div class="col-app col-unscrollable">
                                        <div class="col-app">

                                            <!-- Widget -->
                                            <div class="widget widget-tabs border-none">

                                                <!-- Widget heading -->
                                                <div class="widget-head">
                                                    <ul>
                                                        <li class="active"><a href="#" data-toggle="tab" class="glyphicons font"><i></i>ข้อมูล</a></li>

                                                    </ul>
                                                </div>
                                                <!-- // Widget heading END -->

                                                <div class="widget-body">
                                                    <div class="tab-content">

                                                        <!-- Description -->
                                                        <div class="tab-pane active" id="productDescriptionTab">

                                                            <!-- Row -->
                                                            <br />
                                                            <div class="row">

                                                                <!-- Column -->
                                                                <div class="col-md-3">
                                                                    <strong>Barcode</strong>
                                                                    <p class="muted">เลขระบุตัวยา</p>
                                                                </div>
                                                                <!-- // Column END -->

                                                                <!-- Column -->

                                                                <div class="col-md-6">


                                                                    <asp:TextBox ID="txtBoxBarcode" runat="server" name="txtBoxBarcode" CssClass="form-control" placeholder="Barcode..."></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="1" runat="server" ControlToValidate="txtBoxBarcode" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="col-md-3">

                                                                    <asp:RequiredFieldValidator ValidationGroup="editGroupValidation" ID="rqfTxtBoxBarcode" ControlToValidate="txtBoxBarcode" runat="server" ErrorMessage=" ใส่บาร์โค๊ด" CssClass="btn btn-danger btn-sm fa fa-arrow-circle-down"></asp:RequiredFieldValidator>

                                                                </div>

                                                            </div>

                                                            <hr class="separator bottom" />
                                                            <div class="row">

                                                                <!-- Column -->
                                                                <div class="col-md-3">
                                                                    <strong>ชื่อยา</strong>
                                                                    <p class="muted">เพื่อใช้ระบุบนตัวยา</p>
                                                                </div>
                                                                <!-- // Column END -->

                                                                <!-- Column -->
                                                                <div class="col-md-6">


                                                                    <asp:TextBox ID="txtBoxMedName" runat="server" CssClass="form-control" placeholder="ชื่อยา..."></asp:TextBox>

                                                                </div>
                                                                <div class="col-md-3">

                                                                    <asp:RequiredFieldValidator ValidationGroup="editGroupValidation" ID="rqfTxtBoxMedName" ControlToValidate="txtBoxMedName" runat="server" ErrorMessage=" ใส่ชื่อยา" CssClass="btn btn-danger btn-sm fa fa-arrow-circle-down"></asp:RequiredFieldValidator>

                                                                </div>
                                                                <!-- // Column END -->

                                                            </div>
                                                            <!-- // Row END -->

                                                            <hr class="separator bottom" />

                                                            <!-- Row -->
                                                            <div class="row">

                                                                <!-- Column -->
                                                                <div class="col-md-3">
                                                                    <strong>ชื่อสามัญ</strong>
                                                                    <p class="muted">เพื่อใช้ระบุบนตัวยา</p>
                                                                </div>
                                                                <!-- // Column END -->

                                                                <!-- Column -->
                                                                <div class="col-md-6">
                                                                    <asp:TextBox ID="txtBoxMedDesc" runat="server" CssClass="form-control col-md-6" placeholder="ชื่อสามัญ..."></asp:TextBox>

                                                                </div>
                                                                <div class="col-md-3">

                                                                    <asp:RequiredFieldValidator ValidationGroup="editGroupValidation" ID="rqfTxtBoxMedDesc" ControlToValidate="txtBoxMedDesc" runat="server" ErrorMessage=" ใส่ชื่อยาสามัญ" CssClass="btn btn-danger btn-sm fa fa-arrow-circle-down"></asp:RequiredFieldValidator>

                                                                </div>
                                                                <!-- // Column END -->

                                                            </div>
                                                            <!-- // Row END -->

                                                            <hr class="separator bottom" />
                                                            <!-- Row -->
                                                            <div class="row">

                                                                <!-- Column -->
                                                                <div class="col-md-3">
                                                                    <strong>ชื่อโรคตามระบบ</strong>
                                                                    <p class="muted">เพื่อใช้ระบุบนตัวยา</p>
                                                                </div>
                                                                <!-- // Column END -->

                                                                <!-- Column -->

                                                                <div class="col-md-6">
                                                                    <asp:DropDownList ID="ddMedGroup" runat="server"  data-style="btn-default" CssClass="form-control selectpicker">
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="col-md-3">

                                                                    <asp:RequiredFieldValidator ValidationGroup="editGroupValidation" ID="rqfDdMedGroup" ControlToValidate="ddMedGroup" InitialValue="" runat="server" ErrorMessage=" เลือกโรคตามระบบ" CssClass="btn btn-danger btn-sm fa fa-arrow-circle-down"></asp:RequiredFieldValidator>

                                                                </div>

                                                            </div>
                                                            <!-- // Row END -->

                                                            <hr class="separator bottom" />
                                                            <!-- Row -->
                                                            <div class="row">

                                                                <!-- Column -->
                                                                <div class="col-md-3">
                                                                    <strong>สรรพคุณ</strong>
                                                                    <p class="muted">เพื่อใช้ระบุบนตัวยา</p>
                                                                </div>
                                                                <!-- // Column END -->

                                                                <!-- Column -->


                                                                <div class="col-md-6">
                                                                    <asp:DropDownList ID="ddMedType" runat="server"  data-style="btn-default" CssClass="form-control selectpicker">
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="col-md-3">

                                                                    <asp:RequiredFieldValidator ValidationGroup="editGroupValidation" ID="rqfDdMedType" ControlToValidate="ddMedType" InitialValue="" runat="server" ErrorMessage=" เลือกสรรพคุณ" CssClass="btn btn-danger btn-sm fa fa-arrow-circle-down"></asp:RequiredFieldValidator>

                                                                </div>


                                                                <!-- // Column END -->

                                                            </div>
                                                            <!-- // Row END -->

                                                            <hr class="separator bottom" />
                                                            <!-- Row -->
                                                            <div class="row">

                                                                <!-- Column -->
                                                                <div class="col-md-3">
                                                                    <strong>ขนาด</strong>
                                                                    <p class="muted">เพื่อใช้ระบุบนตัวยา</p>
                                                                </div>
                                                                <!-- // Column END -->

                                                                <!-- Column -->

                                                                <div class="col-md-6">

                                                                    <asp:TextBox ID="txtBoxDrugDose" runat="server" CssClass="form-control col-md-3" placeholder="ปริมาณ..." autocomplete="off"></asp:TextBox>
                                                                    <%--<asp:MaskedEditExtender ID="txtBoxDrugDose_MaskedEditExtender" runat="server" Century="2000" 
                                                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                                                            CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" 
                                                                            CultureTimePlaceholder="" Enabled="True" Mask="999999" 
                                                                            TargetControlID="txtBoxDrugDose" AutoComplete="False" 
                                                                            ClearMaskOnLostFocus="False" PromptCharacter="">
                                                                        </asp:MaskedEditExtender>--%>
                                                                </div>
                                                                <div class="col-md-3">

                                                                    <asp:RequiredFieldValidator ValidationGroup="editGroupValidation" ID="rqfTxtBoxDrugDose" ControlToValidate="txtBoxDrugDose" runat="server" ErrorMessage=" ใส่ปริมาณยา" CssClass="btn btn-danger btn-sm fa fa-arrow-circle-down"></asp:RequiredFieldValidator>

                                                                </div>




                                                                <!-- // Column END -->

                                                            </div>
                                                            <!-- // Row END -->
                                                            <hr class="separator bottom" />
                                                            <div class="row">

                                                                <!-- Column -->
                                                                <div class="col-md-3">
                                                                    <strong>หน่วยปริมาตร</strong>
                                                                    <p class="muted">เพื่อใช้ระบุบนตัวยา</p>
                                                                </div>


                                                                <div class="col-md-6">
                                                                    <asp:DropDownList ID="ddMedUnitType" runat="server"  data-style="btn-default" CssClass="form-control selectpicker">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="col-md-3">

                                                                    <asp:RequiredFieldValidator ValidationGroup="editGroupValidation" ID="rqfDdMedUnitType" ControlToValidate="ddMedUnitType" InitialValue="" runat="server" ErrorMessage=" เลือกปริมาตรยา" CssClass="btn btn-danger btn-sm fa fa-arrow-circle-down"></asp:RequiredFieldValidator>

                                                                </div>


                                                                <!-- // Column END -->

                                                            </div>
                                                            <hr class="separator bottom" />
                                                            <!-- Row -->
                                                            <div class="row">

                                                                <!-- Column -->
                                                                <div class="col-md-3">
                                                                    <strong>หน่วยนับ</strong>
                                                                    <p class="muted">เพื่อใช้ระบุบนตัวยา</p>
                                                                </div>
                                                                <!-- // Column END -->

                                                                <!-- Column -->

                                                                <div class="col-md-6">
                                                                    <asp:DropDownList ID="ddMedUnitList" runat="server"  data-style="btn-default" CssClass="form-control selectpicker">
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <!-- // Column END -->
                                                                <div class="col-md-3">

                                                                    <asp:RequiredFieldValidator ValidationGroup="editGroupValidation" ID="rqfDdMedUnitList" ControlToValidate="ddMedUnitList" InitialValue="" runat="server" ErrorMessage=" เลือกหน่วยนับ" CssClass="btn btn-danger btn-sm fa fa-arrow-circle-down"></asp:RequiredFieldValidator>

                                                                </div>
                                                            </div>

                                                            <!-- // Row END -->
                                                            <!-- Row -->
                                                            <!-- // Row END -->

                                                            <hr class="separator bottom" />
                                                            <!-- Row -->
                                                            <div class="row">

                                                                <!-- Column -->
                                                                <div class="col-md-3">
                                                                    <strong>ยกเลิกรายการยา</strong>
                                                                    <p class="muted">เเพื่อใช้ระบุการใช้ยา</p>
                                                                </div>
                                                                <!-- // Column END -->

                                                                <!-- Column -->

                                                                <div class="col-md-6">

                                                                    <asp:DropDownList ID="ddMedCancelList" runat="server"  data-style="btn-default" CssClass="form-control selectpicker">
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="col-md-3">

                                                                    <asp:RequiredFieldValidator ValidationGroup="editGroupValidation" ID="rqfDdMedCancelList" ControlToValidate="ddMedCancelList" InitialValue="" runat="server" ErrorMessage=" เลือกการใช้งาน" CssClass="btn btn-danger btn-sm fa fa-arrow-circle-down"></asp:RequiredFieldValidator>

                                                                </div>
                                                            </div>
                                                            <!-- // Column END -->





                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                            <!-- // Widget END -->

                                            <div class="text-right innerAll border-top">
                                                <asp:Panel ID="pnButtonEditPage" runat="server">
                                                    <div class="btn-group btn-group">

                                                        <asp:LinkButton ID="btnClear" runat="server" CssClass="btn btn-default" OnClick="btnClear_Click"><i class="fa fa-fw fa-edit"></i> ล้าง</asp:LinkButton>
                                                        <asp:LinkButton ID="btnDiscard" runat="server" CssClass="btn btn-primary" OnClick="btnDiscard_Click"><i class="fa fa-fw fa-trash-o"></i> ยกเลิก</asp:LinkButton>
                                                        <asp:LinkButton ID="btnSave" runat="server" OnClientClick="if (!confirm('ยืนยันบันทึก !')) { return false; }" ValidationGroup="editGroupValidation" CssClass="btn btn-success" OnClick="btnSave_Click"><i class="fa fa-fw fa-save"></i> บันทึก</asp:LinkButton>


                                                    </div>
                                                </asp:Panel>

                                                <asp:Panel ID="pnButtonViewPage" runat="server">
                                                    <div class="btn-group btn-group">


                                                        <asp:LinkButton ID="btnEdit" runat="server" CssClass="btn btn-primary" OnClick="btnEdit_Click"><i class="fa fa-fw fa-pencil"></i> แก้ไข</asp:LinkButton>
                                                        <asp:LinkButton ID="btnConfirm" runat="server" CssClass="btn btn-success" OnClick="btnConfirm_Click"><i class="fa fa-fw fa-check-circle-o"></i> ตกลง</asp:LinkButton>

                                                    </div>
                                                </asp:Panel>
                                            </div>





                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </asp:Panel>
            <asp:Panel ID="MedDrugErrorPanel" runat="server">
                <div class="row error">
                    <div class="col-md-4 col-md-offset-1 center">
                        <div class="center">
                            <img src="../assets//images/error-icon-bucket.png " class="error-icon" />
                        </div>
                    </div>
                    <div class="col-md-5 content center">
                        <h1 class="strong">Oups!</h1>
                        <h4 class="innerB">This page does not exist.</h4>
                        <div class="well">
                            ERROR MESSAGE GOES HERE!
                        </div>
                    </div>
                </div>
            </asp:Panel>
         </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


