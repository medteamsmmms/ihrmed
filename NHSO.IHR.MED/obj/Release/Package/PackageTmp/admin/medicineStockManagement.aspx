﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Admin.Master" AutoEventWireup="true" CodeBehind="medicineStockManagement.aspx.cs" Inherits="NHSO.IHR.MED.admin.medicineStockManagement" EnableEventValidation = "false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1">
    <ul class="main pull-left">
        <li class="dropdown">
            <a href="medicineWithdrawManagement.aspx"><i class="fa fa-fw fa-home"></i>หน้าหลัก </a>

        </li>
    </ul>
</asp:Content>


<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder2">

    <script type="text/javascript">
        $(document).ready(function () {
            var pressed = false;
            var chars = [];
            $(window).keypress(function (e) {
                if (e.which >= 48 && e.which <= 57) {
                    chars.push(String.fromCharCode(e.which));
                }
                if (pressed == false) {
                    setTimeout(function () {
                        if (chars.length >= 10) {
                            var barcode = chars.join("");
                            // assign value to some input (or do whatever you want)
                            $("#<%= txtBoxSearchName.ClientID %>").val(barcode);
                           $('#<%= Button1.ClientID %>').click();

                       }
                       chars = [];
                       pressed = false;
                   }, 500);
               }
               pressed = true;
           });
       });
       $("#<%= txtBoxSearchName.ClientID %>").keypress(function (e) {
            if (e.which === 13) {
                e.preventDefault();
            }
       });

        //---- Add active Class-----//
        document.getElementById("Stock").className += " li-active active";
        document.getElementById("iStock").className += " active";

    </script>

    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Button ID="Button1" Style="display: none;" runat="server" Text="Button" OnClick="Button1_Click" />

            <asp:Panel ID="MedDrugListPanel" runat="server">
                <div class="row row-app" id="MedDrugListDiv">
                    <div class="col-md-12 animated fadeInDown">
                        <div class="col-separator col-separator-first col-unscrollable">
                            <div class="col-table">
                                <div class="heading-buttons innerLR box">
                                    <h4 class="margin-none innerTB pull-left">สต๊อกยา</h4>

                                    <%--<asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn-xs pull-right btn btn-primary" OnClick="btnAdd_Click"><i class="fa fa-fw fa-plus"></i>Add record</asp:LinkButton>--%>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-separator-h"></div>
                                <div class="col-table-row">
                                    <div class="col-app col-unscrollable">
                                        <div class="col-app hasNiceScroll animated fadeInDown" tabindex="10000" style="overflow-y: hidden; outline: none;">

                                            <div class="col-table">

                                                <div class="col-separator-h box"></div>

                                                <div class="col-table-row">
                                                    <div class="row-app">
                                                        <div>
                                                            <div class="col-separator box bg-gray hasNiceScroll">
                                                                <h5 class="innerAll margin-none bg-primary strong border-bottom"><i class="fa fa-fw fa-search"></i>&nbsp;ค้นหา</h5>
                                                                <asp:Panel ID="Panel1" DefaultButton="btnSearch" runat="server">
                                                                    <div class="innerAll margin-none bg-gray strong border-bottom">
                                                                        <div class="row innerT">

                                                                            <div class="col-md-12">
                                                                                <div class="col-md-4">
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon bg-purple">โรคตามระบบ&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                        <asp:DropDownList ID="ddMedDrugGroupSearch" runat="server" data-style="btn-default" CssClass="form-control selectpicker col-md-6" OnSelectedIndexChanged="ddMedDrugGroupSearch_SelectedIndexChanged" AutoPostBack="True">
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row innerT">
                                                                            <div class="col-md-12">
                                                                                <div class="col-md-4">
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon bg-purple">สรรพคุณ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                        <asp:DropDownList ID="ddMedDrugTypeSearch" runat="server" data-style="btn-default" CssClass="form-control selectpicker col-md-6">
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row innerT">
                                                                            <div class="col-md-12">
                                                                                <div class="col-md-2">
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon bg-purple">มากกว่า</span>
                                                                                        <asp:TextBox ID="txtBoxLowerNumber" runat="server" CssClass="form-control" placeholder="จำนวน ..."></asp:TextBox>
                                                                                        <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" AutoComplete="False" Century="2000"
                                                                                             CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                                                             CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                                                             CultureTimePlaceholder="" Enabled="True" Mask="999999" PromptCharacter="" 
                                                                                            TargetControlID="txtBoxLowerNumber">
                                                                                        </asp:MaskedEditExtender>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-2">
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon bg-purple">น้อยกว่า</span>
                                                                                        <asp:TextBox ID="txtBoxGreaterNumber" runat="server" CssClass="form-control" placeholder="จำนวน ..."></asp:TextBox>
                                                                                        <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" AutoComplete="False" Century="2000" 
                                                                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" 
                                                                                            CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                                                             CultureTimePlaceholder="" Enabled="True" Mask="999999" PromptCharacter=""
                                                                                             TargetControlID="txtBoxGreaterNumber">
                                                                                        </asp:MaskedEditExtender>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row innerT">
                                                                            <div class="col-md-12">
                                                                                <div class="col-md-4">
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon bg-purple">รายการใช้งาน&nbsp;&nbsp;&nbsp;</span>
                                                                                        <asp:DropDownList ID="ddCancelDrugSearch" runat="server" data-style="btn-default" CssClass="form-control selectpicker col-md-6">
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row innerT">
                                                                            <div class="col-md-12">
                                                                                <div class="col-md-4">
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon bg-purple">ชื่อ หรือบาร์โค๊ด</span>
                                                                                        <asp:TextBox ID="txtBoxSearchName" runat="server" CssClass="form-control col-md-6 " placeholder="ค้นหาชื่อ หรือบาร์โค๊ด"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- /input-group -->
                                                                        </div>
                                                                        <div class="row innerTB">
                                                                            <div class="col-md-12">
                                                                                <div class="btn-group col-md-4">
                                                                                    <asp:LinkButton ID="btnSearch" CssClass="btn btn-success col-md-2" runat="server" OnClick="btnSearch_Click">ค้นหา</asp:LinkButton>
                                                                                    <asp:LinkButton ID="btnClearSearch" CssClass="btn btn-default col-md-2 btn" runat="server" OnClick="btnClearSearch_Click">ล้าง</asp:LinkButton>
                                                                                </div>

                                                                            </div>
                                                                            <!-- /input-group -->
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                                <asp:Label ID="txtLastUpdate" runat="server" Text="Label"></asp:Label>
                                                                <h5 class='innerAll margin-none bg-primary strong border-bottom'><i class='fa fa-fw fa-calendar'></i>&nbsp;&nbsp;ตารางข้อมูล</h5>
                                                                <asp:Label ID="txtTotalMedicines" runat="server" Text="Label"></asp:Label>

                                                                <div class="innerAll bg-gray">
                                                                    <!-- Total bookings & sort by options -->
                                                                    <div class="separator bottom">

                                                                        <span class="pull-right"></span>
                                                                        <div class="clearfix"></div>
                                                                         <asp:LinkButton ID="lbToExcel" CssClass="pull-right btn btn-success" runat="server" OnClick="lbToExcel_Click" >นำข้อมูลออก</asp:LinkButton>
                                                                    </div>
                                                                    <br />
                                                                    <br />
                                                                    <asp:GridView ID="GridView1" runat="server" OnRowCommand="GridView1_RowCommand" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs"
                                                                        PageSize="100" AllowPaging="True" Width="100%" Height="0px"
                                                                        OnPageIndexChanging="GridView1_PageIndexChanging">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="DRUGLIST_ID" HeaderText="รหัส" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" Visible="false">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="center" />
                                                                            </asp:BoundField>

                                                                            <asp:BoundField DataField="DRUGLIST_NAME" HeaderText="ชื่อยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="left" />
                                                                            </asp:BoundField>

                                                                            <asp:BoundField DataField="DRUGLIST_DESC" HeaderText="ชื่อสามัญ" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="left" />
                                                                            </asp:BoundField>


                                                                            <asp:BoundField DataField="DRUGGROUP_NAME" HeaderText="โรคตามระบบ" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="left" />
                                                                            </asp:BoundField>

                                                                            <asp:BoundField DataField="DRUGTYPE_NAME" HeaderText="สรรพคุณ" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="left" />
                                                                            </asp:BoundField>

                                                                            <asp:BoundField DataField="STOCK_BALANCE" HeaderText="คงเหลือ" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="right" />
                                                                            </asp:BoundField>

                                                                            <asp:BoundField DataField="UNITLIST_NAME" HeaderText="หน่วย" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                <HeaderStyle CssClass="center" />
                                                                                <ItemStyle CssClass="center" />
                                                                            </asp:BoundField>
                                                                            <asp:TemplateField HeaderStyle-CssClass="center" HeaderText="" HeaderStyle-Width="15%">
                                                                                <ItemStyle CssClass="center" />
                                                                                <ItemTemplate>
                                                                                    <div class="btn-group btn-group-sm">
                                                                                        <asp:LinkButton ID="btnRowView" runat="server" data-toggle="tooltip" data-original-title="ดูรายละเอียด" data-placement="top" CommandName="cmdView" CommandArgument='<%#Eval("DRUGLIST_ID") %>' CssClass="btn btn-default fa fa-eye" />
                                                                                        <asp:LinkButton ID="btnRowEdit" runat="server" data-toggle="tooltip" data-original-title="แก้ไข" data-placement="top" CommandName="cmdEdit" CommandArgument='<%#Eval("DRUGLIST_ID") %>' CssClass="btn btn-success fa fa-pencil" />
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                <div class="col-separator-h box"></div>
                                                            </div>
                                                        </div>
                                                        <div id="ascrail2000" class="nicescroll-rails" style="width: 5px; z-index: 2; cursor: default; position: absolute; top: 0px; left: 1073px; height: 432px; display: none; opacity: 0;">
                                                            <div style="position: relative; top: 0px; float: right; width: 5px; height: 432px; border: none; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px; background-color: rgb(235, 106, 90); background-clip: padding-box;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:HiddenField ID="hdfFunc" runat="server" />
            <asp:HiddenField ID="hdfDrugID" runat="server" />
            <asp:HiddenField ID="hdfDrugIndex" runat="server" />
            <asp:HiddenField ID="hdfRdoSearch" runat="server" />
            <asp:HiddenField ID="hdfDDGroupSearch" runat="server" />
            <asp:HiddenField ID="hdfDDTypeSearch" runat="server" />
            <asp:HiddenField ID="hdfLowerNumber" runat="server" />
            <asp:HiddenField ID="hdfGreaterNumber" runat="server" />

            <asp:Panel ID="MedDrugOptionPanel" runat="server">
                <div class="row row-app">
                    <div class="col-md-12">
                        <div class="col-separator box col-separator-first col-unscrollable">

                            <div class="col-table">

                                <!-- Heading -->
                                <div class="heading-buttons border-bottom innerLR bg-primary">

                                    <asp:Label ID="lbSectionTitle" runat="server" Text="Label" CssClass="margin-none innerTB"></asp:Label>

                                    <div class="clearfix"></div>
                                </div>
                                <!-- // Heading END -->

                                <div class="col-table-row">
                                    <div class="col-app col-unscrollable">
                                        <div class="col-app">

                                            <!-- Widget -->
                                            <div class="widget widget-tabs border-none">

                                                <!-- Widget heading -->
                                                <div class="widget-head">
                                                    <ul>
                                                        <li class="active"><a href="#" data-toggle="tab" class="glyphicons font"><i></i>ข้อมูล</a></li>

                                                    </ul>
                                                </div>
                                                <!-- // Widget heading END -->

                                                <div class="widget-body">
                                                    <div class="tab-content">

                                                        <!-- Description -->
                                                        <div class="tab-pane active" id="productDescriptionTab">

                                                            <!-- Row -->
                                                            <br />
                                                            <div class="row">

                                                                <!-- Column -->
                                                                <div class="col-md-3">
                                                                    <strong>คงเหลือ</strong>
                                                                    <p class="muted">จำนวนยาคงเหลือ</p>
                                                                </div>
                                                                <!-- // Column END -->

                                                                <!-- Column -->

                                                                <div class="col-md-6">

                                                                    <asp:TextBox ID="txtBoxAmount" runat="server" CssClass="form-control" placeholder="คงเหลือ..."></asp:TextBox>

                                                                </div>

                                                            </div>

                                                            <hr class="separator bottom" />
                                                            <asp:Panel ID="pnShowAddedValue" runat="server">
                                                                <div class="row">

                                                                    <!-- Column -->
                                                                    <div class="col-md-3">
                                                                        <strong>ประเภทการจำหน่าย</strong>
                                                                        <p class="muted">เพื่อบันทึกจำนวนยา</p>
                                                                    </div>
                                                                    <!-- // Column END -->

                                                                    <!-- Column -->
                                                                    <div class="col-md-9">



                                                                        <div class="btn-group" data-toggle="buttons">
                                                                            <asp:RadioButton ID="rdoIn" runat="server" GroupName="rdoBarOption" Checked="true" CssClass="btn btn-primary active" Text="นำเข้า" />
                                                                            <asp:RadioButton ID="rdoOut" runat="server" GroupName="rdoBarOption" CssClass="btn btn-primary" Text="นำออก" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr class="separator bottom" />
                                                            </asp:Panel>
                                                            <asp:Panel ID="pnShowAmountValue" runat="server">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <strong>จำนวน</strong>
                                                                        <p class="muted">จำนวนยาสู่ระบบ</p>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <asp:TextBox ID="txtBoxValue" runat="server" CssClass="form-control" placeholder="จำนวน..."></asp:TextBox>
                                                                        <asp:MaskedEditExtender ID="txtamont_MaskedEditExtender" runat="server" AutoComplete="False" Century="2000" 
                                                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" 
                                                                            CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                                                             Enabled="True" Mask="999999" PromptCharacter="" TargetControlID="txtBoxValue">
                                                                        </asp:MaskedEditExtender>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <asp:RequiredFieldValidator ValidationGroup="editStockGroupValidation" ID="rqfTxtBoxValue" ControlToValidate="txtBoxValue" runat="server" ErrorMessage=" ใส่จำนวนที่ต้องการ" CssClass="btn btn-danger btn-sm fa fa-arrow-circle-down"></asp:RequiredFieldValidator>
                                                                    </div>
                                                                </div>
                                                                <!-- Row -->
                                                                <hr class="separator bottom" />
                                                            </asp:Panel>
                                                            <div class="row">

                                                                <!-- Column -->
                                                                <div class="col-md-3">
                                                                    <strong>หน่วยนับ</strong>
                                                                    <p class="muted">หน่วยในการนับ</p>
                                                                </div>
                                                                <!-- // Column END -->


                                                                <div class="col-md-3">

                                                                    <asp:TextBox ID="txtBoxUnitList" runat="server" CssClass="form-control" placeholder="หน่วย..."></asp:TextBox>

                                                                </div>


                                                            </div>




                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                            <!-- // Widget END -->

                                            <div class="text-right innerAll border-top">
                                                <asp:Panel ID="pnButtonEditPage" runat="server">
                                                    <div class="btn-group btn-group">

                                                        <asp:LinkButton ID="btnClear" runat="server" CssClass="btn btn-default" OnClick="btnClear_Click"><i class="fa fa-fw fa-edit"></i> ล้าง</asp:LinkButton>
                                                        <asp:LinkButton ID="btnDiscard" runat="server" CssClass="btn btn-primary" OnClick="btnDiscard_Click"><i class="fa fa-fw fa-trash-o"></i> ยกเลิก</asp:LinkButton>
                                                        <asp:LinkButton ID="btnSave" runat="server" OnClientClick="if (!confirm('ยืนยันบันทึก !')) { return false; }" ValidationGroup="editStockGroupValidation" CssClass="btn btn-success" OnClick="btnSave_Click"><i class="fa fa-fw fa-save"></i> บันทึก</asp:LinkButton>


                                                    </div>
                                                </asp:Panel>

                                                <asp:Panel ID="pnButtonViewPage" runat="server">
                                                    <div class="btn-group btn-group">


                                                        <asp:LinkButton ID="btnEdit" runat="server" CssClass="btn btn-primary" OnClick="btnEdit_Click"><i class="fa fa-fw fa-pencil"></i> แก้ไข</asp:LinkButton>
                                                        <asp:LinkButton ID="btnConfirm" runat="server" CssClass="btn btn-success" OnClick="btnConfirm_Click"><i class="fa fa-fw fa-check-circle-o"></i> ตกลง</asp:LinkButton>

                                                    </div>
                                                </asp:Panel>
                                            </div>




                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </asp:Panel>

            <asp:Panel ID="MedDrugErrorPanel" runat="server">
                <div class="row error">
                    <div class="col-md-4 col-md-offset-1 center">
                        <div class="center">
                            <img src="../assets//images/error-icon-bucket.png " class="error-icon" />
                        </div>
                    </div>
                    <div class="col-md-5 content center">
                        <h1 class="strong">Oups!</h1>
                        <h4 class="innerB">This page does not exist.</h4>
                        <div class="well">
                            UNDER CONSTRUCTION !
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
           <Triggers>
            <asp:PostBackTrigger ControlID="lbToExcel" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

