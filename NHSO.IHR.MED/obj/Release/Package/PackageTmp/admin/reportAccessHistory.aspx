﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/Admin.Master" AutoEventWireup="true" CodeBehind="reportAccessHistory.aspx.cs" Inherits="NHSO.IHR.MED.admin.reportAccessHistory" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Usercontrols/ucTextDateValidate.ascx" TagPrefix="uc1" TagName="ucTextDateValidate" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1">
    <ul class="main pull-left">
        <li class="dropdown">
            <a href="medicineWithdrawManagement.aspx"><i class="fa fa-fw fa-home"></i>หน้าหลัก </a>
        </li>
    </ul>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {
            $("#txtSelectDateBegins").datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: "linked",
                language: "th",
                todayHighlight: true,
                autoclose: true
            });
            $("#txtSelectDateEnds").datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: "linked",
                language: "th",
                todayHighlight: true,
                autoclose: true
            });
        });

        //---- Add active Class-----//
        document.getElementById("HasReport").className += " li-active active";
        document.getElementById("SReport").style.borderColor = "#4a8bc2";
        document.getElementById("SReport").style.background = "#314250";
        document.getElementById("SReport").style.color = "white";
        document.getElementById("iReport").className += " active";
        document.getElementById("AccessHistory").className += " active";
        document.getElementById("sidebar-report").className += " in";

    </script>
</asp:Content>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="ContentPlaceHolder2">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
        <ContentTemplate>
            <asp:Panel ID="MedDrugListPanel" runat="server">
                <div class="row row-app" id="MedDrugListDiv">
                    <div class="col-md-12 animated fadeInDown">
                        <div class="col-separator col-separator-first col-unscrollable">
                            <div class="col-table">
                                <div class="heading-buttons innerLR box">
                                    <h4 class="margin-none innerTB pull-left">รายงานการออกฉลากยา</h4>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-separator-h"></div>
                                <div class="col-table-row">
                                    <div class="col-app col-unscrollable">
                                        <div class="col-app hasNiceScroll animated fadeInDown" tabindex="10000" style="overflow-y: hidden; outline: none;">
                                            <div class="col-table">
                                                <div class="col-separator-h box"></div>
                                                <div class="col-table-row">
                                                    <div class="row-app">
                                                        <div>
                                                            <div class="col-separator box bg-gray hasNiceScroll">
                                                                <h5 class="innerAll margin-none bg-primary strong border-bottom"><i class="fa fa-fw fa-search"></i>&nbsp;ค้นหา</h5>
                                                                <asp:Panel ID="Panel1" DefaultButton="btnSearch" runat="server">
                                                                    <div class="innerAll margin-none bg-gray strong border-bottom">
                                                                        <div class="row innerT">
                                                                            <div class="col-md-12">
                                                                                <div class="col-md-6">
                                                                                    <div class="input-group">
                                                                                        <span class="input-group-addon bg-purple">สำนัก&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                        <asp:DropDownList ID="ddDepSearch" runat="server" data-style="btn-default" CssClass="form-control selectpicker col-md-6">
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div class="row innerT">
                                                                            <div class="col-md-12">
                                                                                <div class="col-md-3">
                                                                                    <div class="input-group input-group date" id="txtSelectDateBegins">
                                                                                        <span class="input-group-addon bg-purple">วันเริ่มต้น&nbsp;</span>
                                                                                        <asp:TextBox ID="txtSelectDateBegin1" runat="server" class="form-control" disabled></asp:TextBox>
                                                                                        <span class="input-group-addon"><i class="fa fa-th"></i></span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3">
                                                                                    <div class="input-group input-group date" id="txtSelectDateEnds">
                                                                                        <span class="input-group-addon bg-purple">วันสิ้นสุด&nbsp;</span>
                                                                                        <asp:TextBox ID="txtSelectDateEnd1" runat="server" class="form-control" disabled></asp:TextBox>
                                                                                        <span class="input-group-addon"><i class="fa fa-th"></i></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row innerT">
                                                                            <div class="col-md-12">
                                                                                <div class="col-md-6">
                                                                                    <div class="input-group input-group col-md-12">
                                                                                        <span class="input-group-addon bg-purple">ค้นหาจาก</span>
                                                                                        <div class="input-group-btn ">
                                                                                            <asp:DropDownList ID="ddSearchBy" runat="server" data-toggle="dropdown" CssClass="btn btn-default dropdown-toggle">
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                        <div class="input-group input-group col-md-12">
                                                                                            <asp:TextBox ID="txtBoxSearchName" runat="server" CssClass="form-control" placeholder="คำค้นหา..."></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row innerTB">
                                                                            <div class="col-md-12">
                                                                                <div class="btn-group col-md-4">
                                                                                    <asp:LinkButton ID="btnSearch" CssClass="btn btn-success col-md-2" runat="server" OnClick="btnSearch_Click">ค้นหา</asp:LinkButton>
                                                                                    <asp:LinkButton ID="btnClearSearch" CssClass="btn btn-default col-md-2 btn" runat="server" OnClick="btnClearSearch_Click">ล้าง</asp:LinkButton>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>


                                                                <asp:Label ID="txtLastUpdate" runat="server" Text="Label"></asp:Label>
                                                                <h5 class='innerAll margin-none bg-primary strong border-bottom'><i class='fa fa-fw fa-calendar'></i>&nbsp;&nbsp;ตารางข้อมูล</h5>
                                                                <asp:Label ID="lbResult" runat="server" Text="Label"></asp:Label>

                                                                <div class="innerAll bg-gray">
                                                                    <!-- Total bookings & sort by options -->
                                                                    <div class="separator bottom">
                                                                        <span class="pull-right"></span>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                    <!-- // Total bookings & sort by options END -->
                                                                    <!-- Table -->
                                                                    <asp:Panel ID="pnGridview1" runat="server">
                                                                        <asp:GridView ID="GridView1" runat="server" OnRowCommand="GridView1_RowCommand" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs"
                                                                            PageSize="100" AllowPaging="True" Width="100%" Height="0px"
                                                                            OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound">
                                                                            <Columns>
                                                                                <asp:BoundField DataField="DRUGBRING_ORDER" HeaderText="เลขที่เบิกยา" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="center" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="EMP_ID" HeaderText="รหัสพนักงาน" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="left" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="EMP_THNAME" HeaderText="ชื่อ" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="left" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="EMP_BUREAU" HeaderText="สำนัก" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="left" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="MEDICINE_AMOUNT" HeaderText="จำนวน" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="right" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="DRUGBRING_ORDER" Visible="false" HeaderText="เลขที่เบิกยา" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="center" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="CREATE_DATE" HeaderText="วันที่เบิกยา" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="center" />
                                                                                </asp:BoundField>

                                                                                <asp:TemplateField HeaderStyle-CssClass="center" HeaderText="" HeaderStyle-Width="15%">
                                                                                    <ItemStyle CssClass="center" />
                                                                                    <ItemTemplate>
                                                                                        <div class="btn-group btn-group-sm">
                                                                                            <asp:LinkButton ID="btnRowView" data-toggle="tooltip" data-original-title="ดูรายละเอียด" data-placement="top" runat="server" CommandName="viewDrugBringFunc" CommandArgument='<%#Eval("DRUGBRING_ID") %>' CssClass="btn btn-default fa fa-eye" />
                                                                                            <asp:LinkButton ID="btnRowPrint" data-toggle="tooltip" data-original-title="Print" data-placement="top" OnClientClick="return confirm('ยืนยันการพิมพ์');" runat="server" CommandName="cmdPrint" CommandArgument='<%#Eval("DRUGBRING_ID") %>' CssClass="btn btn-warning fa fa-print" />

                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                    <asp:Panel ID="pnGridView2" runat="server">
                                                                        <asp:GridView ID="GridView2" runat="server" OnRowCommand="GridView2_RowCommand" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs"
                                                                            PageSize="100" AllowPaging="True" Width="100%" Height="0px"
                                                                            OnPageIndexChanging="GridView2_PageIndexChanging">
                                                                            <Columns>
                                                                                <asp:BoundField DataField="DRUGBRING_ORDER" HeaderText="รหัส" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="center" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="EMP_ID" HeaderText="รหัส" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" Visible="false">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="center" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="EMP_THNAME" HeaderText="ชื่อพนักงาน" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" Visible="false">
                                                                                    <HeaderStyle CssClass="center" />

                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="EMP_BUREAU" HeaderText="สำนัก" Visible="false" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="left" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="DRUGLIST_INDEX" HeaderText="ชื่อยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" Visible="false">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="DRUGLIST_NAME" HeaderText="ชื่อยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="LABEL_DESC" HeaderText="ฉลากยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="DRUGBRING_ORDER" Visible ="false" HeaderText="เลขที่เบิกยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="CREATE_DATE" HeaderText="วันที่เบิกยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="center" />
                                                                                </asp:BoundField>

                                                                                <asp:TemplateField HeaderStyle-CssClass="center" HeaderText="" HeaderStyle-Width="10%">
                                                                                    <ItemStyle CssClass="center" />
                                                                                    <ItemTemplate>
                                                                                        <div class="btn-group btn-group-sm">
                                                                                            <asp:LinkButton ID="btnRowPrint" data-toggle="tooltip" data-original-title="Print" data-placement="top" runat="server" OnClientClick="return confirm('ยืนยันการพิมพ์');" CommandName="cmdPrint" CommandArgument='<%#Eval("DRUGLIST_INDEX") %>' CssClass="btn btn-warning fa fa-print" />
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>

                                                                    </asp:Panel>
                                                                    <asp:Panel ID="pnGridView3" runat="server">
                                                                        <asp:GridView ID="GridView3" runat="server" OnRowCommand="GridView3_RowCommand" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed table-striped table-primary table-vertical-center checkboxs"
                                                                            PageSize="100" AllowPaging="True" Width="100%" Height="0px"
                                                                            OnPageIndexChanging="GridView3_PageIndexChanging">
                                                                            <Columns>
                                                                                <asp:BoundField DataField="DRUGBRING_ORDER" HeaderText="รหัส" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="center" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="EMP_ID" HeaderText="รหัส" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" Visible="false">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="center" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="EMP_THNAME" HeaderText="ชื่อพนักงาน" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" Visible="false">
                                                                                    <HeaderStyle CssClass="center" />

                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="EMP_BUREAU" Visible="false" HeaderText="สำนัก" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="left" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="DRUGLIST_INDEX" HeaderText="ชื่อยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-" Visible="false">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                </asp:BoundField>


                                                                                <asp:BoundField DataField="DRUGLIST_NAME" HeaderText="ชื่อยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="LABEL_DESC" HeaderText="ฉลากยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="DRUGBRING_ORDER" Visible="false" HeaderText="เลขที่เบิกยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                </asp:BoundField>

                                                                                <asp:BoundField DataField="CREATE_DATE" HeaderText="วันที่เบิกยา" SortExpression="DRUGLIST_INDEX" NullDisplayText="-">
                                                                                    <HeaderStyle CssClass="center" />
                                                                                    <ItemStyle CssClass="center" />

                                                                                </asp:BoundField>

                                                                                <asp:TemplateField HeaderStyle-CssClass="center" HeaderText="" HeaderStyle-Width="10%">
                                                                                    <ItemStyle CssClass="center" />
                                                                                    <ItemTemplate>
                                                                                        <div class="btn-group btn-group-sm">
                                                                                            <asp:LinkButton ID="btnRowPrint" data-toggle="tooltip" data-original-title="ส่งออก" data-placement="top" runat="server" OnClientClick="return confirm('ยืนยันการส่งออก');" CommandName="cmdPrint" CommandArgument='<%#Eval("DRUGLIST_INDEX") %>' CssClass="btn btn-warning fa fa-print" />

                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </div>

                                                                <div class="text-right innerAll border-top bg-white">
                                                                    <asp:Panel ID="pnButtonViewDrugBringPage" runat="server">
                                                                        <div class="btn-group btn-group">
                                                                            <asp:LinkButton ID="btnShowAll" runat="server" CssClass="btn btn-primary" OnClick="btnShowAll_Click"><i class="fa fa-fw fa-check-circle-o"></i> แสดงการเบิกยาทั้งหมด</asp:LinkButton>
                                                                            <asp:LinkButton ID="btnConfirm" runat="server" CssClass="btn btn-success" OnClick="btnConfirm_Click"><i class="fa fa-fw fa-check-circle-o"></i> ย้อนกลับ</asp:LinkButton>
                                                                        </div>
                                                                    </asp:Panel>
                                                                    <asp:Panel ID="pnButtonViewEmpBringPage" runat="server">
                                                                        <div class="btn-group btn-group">
                                                                            <asp:LinkButton ID="btnConfirm2" runat="server" CssClass="btn btn-success" OnClick="btnConfirm_Click"><i class="fa fa-fw fa-check-circle-o"></i> ย้อนกลับ</asp:LinkButton>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </div>
                                                                <div class="col-separator-h box"></div>
                                                            </div>
                                                        </div>
                                                        <div id="ascrail2000" class="nicescroll-rails" style="width: 5px; z-index: 2; cursor: default; position: absolute; top: 0px; left: 1073px; height: 432px; display: none; opacity: 0;">
                                                            <div style="position: relative; top: 0px; float: right; width: 5px; height: 432px; border: none; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px; background-color: rgb(235, 106, 90); background-clip: padding-box;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:HiddenField ID="hdfFunc" runat="server" />
            <asp:HiddenField ID="hdfDrugID" runat="server" />
            <asp:HiddenField ID="hdfEmpID" runat="server" />
            <asp:HiddenField ID="hdfBringID" runat="server" />
            <asp:HiddenField ID="hdfDateBegin" runat="server" />
            <asp:HiddenField ID="hdfDateEnd" runat="server" />

            <asp:Panel ID="MedDrugErrorPanel" runat="server">
                <div class="row error">
                    <div class="col-md-4 col-md-offset-1 center">
                        <div class="center">
                            <img src="../assets//images/error-icon-bucket.png " class="error-icon" />
                        </div>
                    </div>
                    <div class="col-md-5 content center">
                        <h1 class="strong">Oups!</h1>
                        <h4 class="innerB">This page does not exist.</h4>
                        <div class="well">
                            ERROR MESSAGE GOES HERE!
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="GridView2" />
            <asp:PostBackTrigger ControlID="GridView3" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
