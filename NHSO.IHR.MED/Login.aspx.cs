﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NHSO.IHR.MED.Utility;
namespace NHSO.IHR.MED
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[CookieManager.SESSION_KEY] != null)
            {              
                Response.Redirect("admin/medicineWithdrawManagement.aspx");
            }
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (txtusername.Text.ToUpper() == "ADMIN" & txtpassword.Text.ToUpper() == "ADMIN")
            {
                Session[CookieManager.SESSION_KEY] = CookieManager.SESSION_VALUE;
                Response.Redirect(@"admin/medicineWithdrawManagement.aspx");
            }
            else
            {
                PopupMessage.Show("แจ้งเตือน", "รหัสผ่านไม่ถูกต้อง");
            }
        }
    }
}