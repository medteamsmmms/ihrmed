﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportPerson.aspx.cs" Inherits="NHSO.IHR.MED.Reports.ReportPerson" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="div1">
   <table align="center" width="98%" cellpadding="0" cellspacing="0" border="1">
    <tr><th colspan="5" height="20">Report</th></tr>
    <tr><td colspan="5">&nbsp;</td></tr>
    <tr>
       <td colspan="5"><input type="button" id="btnExport" runat="server" value="Export Report"></td>
    </tr>
    <tr>
       <td colspan="5">
     </td>
    </tr>
    <asp:Repeater ID="rptHistory" Runat="server">
     <HeaderTemplate>
      <tr>
       <td align="center" style="BACKGROUND-COLOR: #ffcccc"><b>Name</b></td>
       <td align="center" style="BACKGROUND-COLOR: #ffcccc"><b>Age</b></td>
       <td align="center" style="BACKGROUND-COLOR: #ffcccc"><b>City</b></td>
       <td align="center" style="BACKGROUND-COLOR: #ffcccc"><b>School</b></td>
       <td align="center" style="BACKGROUND-COLOR: #ffcccc"><b>Class</b></td>
      </tr>
     </HeaderTemplate>
     <ItemTemplate>
      <tr id="trCnt" runat="server">
       <td>&nbsp;<%# DataBinder.Eval(Container.DataItem, "Name") %></td>
       <td align="center"><%# DataBinder.Eval(Container.DataItem, "Age") %></td>
       <td align="center"><%# DataBinder.Eval(Container.DataItem, "City") %></td>
       <td align="center"><%# DataBinder.Eval(Container.DataItem, "School") %></td>
       <td>&nbsp;<%# DataBinder.Eval(Container.DataItem, "Class") %></td>
      </tr>
     </ItemTemplate>
    </asp:Repeater>
   </table>
  </div>
    </form>
</body>
</html>
