﻿using NHSO.IHR.MED.Data.Oracle.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NHSO.IHR.MED.Entity;
using System.Data;
using System.Linq.Expressions;
using System.Collections;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.OracleClient;
using NHSO.IHR.MED.Entity.DataController;
namespace NHSO.IHR.MED
{
    public partial class _Default : Page
    {

        MedDrugListCtr medCtr;
        public virtual string NullDisplayText { get; set; }


        protected void Page_Init(object sender, EventArgs e)
        {
            medCtr = new MedDrugListCtr();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //g
            if (!IsPostBack)
            {
                try
                {
                    bindGridView();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }

        protected void bindGridView()
        {
            GridView1.DataSource = medCtr.dataList;
            GridView1.DataBind();
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            txtTotalMedicines.Text = medCtr.medAmount;
            txtLastUpdate.Text = "<h5 class='innerAll margin-none bg-white strong border-bottom'><i class='fa fa-fw fa-calendar'></i>&nbsp;" + medCtr.lastUpdate + "</h5>";
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        

    }
}