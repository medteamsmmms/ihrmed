﻿using NHSO.IHR.MED.Data.Oracle.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Web;
using NHSO.IHR.MED.Utility;
using NHSO.IHR.MED.Entity;
namespace NHSO.IHR.MED.admin
{
    public class ReportType
    {
        private string pkgname = "PKG_MED_WITHDRAW";
        private string pkgReport = "PKG_RPT_EACH_TYPE";
        private string refcursor = "o_result";

        public DataSet GetReportDetail(string group_id, string type_id,string symptom_id, string sdate, string edate)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(pkgReport + ".md_search_report_type");
            comm.AddOutputParameter(refcursor, OracleType.Cursor, 100000);
            comm.AddInputParameter("p_druggroup_id", OracleType.VarChar, group_id.ToStringOrNull());
            comm.AddInputParameter("p_drugtype_id", OracleType.VarChar, type_id.ToStringOrNull());
            comm.AddInputParameter("p_symptom_id", OracleType.VarChar, symptom_id.ToStringOrNull());
            comm.AddInputParameter("p_date_begin", OracleType.VarChar, sdate.ToStringOrNull());
            comm.AddInputParameter("p_date_end", OracleType.VarChar, edate.ToStringOrNull());
            return comm.ExecuteToDataset();
        }
        public List<MedDrugTypeEntity> GetDrugType(string id)
        {
            string sqltext = pkgname + ".md_get_type";
            CommandData cmd = new CommandData();
            cmd.SetCommandStoredProcedure(sqltext);
            cmd.AddOutputParameter(refcursor, OracleType.Cursor, 100000);
            cmd.AddInputParameter("o_id", OracleType.VarChar, id.ToStringOrNull());
            List<MedDrugTypeEntity> list = cmd.ExecuteToList<MedDrugTypeEntity>();
            return list;
        }
        public List<MedDrugGroupEntity> GetDrugGroup()
        {
            string sqltext = pkgname + ".md_get_group";
            CommandData cmd = new CommandData();
            cmd.SetCommandStoredProcedure(sqltext);
            cmd.AddOutputParameter(refcursor, OracleType.Cursor, 100000);
            List<MedDrugGroupEntity> list = cmd.ExecuteToList<MedDrugGroupEntity>();
            return list;
        }
        public List<MedDrugSymptomEntity> GetDrugSym(string msg)
        {
            string sqltext = pkgname + ".md_get_symtom";
            CommandData cmd = new CommandData();
            cmd.SetCommandStoredProcedure(sqltext);
            cmd.AddOutputParameter(refcursor, OracleType.Cursor, 100000);
            cmd.AddInputParameter("o_id", OracleType.VarChar, msg.ToStringOrNull());
            List<MedDrugSymptomEntity> list = cmd.ExecuteToList<MedDrugSymptomEntity>();
            return list;
        }
    }
}