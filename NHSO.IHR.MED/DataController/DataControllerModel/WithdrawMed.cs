﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHSO.IHR.MED.Entity;
using NHSO.IHR.MED.Data.Oracle.Common;
using NHSO.IHR.MED.Utility;
using System.Data.OracleClient;
namespace NHSO.IHR.MED.admin
{
    public class WithdrawMed
    {
        public WithdrawMed()
        { }
        private string pkgname = "PKG_MED_WITHDRAW";
        private string refcursor = "o_result";
        public List<MedDrugListEntity> GetDrugList(WithDrawSearchEntity ent)
        {

            //            string sqltext = @"select rownum as NUM,t1.DRUGLIST_INDEX,t1.DRUGLIST_ID, t1.DRUGLIST_NAME,t1.DRUGLIST_DESC,t3.DRUGGROUP_NAME,t4.DRUGTYPE_NAME,NVL(t2.STOCK_BALANCE,0) as STOCK_BALANCE,t5.UNITLIST_NAME,t1.DRUGGROUP_ID 
            //                                from MED_DRUGLIST t1 left join 
            //                 MED_OVERSTOCK t2 on t1.DRUGLIST_INDEX = t2.DRUGLIST_INDEX left join 
            //                 MED_DRUGGROUP t3 on t1.DRUGGROUP_ID = t3.DRUGGROUP_ID left join 
            //                 MED_DRUGTYPE t4 on t1.DRUGTYPE_ID = t4.DRUGTYPE_ID left join 
            //                 MED_UNITLIST t5 on t1.UNITLIST_ID = t5.UNITLIST_ID
            //                 where NVL(t1.FLAG,0) != 'D' ";
            //            CommandData cmd = new CommandData();
            //            cmd.SetCommandText(sqltext);
            //            if (ent.Remain == true)
            //            {
            //                sqltext += " and  NVL(t2.STOCK_BALANCE,0) >0";
            //            }
            //            if (ent.MedName != "")
            //            {
            //                sqltext += " and (trim(upper(t1.DRUGLIST_NAME)) like trim(upper('%' || :medname || '%')) or trim(upper(t1.BARCODE_STANDARD)) = trim(upper(:medname)))";
            //                cmd.AddInputParameter("medname", ent.MedName);
            //            }
            //            if (ent.MedGroup != "all")
            //            {
            //                sqltext += " and t1.DRUGGROUP_ID = :medgroup";
            //                cmd.AddInputParameter("medgroup", ent.MedGroup);
            //            }
            //            if (ent.MedType != "all")
            //            {
            //                sqltext += " and t1.DRUGTYPE_ID = :medtype";
            //                cmd.AddInputParameter("medtype", ent.MedType);
            //            }
            //            cmd.SetCommandNewText(sqltext);
            string sqltext = pkgname + ".md_get_druglist";
            CommandData cmd = new CommandData();
            cmd.SetCommandStoredProcedure(sqltext);
            cmd.AddOutputParameter(refcursor, OracleType.Cursor, 100000);
            cmd.AddInputParameter("remain", OracleType.VarChar, ((ent.Remain == true) ? "1" : "").ToStringOrNull());
            cmd.AddInputParameter("medname", OracleType.VarChar, ent.MedName.ToStringOrNull());
            cmd.AddInputParameter("medgroup", ent.MedGroup.ToStringOrNull());
            cmd.AddInputParameter("medtype", ent.MedType.ToStringOrNull());
            List<MedDrugListEntity> list = cmd.ExecuteToList<MedDrugListEntity>();
            return list;
        }
        public List<MedDrugTypeEntity> GetDrugType(string id)
        {
            string sqltext = pkgname + ".md_get_type";
            CommandData cmd = new CommandData();
            cmd.SetCommandStoredProcedure(sqltext);
            cmd.AddOutputParameter(refcursor, OracleType.Cursor, 100000);
            cmd.AddInputParameter("o_id", OracleType.VarChar, id.ToStringOrNull());
            List<MedDrugTypeEntity> list = cmd.ExecuteToList<MedDrugTypeEntity>();
            return list;
        }
        public List<MedDrugGroupEntity> GetDrugGroup()
        {
            string sqltext = pkgname + ".md_get_group";
            CommandData cmd = new CommandData();
            cmd.SetCommandStoredProcedure(sqltext);
            cmd.AddOutputParameter(refcursor, OracleType.Cursor, 100000);
            List<MedDrugGroupEntity> list = cmd.ExecuteToList<MedDrugGroupEntity>();
            return list;
        }
        public List<MedDrugSymptomEntity> GetDrugSym(string msg)
        {
            string sqltext = pkgname + ".md_get_symtom";
            CommandData cmd = new CommandData();
            cmd.SetCommandStoredProcedure(sqltext);
            cmd.AddOutputParameter(refcursor, OracleType.Cursor, 100000);
            cmd.AddInputParameter("o_id", OracleType.VarChar, msg.ToStringOrNull());
            List<MedDrugSymptomEntity> list = cmd.ExecuteToList<MedDrugSymptomEntity>();
            return list;
        }
        public int GetMaxBringID()
        {
            int result = 0;
            System.Data.DataSet ds;
            string sqltext = pkgname + ".md_get_maxbringid";
            CommandData cmd = new CommandData();
            cmd.SetCommandStoredProcedure(sqltext);
            cmd.AddOutputParameter(refcursor, OracleType.Cursor, 100000);
            ds = cmd.ExecuteToDataset();
            if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
            {
                result = ds.Tables[0].Rows[0][0].ToString().Toint();
            }
            return result + 1;
        }
        public int GetMaxOrder(string id)
        {
            try
            {
                string result;
                string sqltext = pkgname + ".md_get_maxbringorder";
                CommandData cmm = new CommandData();
                cmm.SetCommandStoredProcedure(sqltext);
                cmm.AddInputParameter("o_emp_id", id);
                cmm.AddOutputParameter("o_drugbring_order", OracleType.VarChar, 10000);
                cmm.ExecuteNonQuery();
                result = cmm.GetParameter("o_drugbring_order");
                string[] s = result.Split('-');
                return s[2].Toint() + 1;
            }
            catch (Exception)
            {

                return 1;
            }
        }
        public bool CheckEmployeeID(string msg)
        {
            bool result = false;
            string sqltext = pkgname + ".md_get_employeeid";
            CommandData cmd = new CommandData();
            cmd.SetCommandStoredProcedure(sqltext);
            cmd.AddInputParameter("o_id", OracleType.VarChar, msg.ToStringOrNull());
            cmd.AddOutputParameter(refcursor, OracleType.Cursor, 100000);
            System.Data.DataSet ds = cmd.ExecuteToDataset();
            System.Data.DataTable dt = ds.Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = true;
            }
            return result;
        }
        public bool CheckAllergicRecord(string msg)
        {
            bool result = false;
            string sqltext = pkgname + ".md_get_allergic";
            CommandData cmd = new CommandData();
            cmd.SetCommandStoredProcedure(sqltext);
            cmd.AddInputParameter("o_id", OracleType.VarChar, msg.ToStringOrNull());
            cmd.AddOutputParameter(refcursor, OracleType.Cursor, 100000);
            System.Data.DataSet ds = cmd.ExecuteToDataset();
            System.Data.DataTable dt = ds.Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = true;
            }
            return result;
        }
        public bool SetAllergicRecord(string id, string msg)
        {
            bool result = false;
            string sqltext = pkgname;
            if (CheckAllergicRecord(id))
            {
                sqltext += ".md_update_allergic";
            }
            else
            {
                sqltext += ".md_insert_allergic";
            }
            try
            {
                CommandData cmd = new CommandData();
                cmd.SetCommandStoredProcedure(sqltext);
                cmd.AddInputParameter("o_emp_id", id);
                cmd.AddInputParameter("o_detail", msg);
                cmd.ExecuteNonQuery();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }
        public string SetAllergicDetail(string id)
        {
            string result = "";
            string sqltext = pkgname + ".md_get_allergicdetail";
            CommandData cmd = new CommandData();
            cmd.SetCommandStoredProcedure(sqltext);
            cmd.AddInputParameter("o_id", OracleType.VarChar, id.ToStringOrNull());
            cmd.AddOutputParameter(refcursor, OracleType.Cursor, 100000);
            System.Data.DataSet ds = cmd.ExecuteToDataset();
            System.Data.DataTable dt = ds.Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = dt.Rows[0][0].ToString();
            }
            return result;
        }
        public bool CheckBalance(string id, int amount)
        {
            string result;
            string sqltext = pkgname + ".md_get_balance";
            CommandData cmm = new CommandData();
            cmm.SetCommandStoredProcedure(sqltext);
            cmm.AddInputParameter("o_id", id.ToStringOrNull());
            cmm.AddOutputParameter("balance", OracleType.VarChar, 10000);
            cmm.ExecuteNonQuery();
            result = cmm.GetParameter("balance");
            if (amount > result.Toint())
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool InsertRecord(List<MedDrugBringEntity> list, string emp, DateTime sdate)
        {
            bool result = false;
            int bring_id = GetMaxBringID();
            //new เปลี่ยนเป็น 10 หลัก
            //int MaxOrder = GetMaxOrder(emp);
           // string Orderid = string.Format("{0:000000}", bring_id) + "-" + emp + "-" + string.Format("{0:000000}", MaxOrder);
            string CurrentDate = DateTime.Now.Date.ToString("yy", System.Globalization.CultureInfo.GetCultureInfo("th-TH"));
            string Orderid = string.Format("{0:0000000000}", CurrentDate + string.Format("{0:00000000}", bring_id));
            string sqlInsertBring = pkgname + ".md_insert_bring";
            string sqlInsertBringDesc = pkgname + ".md_insert_bring_desc";
            string sqlInsertFlow = pkgname + ".md_insert_flow";
            string sqlUpdateOverStock = pkgname + ".md_update_overstock";
            CommandData cmd = new CommandData();
            try
            {
                cmd.CreateTransection();

                cmd.SetCommandStoredProcedureTran(sqlInsertBring);
                cmd.AddInputParameter("o_drugbring", bring_id);
                cmd.AddInputParameter("o_emp_id", emp);
                cmd.AddInputParameter("o_create_date", sdate);
                cmd.AddInputParameter("o_create_by", -1);
                cmd.AddInputParameter("o_update_date", sdate);
                cmd.AddInputParameter("o_update_by", -1);
                cmd.AddInputParameter("o_drugbring_order", Orderid);
                cmd.ExecuteTrans();
                foreach (MedDrugBringEntity item in list)
                {
                    cmd.SetCommandStoredProcedureTran(sqlInsertFlow);
                    //cmd.AddInputParameter("FLOWSTOCK_ID", brind_id);
                    cmd.AddInputParameter("o_druglist_index", item.DRUGLIST_INDEX);
                    cmd.AddInputParameter("o_stock_input", 0);
                    cmd.AddInputParameter("o_stock_output", item.STOCK_OUTPUT.Toint());
                    cmd.AddInputParameter("o_create_date", -1);
                    cmd.AddInputParameter("o_create_by", sdate);
                    cmd.AddInputParameter("o_update_date", -1);
                    cmd.AddInputParameter("o_update_by", sdate);
                    cmd.AddInputParameter("o_drugbring_id", bring_id);
                    cmd.ExecuteTrans();

                    cmd.SetCommandStoredProcedureTran(sqlInsertBringDesc);
                    cmd.AddInputParameter("o_drugbring_id", bring_id);
                    cmd.AddInputParameter("o_druglist_index", item.DRUGLIST_INDEX);
                    cmd.AddInputParameter("o_symptom_id", item.SYMTOM_ID);
                    cmd.AddInputParameter("o_create_date", sdate);
                    cmd.AddInputParameter("o_create_by", -1);
                    cmd.AddInputParameter("o_update_date", sdate);
                    cmd.AddInputParameter("o_update_by", -1);
                    cmd.ExecuteTrans();

                    cmd.SetCommandStoredProcedureTran(sqlUpdateOverStock);
                    cmd.AddInputParameter("o_stock_balance", item.STOCK_OUTPUT.Toint());
                    cmd.AddInputParameter("o_druglist_index", item.DRUGLIST_INDEX);
                    cmd.ExecuteTrans();
                }
                cmd.TransCommit();
                result = true;
            }
            catch (Exception)
            {
                cmd.TransRollBack();
                result = false;
            }
            return result;
        }
        public List<MedDrugSymptomEntity> GetLastBingDesc(int Druglist_index)
        {
            string sqltext = pkgname + ".md_last_bringdesc";
            CommandData cmd = new CommandData();
            cmd.SetCommandStoredProcedure(sqltext);
            cmd.AddOutputParameter(refcursor, OracleType.Cursor, 100000);
            cmd.AddInputParameter("o_druglist_index", OracleType.Int32, Druglist_index);
            List<MedDrugSymptomEntity> list = cmd.ExecuteToList<MedDrugSymptomEntity>();
            return list;
        }

    }
}