﻿using NHSO.IHR.MED.Data.Oracle.Common;
using NHSO.IHR.MED.Entity;
using NHSO.IHR.MED.Entity.DataController;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.OracleClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace NHSO.IHR.MED.DataController.DataControllerModel
{
    public class MedDrugSymptomCtr: MotherDrugCtr
    {

        //Declaraion-PL & Package
        private string PKG_NAME = "PKG_MED_SYMPTOM";
        private string MD_LIST_SELECT = ".MD_LIST_SELECT";
        private string MD_LIST_SELECT_DROPDOWN = ".MD_LIST_SELECT_DROPDOWN";
        private string MD_SELECT = ".MD_SELECT";
        private string MD_UPDATE = ".MD_UPDATE";
        private string MD_DELETE = ".MD_DELETE";
        private string MD_DELETE_BY_FLAG = ".MD_DELETE_BY_FLAG";
        private string MD_CAN_BE_DELETED = ".MD_CAN_BE_DELETED";
        private string MD_INSERT = ".MD_INSERT";
        private string MD_SELECT_GROUP = ".md_select_group";
        private string MD_SELECT_SYMPTOM_NAME = ".md_select_symptom_name";
        private string MD_SEARCH = ".md_search";
        private string MD_CHECK_CHR = ".md_check_chr";

        //Declaration-Variable
        public List<MedDrugSymptomEntity> dataList { get; set; }
        public MedDrugSymptomEntity data { get; set; }
        public string medAmount { get; set; }
        public string medDeleted { get; set; }
        private string outputResult1 = "o_result";
        private string outputResult2 = "o_lastupdate";
        private string outputResult3 = "o_amount";

        //Declaration-Columns
        public string SYMPTOM_ID = "SYMPTOM_ID";
        public string SYMPTOM_NAME = "SYMPTOM_NAME";
        public string SYMPTOM_DESC = "SYMPTOM_DESC";
        public string DRUGGROUP_ID = "DRUGGROUP_ID";
        public string DRUGGROUP_NAME = "DRUGGROUP_NAME";        
        public string CREATE_BY = "CREATE_BY";
        public string CREATE_DATE = "CREATE_DATE";
        public string UPDATE_BY = "UPDATE_BY";
        public string UPDATE_DATE = "UPDATE_DATE";
        public string FLAG = "FLAG";

        //Declaration-Params
        public string p_symptom_id = "p_symptom_id";
        public string p_symptom_name = "p_symptom_name";
        public string p_symptom_desc = "p_symptom_desc";
        public string p_druggroup_id = "p_druggroup_id";
        public string p_druggroup_name = "p_druggroup_name";
        public string p_create_by = "p_create_by";
        public string p_update_by = "p_update_by";
        public string p_flag = "p_flag";
        public string p_create_date = "p_create_date";
        public string p_start_date = "p_start_date";
        public string p_end_date = "p_end_date";


        public MedDrugSymptomCtr()
        {
            this.lastUpdate = "";
            this.medAmount = "";
            this.medDeleted = "";
            this.dataList = new List<MedDrugSymptomEntity>();
            data = new MedDrugSymptomEntity();
        }

        //SQL-Command
        public List<MedDrugSymptomEntity> getList()
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_LIST_SELECT);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugSymptomEntity>();
            return dataList;
        }

        public List<MedDrugSymptomEntity> getListDropdown()
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_LIST_SELECT_DROPDOWN);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.DateTime, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugSymptomEntity>();
            return dataList;
        }

        public int insertData(MedDrugSymptomEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_INSERT);
            comm.AddInputParameter(p_symptom_name, OracleType.VarChar, newEntity.SYMPTOM_NAME);
            comm.AddInputParameter(p_symptom_desc, OracleType.VarChar, newEntity.SYMPTOM_DESC);
            comm.AddInputParameter(p_druggroup_id, OracleType.VarChar, newEntity.DRUGGROUP_ID);
            comm.AddInputParameter(p_create_by, OracleType.VarChar, newEntity.CREATE_BY);
            comm.AddInputParameter(p_update_by, OracleType.VarChar, newEntity.UPDATE_BY);
            comm.AddInputParameter(p_flag, OracleType.VarChar, newEntity.FLAG);
            
            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int updateData(MedDrugSymptomEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_UPDATE);
            comm.AddInputParameter(p_symptom_id, OracleType.VarChar, newEntity.SYMPTOM_ID);
            comm.AddInputParameter(p_symptom_name, OracleType.VarChar, newEntity.SYMPTOM_NAME);
            comm.AddInputParameter(p_symptom_desc, OracleType.VarChar, newEntity.SYMPTOM_DESC);
            comm.AddInputParameter(p_druggroup_id, OracleType.VarChar, newEntity.DRUGGROUP_ID);
            comm.AddInputParameter(p_update_by, OracleType.VarChar, newEntity.UPDATE_BY);
            comm.AddInputParameter(p_flag, OracleType.VarChar, newEntity.FLAG);
            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int deleteData(MedDrugSymptomEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_DELETE);
            comm.AddInputParameter(p_symptom_id, OracleType.VarChar, newEntity.SYMPTOM_ID);

            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int deleteDataByFlag(MedDrugSymptomEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_DELETE_BY_FLAG);
            comm.AddInputParameter(p_symptom_id, OracleType.VarChar, newEntity.SYMPTOM_ID);

            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int canBeDeleted(MedDrugSymptomEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_CAN_BE_DELETED);
            comm.AddInputParameter(p_symptom_id, OracleType.VarChar, newEntity.SYMPTOM_ID);
            comm.AddOutputParameter(outputResult3, OracleType.VarChar, 100000);
            comm.ExecuteNonQuery();
            medDeleted = comm.GetParameter(outputResult3);
            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public MedDrugSymptomEntity getMedSymptom(string symptomId)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SELECT);
            comm.AddInputParameter(p_symptom_id, OracleType.VarChar, symptomId);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.DateTime, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugSymptomEntity>();
            foreach (MedDrugSymptomEntity m in dataList)
            {
                data = m;
            }
            return data;
        }



        //Utilities
        public DropDownList getDropdown(DropDownList d1, string txtName, string txtValue, string firstItem)
        {
            getListDropdown();
            d1.DataSource = dataList;
            d1.DataTextField = txtName;
            d1.DataValueField = txtValue;
            d1.DataBind();
            if (firstItem != null || firstItem != " ")
            {
                ListItem i = new ListItem();
                i.Text = firstItem;
                i.Value = "";
                d1.Items.Insert(0, i);
            }
            return d1;
        }

        public List<MedDrugSymptomEntity> getListGroup()
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SELECT_GROUP);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.ExecuteNonQuery();
            dataList = comm.ExecuteToList<MedDrugSymptomEntity>();
            return dataList;
        }

        public List<MedDrugSymptomEntity> getListSymptomName()
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SELECT_SYMPTOM_NAME);
            comm.AddInputParameter(p_flag, OracleType.VarChar,"Y");
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.ExecuteNonQuery();
            dataList = comm.ExecuteToList<MedDrugSymptomEntity>();
            return dataList;
        }

        public List<MedDrugSymptomEntity> getListSearch(MedDrugSymptomEntity _pOdj)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SEARCH);
            comm.AddInputParameter(p_druggroup_id, OracleType.VarChar, _pOdj.DRUGGROUP_ID);
            comm.AddInputParameter(p_symptom_name, OracleType.VarChar, _pOdj.SYMPTOM_NAME);
            comm.AddInputParameter(p_start_date, OracleType.VarChar, _pOdj.START_DATE);
            comm.AddInputParameter(p_end_date, OracleType.VarChar, _pOdj.END_DATE);
            comm.AddInputParameter(p_flag, OracleType.VarChar, _pOdj.FLAG);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            //comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            //medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugSymptomEntity>();
            return dataList;
        }

        public List<MedDrugSymptomEntity> Check_Chr(string Symptom_Name)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_CHECK_CHR);
            comm.AddInputParameter(p_symptom_name, OracleType.VarChar, Symptom_Name);
            comm.AddInputParameter(p_flag, OracleType.VarChar, "Y");
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.ExecuteNonQuery();
            dataList = comm.ExecuteToList<MedDrugSymptomEntity>();
            return dataList;
        }

    }
}