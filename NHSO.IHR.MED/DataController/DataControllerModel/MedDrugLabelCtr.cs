﻿using NHSO.IHR.MED.Data.Oracle.Common;
using NHSO.IHR.MED.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Web;

namespace NHSO.IHR.MED.Entity.DataController
{
    public class MedDrugLabelCtr : MotherDrugCtr
    {

        //Declaraion-PL & Package
        private string PKG_NAME = "PKG_MED_DRUG_LABEL";
        private string MD_LIST_SELECT = ".MD_LIST_SELECT";
        private string MD_SELECT_DUP = ".MD_SELECT_IS_DUPLICATE";
        private string MD_SELECT = ".MD_SELECT";
        private string MD_UPDATE = ".MD_UPDATE";
        private string MD_DELETE = ".MD_DELETE";
        private string MD_INSERT = ".MD_INSERT";
        private string MD_SEARCH = ".MD_SEARCH";
        private string MD_ALL_SELECT = ".md_all_select";
        
        //Declaration-Variable
        public List<MedDrugLabelEntity> dataList { get; set; }
        public  MedDrugLabelEntity data { get; set; }
        public DataSet dataSet { get; set; }
        public string medAmount { get; set; }
        private string outputResult1 = "o_result";
        private string outputResult2 = "o_lastupdate";
        private string outputResult3 = "o_amount";

        //Declaration-Params
        private string p_druglist_name = "p_druglist_name";
        private string p_drugtype_id = "p_drugtype_id";
        private string p_druggroup_id = "p_druggroup_id";

        private string p_druglist_id = "p_druglist_id"; 
        private string p_druglist_index = "p_druglist_index";    
        private string p_drug_usetime = "p_drug_usetime";
        private string p_drug_mealtime0 = "p_drug_mealtime0";
        private string p_drug_mealtime1 = "p_drug_mealtime1";
        private string p_drug_time0 = "p_drug_time0";
        private string p_drug_time1 = "p_drug_time1";
        private string p_drug_time2 = "p_drug_time2";
        private string p_drug_time3 = "p_drug_time3";
        private string p_drug_usefor = "p_drug_usefor";
        private string p_create_by = "p_create_by";
        private string p_update_by = "p_update_by";
        private string p_druguse_id = "p_druguse_id";


        //Constructor
        public MedDrugLabelCtr()
        {
            this.lastUpdate = "";
            this.medAmount = "";
            this.dataList = new List<MedDrugLabelEntity>();
            this.data = new MedDrugLabelEntity();
        }

        //Method
        public List<MedDrugLabelEntity> getList()
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME+MD_LIST_SELECT);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugLabelEntity>();
            return dataList;
        }

        public int insertData(MedDrugLabelEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_INSERT);
            comm.AddInputParameter(p_druglist_index, OracleType.VarChar, newEntity.DRUGLIST_INDEX);
            comm.AddInputParameter(p_drug_usetime, OracleType.VarChar, newEntity.DRUG_USETIME);
            comm.AddInputParameter(p_drug_mealtime0, OracleType.VarChar, newEntity.DRUG_MEALTIME0);
            comm.AddInputParameter(p_drug_mealtime1, OracleType.VarChar, newEntity.DRUG_MEALTIME1);
            comm.AddInputParameter(p_drug_time0, OracleType.VarChar, newEntity.DRUG_TIME0);
            comm.AddInputParameter(p_drug_time1, OracleType.VarChar, newEntity.DRUG_TIME1);
            comm.AddInputParameter(p_drug_time2, OracleType.VarChar, newEntity.DRUG_TIME2);
            comm.AddInputParameter(p_drug_time3, OracleType.VarChar, newEntity.DRUG_TIME3);
            comm.AddInputParameter(p_drug_usefor, OracleType.VarChar, newEntity.DRUG_USEFOR);
            comm.AddInputParameter(p_create_by, OracleType.VarChar, newEntity.CREATE_BY);
            comm.AddInputParameter(p_update_by, OracleType.VarChar, newEntity.UPDATE_BY);
                        
            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int updateData(MedDrugLabelEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_UPDATE);
            comm.AddInputParameter(p_druglist_index, OracleType.VarChar, newEntity.DRUGLIST_INDEX);
            comm.AddInputParameter(p_drug_usetime, OracleType.VarChar, newEntity.DRUG_USETIME);
            comm.AddInputParameter(p_drug_mealtime0, OracleType.VarChar, newEntity.DRUG_MEALTIME0);
            comm.AddInputParameter(p_drug_mealtime1, OracleType.VarChar, newEntity.DRUG_MEALTIME1);
            comm.AddInputParameter(p_drug_time0, OracleType.VarChar, newEntity.DRUG_TIME0);
            comm.AddInputParameter(p_drug_time1, OracleType.VarChar, newEntity.DRUG_TIME1);
            comm.AddInputParameter(p_drug_time2, OracleType.VarChar, newEntity.DRUG_TIME2);
            comm.AddInputParameter(p_drug_time3, OracleType.VarChar, newEntity.DRUG_TIME3);
            comm.AddInputParameter(p_drug_usefor, OracleType.VarChar, newEntity.DRUG_USEFOR);
            comm.AddInputParameter(p_update_by, OracleType.VarChar, newEntity.UPDATE_BY);
                        
            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int deleteData(MedDrugLabelEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_DELETE);
            comm.AddInputParameter(p_druguse_id, OracleType.VarChar, newEntity.DRUGUSE_ID);      

            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public MedDrugLabelEntity getLabel(string drugId)
        {
            CommandData comm = new CommandData();
            List<MedDrugLabelEntity> m = new List<MedDrugLabelEntity>();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SELECT);
            comm.AddInputParameter(p_druglist_index, OracleType.VarChar, drugId);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.DateTime, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            m = comm.ExecuteToList<MedDrugLabelEntity>();
            foreach (MedDrugLabelEntity m1 in m)
            {
                data = m1;
            }
            return data;
        }

        public DataSet getLabelDataSet(string drugId)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SELECT);
            comm.AddInputParameter(p_druglist_index, OracleType.VarChar, drugId);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.DateTime, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataSet = comm.ExecuteToDataset();
            return dataSet;
        }

        public Boolean isDuplicated(string drugId)
        {
            List<MedDrugLabelEntity> m = new List<MedDrugLabelEntity>();
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SELECT_DUP);
            comm.AddInputParameter(p_druglist_index, OracleType.VarChar, drugId);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.DateTime, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            m = comm.ExecuteToList<MedDrugLabelEntity>();
            if(m.Count==0){
                return false;
            }else{
                return true;       
            }
            
        }

        //Search
        public List<MedDrugLabelEntity> searhFor(MedDrugLabelEntity e)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SEARCH);
            comm.AddInputParameter(p_druglist_name, OracleType.VarChar, e.DRUGLIST_NAME);
            comm.AddInputParameter(p_druggroup_id, OracleType.VarChar, e.DRUGGROUP_ID);
            comm.AddInputParameter(p_drugtype_id, OracleType.VarChar, e.DRUGTYPE_ID);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugLabelEntity>();
            return dataList;
        }

        public List<MedDrugLabelEntity> getListAll()
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_ALL_SELECT);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            dataList = comm.ExecuteToList<MedDrugLabelEntity>();
            return dataList;
        }

    }
}
