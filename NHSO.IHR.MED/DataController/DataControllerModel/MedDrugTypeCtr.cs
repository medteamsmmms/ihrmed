﻿using NHSO.IHR.MED.Data.Oracle.Common;
using NHSO.IHR.MED.Entity.DataController;
using NHSO.IHR.MED.Entity;
using System;
using System.Collections.Generic;
using System.Data.OracleClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace NHSO.IHR.MED.DataController
{
    [Serializable]
    public class MedDrugTypeCtr : MotherDrugCtr
    {

        //Declaraion-PL & Package
        private string PKG_NAME = "PKG_MED_DRUG_TYPE";
        private string MD_LIST_SELECT = ".MD_LIST_SELECT";
        private string MD_LIST_SELECT_DROPDOWN = ".MD_LIST_SELECT_DROPDOWN";
        private string MD_DROPDOWN = ".MD_DROPDOWN";
        private string MD_SELECT = ".MD_SELECT";
        private string MD_UPDATE = ".MD_UPDATE";
        private string MD_DELETE = ".MD_DELETE";
        private string MD_DELETE_BY_FLAG = ".MD_DELETE_BY_FLAG";
        private string MD_CAN_BE_DELETED = ".MD_CAN_BE_DELETED";
        private string MD_INSERT = ".MD_INSERT";
        private string MD_SEARCH = ".md_search";
        private string MD_CHECK_CHR = ".md_check_chr";

        //Declaration-Variable
        public List<MedDrugTypeEntity> dataList { get; set; }
        public MedDrugTypeEntity data { get; set; }
        public string medAmount { get; set; }
        public string medDeleted { get; set; }
        private string outputResult1 = "o_result";
        private string outputResult2 = "o_lastupdate";
        private string outputResult3 = "o_amount";

        //Declaration-Column_Name
        public string DRUGTYPE_ID = "DRUGTYPE_ID";
        public string DRUGTYPE_NAME = "DRUGTYPE_NAME";
        public string DRUGTYPE_DESC = "DRUGTYPE_DESC";
        public string CREATE_BY = "CREATE_BY";
        public string CREATE_DATE = "CREATE_DATE";
        public string UPDATE_BY = "UPDATE_BY";
        public string UPDATE_DATE = "UPDATE_DATE";
        public string FLAG = "FLAG";
        public string SYS_CREATE_DATE = "sys_create_date";

        //Declaration-Params
        public string p_drugtype_id = "p_drugtype_id";
        public string p_druggroup_id = "p_druggroup_id";
        public string p_drugtype_name = "p_drugtype_name";
        public string p_drugtype_desc = "p_drugtype_desc";
        public string p_create_by = "p_create_by";
        public string p_start_date = "p_start_date";
        public string p_end_date = "p_end_date";
        public string p_update_by = "p_update_by";
        public string p_flag = "p_flag";
        public string p_create_date = "p_create_date";


        public MedDrugTypeCtr()
        {
            this.lastUpdate = "";
            this.medAmount = "";
            this.medDeleted = "";
            this.dataList = new List<MedDrugTypeEntity>();
            data = new MedDrugTypeEntity();
        }

        //SQL-Command
        public List<MedDrugTypeEntity> getList()
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_LIST_SELECT);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugTypeEntity>();
            return dataList;
        }

        public List<MedDrugTypeEntity> getListDropdown()
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_LIST_SELECT_DROPDOWN);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.DateTime, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugTypeEntity>();
            return dataList;
        }

        public List<MedDrugTypeEntity> getListDropdown(string druggroup_id)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_DROPDOWN);
            comm.AddInputParameter(p_druggroup_id, OracleType.VarChar, druggroup_id);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            //comm.AddOutputParameter(outputResult2, OracleType.DateTime, 10000);
            //comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            //lastUpdate = comm.GetParameter(outputResult2);
            //medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugTypeEntity>();
            return dataList;
        }

        public int insertData(MedDrugTypeEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_INSERT);
            comm.AddInputParameter(p_drugtype_name, OracleType.VarChar, newEntity.DRUGTYPE_NAME);
            comm.AddInputParameter(p_drugtype_desc, OracleType.VarChar, newEntity.DRUGTYPE_DESC);
            comm.AddInputParameter(p_create_by, OracleType.VarChar, newEntity.CREATE_BY);
            comm.AddInputParameter(p_update_by, OracleType.VarChar, newEntity.UPDATE_BY);
            comm.AddInputParameter(p_flag, OracleType.VarChar, newEntity.FLAG);

            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int updateData(MedDrugTypeEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_UPDATE);
            comm.AddInputParameter(p_drugtype_id, OracleType.VarChar, newEntity.DRUGTYPE_ID);
            comm.AddInputParameter(p_drugtype_name, OracleType.VarChar, newEntity.DRUGTYPE_NAME);
            comm.AddInputParameter(p_drugtype_desc, OracleType.VarChar, newEntity.DRUGTYPE_DESC);
            comm.AddInputParameter(p_update_by, OracleType.VarChar, newEntity.UPDATE_BY);
            comm.AddInputParameter(p_flag, OracleType.VarChar, newEntity.FLAG);

            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int deleteData(MedDrugTypeEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_DELETE);
            comm.AddInputParameter(p_drugtype_id, OracleType.VarChar, newEntity.DRUGTYPE_ID);

            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int deleteDataByFlag(MedDrugTypeEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_DELETE_BY_FLAG);
            comm.AddInputParameter(p_drugtype_id, OracleType.VarChar, newEntity.DRUGTYPE_ID);

            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int canBeDeleted(MedDrugTypeEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_CAN_BE_DELETED);
            comm.AddInputParameter(p_drugtype_id, OracleType.VarChar, newEntity.DRUGTYPE_ID);
            comm.AddOutputParameter(outputResult3, OracleType.VarChar, 100000);
            comm.ExecuteNonQuery();
            medDeleted = comm.GetParameter(outputResult3);
            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public MedDrugTypeEntity getMedDrug(string drugId)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SELECT);
            comm.AddInputParameter(p_drugtype_id, OracleType.VarChar, drugId);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.DateTime, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugTypeEntity>();
            foreach (MedDrugTypeEntity m in dataList)
            {
                data = m;
            }
            return data;
        }


        //Utility
        public DropDownList getDropdown(DropDownList d1, string txtName, string txtValue, string firstItem)
        {
            getListDropdown();
            d1.DataSource = dataList;
            d1.DataTextField = txtName;
            d1.DataValueField = txtValue;
            d1.DataBind();
            if (firstItem != null || firstItem != " ")
            {
                ListItem i = new ListItem();
                i.Text = firstItem;
                i.Value = "";
                d1.Items.Insert(0, i);
            }
            return d1;
        }
        public DropDownList getDropdown(DropDownList d1, string txtName, string txtValue, string firstItem, string druggroup_id)
        {
            getListDropdown(druggroup_id);
            d1.DataSource = dataList;
            d1.DataTextField = txtName;
            d1.DataValueField = txtValue;
            d1.DataBind();
            if (firstItem != null || firstItem != " ")
            {
                ListItem i = new ListItem();
                i.Text = firstItem;
                i.Value = "";
                d1.Items.Insert(0, i);
            }
            return d1;
        }

        public List<MedDrugTypeEntity> getListSearch(MedDrugTypeEntity _pObj)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SEARCH);
            comm.AddInputParameter(p_drugtype_id, OracleType.VarChar, _pObj.DRUGTYPE_ID);
            comm.AddInputParameter(p_start_date, OracleType.VarChar, _pObj.START_DATE);
            comm.AddInputParameter(p_end_date, OracleType.VarChar, _pObj.END_DATE);
            comm.AddInputParameter(p_flag, OracleType.VarChar,_pObj.FLAG);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            //comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            //medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugTypeEntity>();
            return dataList;
        }
        public List<MedDrugTypeEntity> Check_Chr(string Drugtypetype_Name)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_CHECK_CHR);
            comm.AddInputParameter(p_drugtype_name, OracleType.VarChar,Drugtypetype_Name);
            comm.AddInputParameter(p_flag, OracleType.VarChar,"Y");
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.ExecuteNonQuery();
            dataList = comm.ExecuteToList<MedDrugTypeEntity>();
            return dataList;
        }
    }
}
