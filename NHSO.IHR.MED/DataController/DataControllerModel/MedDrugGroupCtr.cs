﻿using NHSO.IHR.MED.Data.Oracle.Common;
using NHSO.IHR.MED.Entity.DataController;
using NHSO.IHR.MED.Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.OracleClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace NHSO.IHR.MED.DataController
{
    [Serializable]
    public class MedDrugGroupCtr : MotherDrugCtr
    {

        //Declaraion-PL & Package
        private string PKG_NAME = "PKG_MED_DRUG_GROUP";
        private string MD_LIST_SELECT = ".MD_LIST_SELECT";
        private string MD_LIST_SELECT_DROPDOWN = ".MD_LIST_SELECT_DROPDOWN";
        private string MD_DROPDOWN = ".MD_DROPDOWN";
        private string MD_SELECT = ".MD_SELECT";
        private string MD_UPDATE = ".MD_UPDATE";
        private string MD_DELETE = ".MD_DELETE";
        private string MD_DELETE_BY_FLAG = ".MD_DELETE_BY_FLAG";
        private string MD_CAN_BE_DELETED = ".MD_CAN_BE_DELETED";
        private string MD_INSERT = ".MD_INSERT";
        private string MD_SEARCH = ".md_search";

        //Declaration-Variable
        public List<MedDrugGroupEntity> dataList { get; set; }
        public MedDrugGroupEntity data { get; set; }
        public string medAmount { get; set; }
        public string medDeleted { get; set; }
        private string outputResult1 = "o_result";
        private string outputResult2 = "o_lastupdate";
        private string outputResult3 = "o_amount";

        //Declaration-Columns
        public string DRUGGROUP_ID = "DRUGGROUP_ID";
        public string DRUGGROUP_NAME = "DRUGGROUP_NAME";
        public string DRUGGROUP_DESC = "DRUGGROUP_DESC";
        public string CREATE_BY = "CREATE_BY";
        public string CREATE_DATE = "CREATE_DATE";
        public string UPDATE_BY = "UPDATE_BY";
        public string UPDATE_DATE = "UPDATE_DATE";
        public string FLAG = "FLAG";

        //Declaration-Params
        public string p_druggroup_id = "p_druggroup_id";
        public string p_druggroup_name = "p_druggroup_name";
        public string p_druggroup_desc = "p_druggroup_desc";
        public string p_create_by = "p_create_by";
        public string p_create_date = "p_create_date";
        public string p_start_date = "p_start_date";
        public string p_end_date = "p_end_date";
        public string p_update_by = "p_update_by";
        public string p_flag = "p_flag";
        public string p_drugtype_id = "p_drugtype_id";



        public MedDrugGroupCtr()
        {
            this.lastUpdate = "";
            this.medAmount = "";
            this.medDeleted = "";
            this.dataList = new List<MedDrugGroupEntity>();
            data = new MedDrugGroupEntity();
        }

        //SQL-Command
        public List<MedDrugGroupEntity> getList()
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_LIST_SELECT);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugGroupEntity>();
            return dataList;
        }

        public List<MedDrugGroupEntity> getListDropdown()
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_LIST_SELECT_DROPDOWN);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.DateTime, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugGroupEntity>();
            return dataList;
        }

        public List<MedDrugGroupEntity> getListDropdown(string drugtype_id)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_DROPDOWN);
            comm.AddInputParameter(p_drugtype_id, OracleType.VarChar, drugtype_id);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.ExecuteNonQuery();
            dataList = comm.ExecuteToList<MedDrugGroupEntity>();
            return dataList;
        }
        
        public int insertData(MedDrugGroupEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_INSERT);
            comm.AddInputParameter(p_druggroup_name, OracleType.VarChar, newEntity.DRUGGROUP_NAME);
            comm.AddInputParameter(p_druggroup_desc, OracleType.VarChar, newEntity.DRUGGROUP_DESC);
            comm.AddInputParameter(p_create_by, OracleType.VarChar, newEntity.CREATE_BY);
            comm.AddInputParameter(p_update_by, OracleType.VarChar, newEntity.UPDATE_BY);
            comm.AddInputParameter(p_flag, OracleType.VarChar, newEntity.FLAG);
            
            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int updateData(MedDrugGroupEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_UPDATE);
            comm.AddInputParameter(p_druggroup_id, OracleType.VarChar, newEntity.DRUGGROUP_ID);
            comm.AddInputParameter(p_druggroup_name, OracleType.VarChar, newEntity.DRUGGROUP_NAME);
            comm.AddInputParameter(p_druggroup_desc, OracleType.VarChar, newEntity.DRUGGROUP_DESC);
            comm.AddInputParameter(p_update_by, OracleType.VarChar, newEntity.UPDATE_BY);
            comm.AddInputParameter(p_flag, OracleType.VarChar, newEntity.FLAG);

            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int deleteData(MedDrugGroupEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_DELETE);
            comm.AddInputParameter(p_druggroup_id, OracleType.VarChar, newEntity.DRUGGROUP_ID);

            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int deleteDataByFlag(MedDrugGroupEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_DELETE_BY_FLAG);
            comm.AddInputParameter(p_druggroup_id, OracleType.VarChar, newEntity.DRUGGROUP_ID);

            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int canBeDeleted(MedDrugGroupEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_CAN_BE_DELETED);
            comm.AddInputParameter(p_druggroup_id, OracleType.VarChar, newEntity.DRUGGROUP_ID);
            comm.AddOutputParameter(outputResult3, OracleType.VarChar, 100000);
            comm.ExecuteNonQuery();
            medDeleted = comm.GetParameter(outputResult3);
            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }


        public MedDrugGroupEntity getMedDrug(string drugId)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SELECT);
            comm.AddInputParameter(p_druggroup_id, OracleType.VarChar, drugId);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.DateTime, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugGroupEntity>();
            foreach (MedDrugGroupEntity m in dataList)
            {
                data = m;
            }
            return data;
        }



        //Utilities
        public DropDownList getDropdown(DropDownList d1, string txtName, string txtValue, string firstItem)
        {
            getListDropdown();
            d1.DataSource = dataList;
            d1.DataTextField = txtName;
            d1.DataValueField = txtValue;
            d1.DataBind();
            if (firstItem != null || firstItem != " ")
            {
                ListItem i = new ListItem();
                i.Text=firstItem;
                i.Value = "";
                d1.Items.Insert(0, i);
            }
            return d1;
        }

        public DropDownList getDropdown(DropDownList d1, string txtName, string txtValue, string firstItem,string drugtype_id)
        {
            getListDropdown(drugtype_id);
            d1.DataSource = dataList;
            d1.DataTextField = txtName;
            d1.DataValueField = txtValue;
            d1.DataBind();
            if (firstItem != null || firstItem != " ")
            {
                ListItem i = new ListItem();
                i.Text = firstItem;
                i.Value = "";
                d1.Items.Insert(0, i);
            }
            return d1;
        }

        public List<MedDrugGroupEntity> getListSearch( MedDrugGroupEntity _pOdj)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SEARCH);
            comm.AddInputParameter(p_druggroup_id, OracleType.VarChar, _pOdj.DRUGGROUP_ID);
            comm.AddInputParameter(p_druggroup_desc, OracleType.VarChar, _pOdj.DRUGGROUP_DESC);
            comm.AddInputParameter(p_start_date, OracleType.VarChar, _pOdj.START_DATE);
            comm.AddInputParameter(p_end_date, OracleType.VarChar, _pOdj.END_DATE);
            comm.AddInputParameter(p_flag, OracleType.VarChar, _pOdj.FLAG);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            //comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            //medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugGroupEntity>();

            return dataList;
        }
        
    }
}
