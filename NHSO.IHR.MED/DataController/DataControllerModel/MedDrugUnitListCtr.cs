﻿using NHSO.IHR.MED.Data.Oracle.Common;
using NHSO.IHR.MED.Entity.DataController;
using NHSO.IHR.MED.Entity;
using System;
using System.Collections.Generic;
using System.Data.OracleClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace NHSO.IHR.MED.DataController
{
    public class MedDrugUnitListCtr : MotherDrugCtr
    {

        //Declaraion-PL & Package
        private string PKG_NAME = "PKG_MED_UNIT_LIST";
        private string MD_LIST_SELECT = ".MD_LIST_SELECT";
        private string MD_SELECT = ".MD_SELECT";
        private string MD_LIST_SELECT_DROPDOWN = ".MD_LIST_SELECT_DROPDOWN";
        private string MD_UPDATE = ".MD_UPDATE";
        private string MD_DELETE = ".MD_DELETE";
        private string MD_DELETE_BY_FLAG = ".MD_DELETE_BY_FLAG";
        private string MD_CAN_BE_DELETED = ".MD_CAN_BE_DELETED";
        private string MD_INSERT = ".MD_INSERT";

        //Declaration-Variable
        public List<MedDrugUnitListEntity> dataList { get; set; }
        public MedDrugUnitListEntity data { get; set; }
        public string medAmount { get; set; }
        public string medDeleted { get; set; }
        private string outputResult1 = "o_result";
        private string outputResult2 = "o_lastupdate";
        private string outputResult3 = "o_amount";

        //Declaration-Column_Name
        public string UNITLIST_ID = "UNITLIST_ID";
        public string UNITLIST_NAME = "UNITLIST_NAME";
        public string UNITLIST_DESC = "UNITLIST_DESC";
        public string CREATE_BY = "CREATE_BY";
        public string CREATE_DATE = "CREATE_DATE";
        public string UPDATE_BY = "UPDATE_BY";
        public string UPDATE_DATE = "UPDATE_DATE";
        public string FLAG = "FLAG";

        //Declaration-Params
        public string p_unitlist_id = "p_unitlist_id";
        public string p_unitlist_name = "p_unitlist_name";
        public string p_unitlist_desc = "p_unitlist_desc";
        public string p_create_by = "p_create_by";
        public string p_update_by = "p_update_by";
        public string p_flag = "p_flag";

        public MedDrugUnitListCtr()
        {
            this.lastUpdate = "";
            this.medAmount = "";
            this.medDeleted = "";
            this.dataList = new List<MedDrugUnitListEntity>();
            data = new MedDrugUnitListEntity();
        }

        //SQL-Command    
        public List<MedDrugUnitListEntity> getList()
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_LIST_SELECT);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugUnitListEntity>();
            return dataList;
        }

        public List<MedDrugUnitListEntity> getListDropdown()
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_LIST_SELECT_DROPDOWN);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.DateTime, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugUnitListEntity>();
            return dataList;
        }

        public int insertData(MedDrugUnitListEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_INSERT);
            comm.AddInputParameter(p_unitlist_name, OracleType.VarChar, newEntity.UNITLIST_NAME);
            comm.AddInputParameter(p_unitlist_desc, OracleType.VarChar, newEntity.UNITLIST_DESC);
            comm.AddInputParameter(p_create_by, OracleType.VarChar, newEntity.CREATE_BY);
            comm.AddInputParameter(p_update_by, OracleType.VarChar, newEntity.UPDATE_BY);
            comm.AddInputParameter(p_flag, OracleType.VarChar, newEntity.FLAG);

            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int updateData(MedDrugUnitListEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_UPDATE);
            comm.AddInputParameter(p_unitlist_id, OracleType.VarChar, newEntity.UNITLIST_ID);
            comm.AddInputParameter(p_unitlist_name, OracleType.VarChar, newEntity.UNITLIST_NAME);
            comm.AddInputParameter(p_unitlist_desc, OracleType.VarChar, newEntity.UNITLIST_DESC);
            comm.AddInputParameter(p_update_by, OracleType.VarChar, newEntity.UPDATE_BY);
            comm.AddInputParameter(p_flag, OracleType.VarChar, newEntity.FLAG);

            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int deleteData(MedDrugUnitListEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_DELETE);
            comm.AddInputParameter(p_unitlist_id, OracleType.VarChar, newEntity.UNITLIST_ID);

            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int deleteDataByFlag(MedDrugUnitListEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_DELETE_BY_FLAG);
            comm.AddInputParameter(p_unitlist_id, OracleType.VarChar, newEntity.UNITLIST_ID);

            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int canBeDeleted(MedDrugUnitListEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_CAN_BE_DELETED);
            comm.AddInputParameter(p_unitlist_id, OracleType.VarChar, newEntity.UNITLIST_ID);
            comm.AddOutputParameter(outputResult3, OracleType.VarChar, 100000);
            comm.ExecuteNonQuery();
            medDeleted = comm.GetParameter(outputResult3);
            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }


        public MedDrugUnitListEntity getMedDrug(string drugId)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SELECT);
            comm.AddInputParameter(p_unitlist_id, OracleType.VarChar, drugId);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.DateTime, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugUnitListEntity>();
            foreach (MedDrugUnitListEntity m in dataList)
            {
                data = m;
            }
            return data;
        }


        //Utility
        public DropDownList getDropdown(DropDownList d1, string txtName, string txtValue, string firstItem)
        {
            getListDropdown();
            d1.DataSource = dataList;
            d1.DataTextField = txtName;
            d1.DataValueField = txtValue;
            d1.DataBind();
            if (firstItem != null || firstItem != " ")
            {
                ListItem i = new ListItem();
                i.Text = firstItem;
                i.Value = "";
                d1.Items.Insert(0, i);
            }
            return d1;
        }
    }
}
