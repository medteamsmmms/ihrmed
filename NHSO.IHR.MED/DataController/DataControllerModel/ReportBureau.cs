﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHSO.IHR.MED.Entity;
using NHSO.IHR.MED.Data.Oracle.Common;
using NHSO.IHR.MED.Utility;
using System.Data.OracleClient;
using System.Data;
namespace NHSO.IHR.MED.admin
{
    public class ReportBureau
    {
        private string pkgnameBurea = "PKG_RPT_ACS_HTR";
        private string pkgReport = "PKG_RPT_EACH_BUREAU";
        private string refcursor = "o_result";
        public ReportBureau()
        { }
        public List<ReportAccessHistoryEntity> GetBurea()
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(pkgnameBurea + ".MD_DROPDOWN");
            comm.AddOutputParameter(refcursor, OracleType.Cursor, 100000);
            return comm.ExecuteToList<ReportAccessHistoryEntity>();
        }
        public DataSet GetReportDetail(string burea, string sdate, string edate)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(pkgReport + ".md_search_report_bureau");
            comm.AddOutputParameter(refcursor, OracleType.Cursor, 100000);
            comm.AddInputParameter("p_emp_bureau", OracleType.VarChar, burea.ToStringOrNull());
            comm.AddInputParameter("p_date_begin", OracleType.VarChar, sdate);
            comm.AddInputParameter("p_date_end", OracleType.VarChar, edate);
            return comm.ExecuteToDataset();
        }
    }
}