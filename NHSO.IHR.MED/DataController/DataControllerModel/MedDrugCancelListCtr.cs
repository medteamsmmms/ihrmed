﻿using NHSO.IHR.MED.Data.Oracle.Common;
using NHSO.IHR.MED.Entity.DataController;
using NHSO.IHR.MED.Entity;
using System;
using System.Collections.Generic;
using System.Data.OracleClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace NHSO.IHR.MED.DataController.DataControllerModel
{
    [Serializable]
    public class MedDrugCancelListCtr
    {

        //Declaration-Variable
        public List<MedDrugCancelListEntity> dataList { get; set; }
        
        

        //Declaration-Column_Name
        public string CANCEL_ID = "CANCEL_ID";
        public string CANCEL_CAPTION = "CANCEL_CAPTION";
        



        public MedDrugCancelListCtr()
        {
            this.dataList = new List<MedDrugCancelListEntity>();
            executeDataCommand();
        }

        public void executeDataCommand()
        {
            MedDrugCancelListEntity medCanList1 = new MedDrugCancelListEntity();
            MedDrugCancelListEntity medCanList2 = new MedDrugCancelListEntity();

            medCanList1.CANCEL_ID = "Y"; // Y = Yes
            medCanList1.CANCEL_CAPTION = "ใช้งานรายการ";
            medCanList2.CANCEL_ID = "N"; // N = No
            medCanList2.CANCEL_CAPTION = "ยกเลิกรายการ";
            dataList.Add(medCanList1);
            dataList.Add(medCanList2);
        }

        public DropDownList getDropdown(DropDownList d1, string txtName, string txtValue, string firstItem)
        {
            d1.DataSource = dataList;
            d1.DataTextField = txtName;
            d1.DataValueField = txtValue;
            d1.DataBind();
            /*if (firstItem != null || firstItem != " ")
            {
                ListItem i = new ListItem();
                i.Text = firstItem;
                i.Value = "";
                d1.Items.Insert(0, i);
            }*/
            return d1;
        }
    }
}
