﻿using NHSO.IHR.MED.Data.Oracle.Common;
using NHSO.IHR.MED.Entity;
using System;
using System.Collections.Generic;
using System.Data.OracleClient;
using System.Linq;
using System.Web;
using System.Data;
namespace NHSO.IHR.MED.Entity.DataController
{
    [Serializable]
    public class MedDrugStockCtr : MotherDrugCtr
    {

        //Declaraion-PL & Package
        private string PKG_NAME = "PKG_MED_DRUG_STOCK";
        private string MD_LIST_SELECT = ".MD_LIST_SELECT";
        private string MD_SELECT = ".MD_SELECT";
        private string MD_SELECT_DUP = ".MD_SELECT_IS_DUPLICATE";
        private string MD_UPDATE = ".MD_UPDATE";
        private string MD_DELETE = ".MD_DELETE";
        private string MD_INSERT_INTO_FLOWSTOCK = ".MD_INSERT_INTO_FLOWSTOCK";
        private string MD_INSERT_INTO_OVERSTOCK = ".MD_INSERT_INTO_OVERSTOCK";
        private string MD_SEARCH = ".MD_SEARCH";

        //Declaration-Variable
        public List<MedDrugStockEntity> dataList { get; set; }
        public MedDrugStockEntity data { get; set; }
        public string medAmount { get; set; }
        private string outputResult1 = "o_result";
        private string outputResult2 = "o_lastupdate";
        private string outputResult3 = "o_amount";

        //Declaration-Params
        private string p_stock_min = "p_stock_min";
        private string p_stock_max = "p_stock_max";
        private string p_flag = "p_flag";
        private string p_druglist_name = "p_druglist_name";
        private string p_druggroup_id = "p_druggroup_id";
        private string p_drugtype_id = "p_drugtype_id";

        private string p_druglist_index = "p_druglist_index";
        private string p_druglist_id= "p_druglist_id";   
        private string p_stock_input = "p_stock_input";
        private string p_stock_output = "p_stock_output";
        private string p_create_by = "p_create_by";
        private string p_update_by = "p_update_by";
        private string p_stock_balance = "p_stock_balance";
       


        //Constructor
        public MedDrugStockCtr()
        {
            this.lastUpdate = "";
            this.medAmount = "";
            this.dataList = new List<MedDrugStockEntity>();
            this.data = new MedDrugStockEntity();
        }

        //Method
        public List<MedDrugStockEntity> getList()
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME+MD_LIST_SELECT);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugStockEntity>();
            return dataList;
        }

        public int insertData(MedDrugStockEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.CreateTransection();   
            if (!isDuplicated(newEntity.DRUGLIST_INDEX))
            {                                             
                //Add Flow
                comm.SetCommandStoredProcedureTran(PKG_NAME + MD_INSERT_INTO_FLOWSTOCK);
                comm.AddInputParameter(p_druglist_index, OracleType.VarChar, newEntity.DRUGLIST_INDEX);
                comm.AddInputParameter(p_stock_input, OracleType.VarChar, newEntity.STOCK_INPUT);
                comm.AddInputParameter(p_stock_output, OracleType.VarChar, newEntity.STOCK_OUTPUT);
                comm.AddInputParameter(p_create_by, OracleType.VarChar, newEntity.CREATE_BY);
                comm.AddInputParameter(p_update_by, OracleType.VarChar, newEntity.UPDATE_BY);
                successful = comm.ExecuteTrans();

                //Add Balance
                comm.SetCommandStoredProcedureTran(PKG_NAME + MD_INSERT_INTO_OVERSTOCK);
                comm.AddInputParameter(p_druglist_index, OracleType.VarChar, newEntity.DRUGLIST_INDEX);
                comm.AddInputParameter(p_stock_balance, OracleType.VarChar, newEntity.STOCK_BALANCE);
                comm.AddInputParameter(p_update_by, OracleType.VarChar, newEntity.UPDATE_BY);
                successful = comm.ExecuteTrans();
            }
            else
            {
                //Add Flow
                comm.SetCommandStoredProcedureTran(PKG_NAME + MD_INSERT_INTO_FLOWSTOCK);
                comm.AddInputParameter(p_druglist_index, OracleType.VarChar, newEntity.DRUGLIST_INDEX);
                comm.AddInputParameter(p_stock_input, OracleType.VarChar, newEntity.STOCK_INPUT);
                comm.AddInputParameter(p_stock_output, OracleType.VarChar, newEntity.STOCK_OUTPUT);
                comm.AddInputParameter(p_create_by, OracleType.VarChar, newEntity.CREATE_BY);
                comm.AddInputParameter(p_update_by, OracleType.VarChar, newEntity.UPDATE_BY);
                successful = comm.ExecuteTrans();

                //Add Balance
                comm.SetCommandStoredProcedureTran(PKG_NAME + MD_UPDATE);
                comm.AddInputParameter(p_druglist_index, OracleType.VarChar, newEntity.DRUGLIST_INDEX);
                comm.AddInputParameter(p_stock_balance, OracleType.VarChar, newEntity.STOCK_BALANCE);
                comm.AddInputParameter(p_update_by, OracleType.VarChar, newEntity.UPDATE_BY);
                successful = comm.ExecuteTrans();
            }
            
            //Has added 1 row: "true" 
            comm.TransCommit();
            return successful;
        }

        public int updateData(MedDrugListEntity newEntity)
        {
            int successful = 0;
            return successful;
        }

        public int deleteData(MedDrugListEntity newEntity)
        {
            int successful = 0;
            return successful;
        }

        public MedDrugStockEntity getMedStock(string drugId)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SELECT);
            comm.AddInputParameter(p_druglist_id, OracleType.VarChar, drugId);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.DateTime, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugStockEntity>();
            foreach (MedDrugStockEntity m in dataList)
            {
                data = m;
            }
            return data;
        }

        public Boolean isDuplicated(string id)
        {
            List<MedDrugStockEntity> mList = new List<MedDrugStockEntity>();
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SELECT_DUP);
            comm.AddInputParameter(p_druglist_index, OracleType.VarChar, id);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.DateTime, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            mList = comm.ExecuteToList<MedDrugStockEntity>();
            if (mList.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //Search
        public List<MedDrugStockEntity> searhFor(MedDrugStockEntity e)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SEARCH);
            comm.AddInputParameter(p_druglist_name, OracleType.VarChar, e.DRUGLIST_NAME);
            comm.AddInputParameter(p_druggroup_id, OracleType.VarChar, e.DRUGGROUP_ID);
            comm.AddInputParameter(p_drugtype_id, OracleType.VarChar, e.DRUGTYPE_ID);
            comm.AddInputParameter(p_flag, OracleType.VarChar, e.FLAG);
            comm.AddInputParameter(p_stock_min, OracleType.VarChar, e.STOCK_MIN);
            comm.AddInputParameter(p_stock_max, OracleType.VarChar, e.STOCK_MAX);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugStockEntity>();
            return dataList;
        }
        public DataSet searhFords(MedDrugStockEntity e)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SEARCH);
            comm.AddInputParameter(p_druglist_name, OracleType.VarChar, e.DRUGLIST_NAME);
            comm.AddInputParameter(p_druggroup_id, OracleType.VarChar, e.DRUGGROUP_ID);
            comm.AddInputParameter(p_drugtype_id, OracleType.VarChar, e.DRUGTYPE_ID);
            comm.AddInputParameter(p_flag, OracleType.VarChar, e.FLAG);
            comm.AddInputParameter(p_stock_min, OracleType.VarChar, e.STOCK_MIN);
            comm.AddInputParameter(p_stock_max, OracleType.VarChar, e.STOCK_MAX);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            DataSet ds = comm.ExecuteToDataset(); ;
            return ds;
        }
    }
}
