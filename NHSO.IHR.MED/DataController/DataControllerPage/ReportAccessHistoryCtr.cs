﻿using NHSO.IHR.MED.Data.Oracle.Common;
using NHSO.IHR.MED.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace NHSO.IHR.MED.DataController
{
    [Serializable]
    public class ReportAccessHistoryCtr
    {

        //Declaraion-PL & Package
        private string PKG_NAME = "PKG_RPT_ACS_HTR";
        private string MD_SEARCH = ".MD_SEARCH";
        private string MD_SEARCH_BY_EMP_ID = ".MD_SEARCH_BY_EMP_ID";
        private string MD_SEARCH_BY_BRING_ID = ".MD_SEARCH_BY_BRING_ID";
        private string MD_SEARCH_BY_EMP_NAME = ".MD_SEARCH_BY_EMP_NAME";
        private string MD_SEARCH_BY_ORDER_NO = ".MD_SEARCH_BY_ORDER_NO";
        private string MD_LIST_SELECT_TOP_EMP = ".MD_LIST_SELECT_TOP_EMP";
        private string MD_LIST_SELECT_TOP_MED = ".MD_LIST_SELECT_TOP_MED";
        private string MD_SELECT_DRUGBRING_ENTITY = ".MD_SELECT_DRUGBRING_ENTITY";
        private string MD_SELECT_EMPBRING_ENTITY = ".MD_SELECT_EMPBRING_ENTITY";
        private string MD_SELECT_MIN_MAX_DATE = ".MD_SELECT_MIN_MAX_DATE";
        private string MD_DROPDOWN = ".MD_DROPDOWN";

        //Print Package Function
        private string MD_SELECT_LABEL_PRINT = ".MD_SELECT_LABEL_PRINT";
        private string MD_SELECT_LABEL_PRINT_ALL = ".MD_SELECT_LABEL_PRINT_ALL";

        //Declaration-Variable
        public List<ReportAccessHistoryEntity> dataList { get; set; }
        public DataSet dataPrint { get; set; }
        public ReportAccessHistoryEntity data { get; set; }
        public string amount { get; set; }
        public string lastUpdate { get; set; }
        public string firstCreate { get; set; }
        public string minDate { get; set; }
        public string maxDate { get; set; }
        private string outputResult1 = "o_result";
        private string outputResult2 = "o_lastupdate";
        private string outputResult3 = "o_amount";
        private string outputResult4 = "o_firstcreate";
        private string outputResult5 = "o_min_date";
        private string outputResult6 = "o_max_date";  
        
        //Declaration-Params
        public string p_emp_index = "p_emp_index";
        public string p_emp_bureau = "p_emp_bureau";
        public string p_date_begin = "p_date_begin";
        public string p_date_end = "p_date_end";
        public string p_top_index = "p_top_index";
        public string p_emp_id = "p_emp_id";
        public string p_drugbring_id = "p_drugbring_id";
        public string p_druglist_index = "p_druglist_index";


        public ReportAccessHistoryCtr()
        {
            getMinMaxDate();
            this.lastUpdate = "";
            this.amount = "";
            this.dataList = new List<ReportAccessHistoryEntity>();
            data = new ReportAccessHistoryEntity();
            dataPrint = new DataSet();
        }

        //SQL-Command    
        public List<ReportAccessHistoryEntity> getListTopEmp(int index)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_LIST_SELECT_TOP_EMP);
            comm.AddInputParameter(p_top_index, OracleType.VarChar, index);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.DateTime, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            amount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<ReportAccessHistoryEntity>();
            return dataList;
        }

        public List<ReportAccessHistoryEntity> getListTopMed(int index)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_LIST_SELECT_TOP_MED);
            comm.AddInputParameter(p_top_index, OracleType.VarChar, index);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.DateTime, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            amount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<ReportAccessHistoryEntity>();
            return dataList;
        }

        public List<ReportAccessHistoryEntity> getEmpBringEntity(ReportAccessHistoryEntity r)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SELECT_EMPBRING_ENTITY);
            comm.AddInputParameter(p_emp_id, OracleType.VarChar, r.EMP_ID);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            amount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<ReportAccessHistoryEntity>();
            return dataList;
        }

        public List<ReportAccessHistoryEntity> getDrugBringEntity(ReportAccessHistoryEntity r)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SELECT_DRUGBRING_ENTITY);
            comm.AddInputParameter(p_drugbring_id, OracleType.VarChar, r.DRUGBRING_ID);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            amount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<ReportAccessHistoryEntity>();
            return dataList;
        }

        public List<ReportAccessHistoryEntity> searchFor(ReportAccessHistoryEntity e)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SEARCH);
            comm.AddInputParameter(p_emp_index, OracleType.VarChar, e.DRUGLIST_NAME);
            comm.AddInputParameter(p_date_begin, OracleType.VarChar, e.BEGIN_DATE);
            comm.AddInputParameter(p_date_end, OracleType.VarChar, e.END_DATE);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.DateTime, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            amount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<ReportAccessHistoryEntity>();
            return dataList;
        }

        public List<ReportAccessHistoryEntity> searchByEmpID(ReportAccessHistoryEntity e)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SEARCH_BY_EMP_ID);
            comm.AddInputParameter(p_emp_index, OracleType.VarChar, e.DRUGLIST_NAME);
            comm.AddInputParameter(p_emp_bureau, OracleType.VarChar, e.EMP_BUREAUID);
            comm.AddInputParameter(p_date_begin, OracleType.VarChar, e.BEGIN_DATE);
            comm.AddInputParameter(p_date_end, OracleType.VarChar, e.END_DATE);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            amount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<ReportAccessHistoryEntity>();
            return dataList;
        }
        public List<ReportAccessHistoryEntity> searchEmpName(ReportAccessHistoryEntity e)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SEARCH_BY_EMP_NAME);
            comm.AddInputParameter(p_emp_index, OracleType.VarChar, e.DRUGLIST_NAME);
            comm.AddInputParameter(p_emp_bureau, OracleType.VarChar, e.EMP_BUREAUID);
            comm.AddInputParameter(p_date_begin, OracleType.VarChar, e.BEGIN_DATE);
            comm.AddInputParameter(p_date_end, OracleType.VarChar, e.END_DATE);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            amount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<ReportAccessHistoryEntity>();
            return dataList;
        }
        public List<ReportAccessHistoryEntity> searchByBringID(ReportAccessHistoryEntity e)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SEARCH_BY_BRING_ID);
            comm.AddInputParameter(p_emp_index, OracleType.VarChar, e.DRUGLIST_NAME);
            comm.AddInputParameter(p_emp_bureau, OracleType.VarChar, e.EMP_BUREAUID);
            comm.AddInputParameter(p_date_begin, OracleType.VarChar, e.BEGIN_DATE);
            comm.AddInputParameter(p_date_end, OracleType.VarChar, e.END_DATE);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            amount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<ReportAccessHistoryEntity>();
            return dataList;
        }
        public List<ReportAccessHistoryEntity> searchByOrderNo(ReportAccessHistoryEntity e)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SEARCH_BY_ORDER_NO);
            comm.AddInputParameter(p_emp_index, OracleType.VarChar, e.DRUGLIST_NAME);
            comm.AddInputParameter(p_emp_bureau, OracleType.VarChar, e.EMP_BUREAUID);
            comm.AddInputParameter(p_date_begin, OracleType.VarChar, e.BEGIN_DATE);
            comm.AddInputParameter(p_date_end, OracleType.VarChar, e.END_DATE);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            amount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<ReportAccessHistoryEntity>();
            return dataList;
        }

        public List<ReportAccessHistoryEntity> getMinMaxDate()
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SELECT_MIN_MAX_DATE);
            comm.AddOutputParameter(outputResult5, OracleType.VarChar, 100000);
            comm.AddOutputParameter(outputResult6, OracleType.VarChar, 10000);
            comm.ExecuteNonQuery();
            minDate = comm.GetParameter(outputResult5);
            maxDate = comm.GetParameter(outputResult6);
            return dataList;
        }

        public DataSet getLabelDesc(ReportAccessHistoryEntity e)
        {            
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SELECT_LABEL_PRINT);
            comm.AddInputParameter(p_emp_id, OracleType.VarChar, e.EMP_ID);
            comm.AddInputParameter(p_drugbring_id, OracleType.VarChar, e.DRUGBRING_ID);
            comm.AddInputParameter(p_druglist_index, OracleType.VarChar, e.DRUGLIST_INDEX);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            dataPrint = comm.ExecuteToDataset();
            return dataPrint;
        }

        public DataSet getLabelDescAll(ReportAccessHistoryEntity e)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SELECT_LABEL_PRINT_ALL);
            comm.AddInputParameter(p_drugbring_id, OracleType.VarChar, e.DRUGBRING_ID);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            dataPrint = comm.ExecuteToDataset();
            return dataPrint;
        }

        public List<ReportAccessHistoryEntity> getListDropdown()
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_DROPDOWN);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            dataList = comm.ExecuteToList<ReportAccessHistoryEntity>();
            return dataList;
        }

        public DropDownList getDropdown(DropDownList d1, string txtName, string txtValue, string firstItem)
        {
            getListDropdown();
            d1.DataSource = dataList;
            d1.DataTextField = txtName;
            d1.DataValueField = txtValue;
            d1.DataBind();
            if (firstItem != null || firstItem != " ")
            {
                ListItem i = new ListItem();
                i.Text = firstItem;
                i.Value = "";
                d1.Items.Insert(0, i);
            }
            return d1;
        }
    }
}
