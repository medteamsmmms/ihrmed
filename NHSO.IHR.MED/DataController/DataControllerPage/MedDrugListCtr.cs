﻿using NHSO.IHR.MED.Data.Oracle.Common;
using System;
using System.Collections.Generic;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using NHSO.IHR.MED.Entity;
namespace NHSO.IHR.MED.Entity.DataController
{
    [Serializable]
    class MedDrugListCtr : MotherDrugCtr
    {

        //Declaraion-PL & Package
        private string PKG_NAME = "PKG_MED_DRUG_LIST";
        private string MD_LIST_SELECT = ".MD_LIST_SELECT";
        private string MD_SELECT = ".MD_SELECT";
        private string MD_UPDATE = ".MD_UPDATE";
        private string MD_DELETE = ".MD_DELETE";
        private string MD_DELETE_BY_FLAG = ".MD_DELETE_BY_FLAG";
        private string MD_INSERT = ".MD_INSERT";
        private string MD_SEARCH = ".MD_SEARCH";
        private string MD_IS_BARCODE_DUP = ".MD_IS_BARCODE_DUP";
        //Declaration-Variable
        public List<MedDrugListEntity> dataList { get; set; }
        public MedDrugListEntity data { get; set; }
        public string medAmount { get; set; }
        public string barAmount { get; set; }
        private string outputResult1 = "o_result";
        private string outputResult2 = "o_lastupdate";
        private string outputResult3 = "o_amount";

        //Declaration-Params
        private string p_druglist_id = "p_druglist_id";    
        private string p_druggroup_id = "p_druggroup_id";
        private string p_drugtype_id = "p_drugtype_id";
        private string p_unitlist_id = "p_unitlist_id";
        private string p_druglist_name = "p_druglist_name";
        private string p_druglist_desc = "p_druglist_desc";
        private string p_create_by = "p_create_by";
        private string p_update_by = "p_update_by";
        private string p_barcode_standard = "p_barcode_standard";
        private string p_barcode_temp = "p_barcode_temp";
        private string p_druglist_dose = "p_druglist_dose";
        private string p_unittype_id = "p_unittype_id";
        private string p_flag = "p_flag";


        //Constructor
        public MedDrugListCtr() {
            this.lastUpdate = "";
            this.medAmount = "";
            this.dataList = new List<MedDrugListEntity>();
            this.data = new MedDrugListEntity();
        }

        //Method
        public List<MedDrugListEntity> getMedDrugList() {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME+MD_LIST_SELECT);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.DateTime, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugListEntity>();
            return dataList;
        }

        public int insertData(MedDrugListEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_INSERT);
            comm.AddInputParameter(p_druggroup_id, OracleType.VarChar, newEntity.DRUGGROUP_ID);
            comm.AddInputParameter(p_drugtype_id, OracleType.VarChar, newEntity.DRUGTYPE_ID);
            comm.AddInputParameter(p_unitlist_id, OracleType.VarChar, newEntity.UNITLIST_ID);
            comm.AddInputParameter(p_druglist_name, OracleType.VarChar, newEntity.DRUGLIST_NAME);
            comm.AddInputParameter(p_druglist_desc, OracleType.VarChar, newEntity.DRUGLIST_DESC);
            comm.AddInputParameter(p_create_by, OracleType.VarChar, newEntity.CREATE_BY);
            comm.AddInputParameter(p_update_by, OracleType.VarChar, newEntity.UPDATE_BY);
            comm.AddInputParameter(p_barcode_standard, OracleType.VarChar, newEntity.BARCODE_STANDARD);
            comm.AddInputParameter(p_barcode_temp, OracleType.VarChar, newEntity.BARCODE_TEMP);
            comm.AddInputParameter(p_druglist_dose, OracleType.VarChar, newEntity.DRUGLIST_DOSE);
            comm.AddInputParameter(p_unittype_id, OracleType.VarChar, newEntity.UNITTYPE_ID);
            comm.AddInputParameter(p_flag, OracleType.VarChar, newEntity.FLAG);
            
            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int updateData(MedDrugListEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_UPDATE);
            comm.AddInputParameter(p_druglist_id, OracleType.VarChar, newEntity.DRUGLIST_ID);
            comm.AddInputParameter(p_druggroup_id, OracleType.VarChar, newEntity.DRUGGROUP_ID);
            comm.AddInputParameter(p_drugtype_id, OracleType.VarChar, newEntity.DRUGTYPE_ID);
            comm.AddInputParameter(p_unitlist_id, OracleType.VarChar, newEntity.UNITLIST_ID);
            comm.AddInputParameter(p_druglist_name, OracleType.VarChar, newEntity.DRUGLIST_NAME);
            comm.AddInputParameter(p_druglist_desc, OracleType.VarChar, newEntity.DRUGLIST_DESC);
            comm.AddInputParameter(p_update_by, OracleType.VarChar, newEntity.UPDATE_BY);
            comm.AddInputParameter(p_barcode_standard, OracleType.VarChar, newEntity.BARCODE_STANDARD);
            comm.AddInputParameter(p_barcode_temp, OracleType.VarChar, newEntity.BARCODE_TEMP);
            comm.AddInputParameter(p_druglist_dose, OracleType.VarChar, newEntity.DRUGLIST_DOSE);
            comm.AddInputParameter(p_unittype_id, OracleType.VarChar, newEntity.UNITTYPE_ID);
            comm.AddInputParameter(p_flag, OracleType.VarChar, newEntity.FLAG);

            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int deleteData(MedDrugListEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_DELETE);
            comm.AddInputParameter(p_druglist_id, OracleType.VarChar, newEntity.DRUGLIST_ID);      

            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int deleteDataByFlag(MedDrugListEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_DELETE_BY_FLAG);
            comm.AddInputParameter(p_druglist_id, OracleType.VarChar, newEntity.DRUGLIST_ID);

            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            return successful;
        }

        public int isBarcodeDup(MedDrugListEntity newEntity)
        {
            int successful = 0;
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_IS_BARCODE_DUP);
            comm.AddInputParameter(p_druglist_id, OracleType.VarChar, newEntity.DRUGLIST_ID);
            comm.AddInputParameter(p_barcode_standard, OracleType.VarChar, newEntity.BARCODE_STANDARD);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            //Has added 1 row: "true"
            successful = comm.ExecuteNonQuery();
            barAmount = comm.GetParameter(outputResult3);            
            return successful;
        }

        public MedDrugListEntity getMedDrug(string drugId)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SELECT);
            comm.AddInputParameter(p_druglist_id, OracleType.VarChar, drugId);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.DateTime, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugListEntity>();
            foreach (MedDrugListEntity m in dataList)
            {
                data = m;
            }
            return data;
        }

        //Search
        public List<MedDrugListEntity> searhFor(MedDrugListEntity e)
        {
            CommandData comm = new CommandData();
            comm.SetCommandStoredProcedure(PKG_NAME + MD_SEARCH);
            comm.AddInputParameter(p_druglist_name, OracleType.VarChar, e.DRUGLIST_NAME);
            comm.AddInputParameter(p_druggroup_id, OracleType.VarChar, e.DRUGGROUP_ID);
            comm.AddInputParameter(p_drugtype_id, OracleType.VarChar, e.DRUGTYPE_ID);
            comm.AddInputParameter(p_flag, OracleType.VarChar, e.FLAG);
            comm.AddOutputParameter(outputResult1, OracleType.Cursor, 100000);
            comm.AddOutputParameter(outputResult2, OracleType.VarChar, 10000);
            comm.AddOutputParameter(outputResult3, OracleType.UInt32, 10000);
            comm.ExecuteNonQuery();
            lastUpdate = comm.GetParameter(outputResult2);
            medAmount = comm.GetParameter(outputResult3);
            dataList = comm.ExecuteToList<MedDrugListEntity>();
            return dataList;
        }

    }
}
