﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NHSO.IHR.MED.Data.Oracle.Common
{
    public class CommanDataPortal : IDisposable
    {
        #region Member Variable

        private OracleCommand _sqlCommand;

        private bool _autoConnection;
        private static string _connString;
        private int _connTimeout;

        #endregion

        #region Property

        private string ConnectionString
        {
            get
            {
                if (_connString == null)
                {
                    _connString = ConfigurationManager.ConnectionStrings["CONN_PORTAL"].ConnectionString;
                }
                return _connString;
            }
        }

        private int ConnectionTimeout
        {
            get
            {
                if (this._connTimeout == int.MinValue)
                {
                    if (ConfigurationManager.AppSettings["CommandTimeout"] != null)
                    {
                        this._connTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["CommandTimeout"]); //seconds
                    }
                    else
                    {
                        this._connTimeout = 3600;
                    }
                }
                return this._connTimeout;
            }
        }

        #endregion

        #region General Methods

        public CommanDataPortal()
        {
            this._sqlCommand = new OracleCommand();
            this._autoConnection = true;
            _connString = null;
            _connTimeout = int.MinValue;
        }

        public void ReturnConnection()
        {
            _connString = null;
        }

        public void SetAutoConnection(bool isAutoConn)
        {
            this._autoConnection = isAutoConn;
        }

        public void SetCommandText(string cmdText)
        {
            this._sqlCommand.Parameters.Clear();
            this._sqlCommand.CommandText = cmdText;
            this._sqlCommand.CommandType = CommandType.Text;
            this._sqlCommand.CommandTimeout = this.ConnectionTimeout;
        }

        public void SetCommandStoredProcedure(string storedProcName)
        {
            this._sqlCommand.Parameters.Clear();
            this._sqlCommand.CommandText = storedProcName;
            this._sqlCommand.CommandType = CommandType.StoredProcedure;
            this._sqlCommand.CommandTimeout = this.ConnectionTimeout;
        }

        public void AddInputParameter(string paramName, object paramValue)
        {
            OracleParameter param = new OracleParameter(paramName, paramValue);
            param.Direction = ParameterDirection.Input;
            this._sqlCommand.Parameters.Add(paramName, paramValue);
        }

        public void AddInputParameter(string paramName, OracleType paramType, object paramValue)
        {
            OracleParameter param = new OracleParameter(paramName, paramType);
            param.Value = paramValue;
            param.Direction = ParameterDirection.Input;
            this._sqlCommand.Parameters.Add(param);
        }

        public void AddInputParameter(string paramName, OracleType paramType, int paramSize, object paramValue)
        {
            OracleParameter param = new OracleParameter(paramName, paramType, paramSize);
            param.Value = paramValue;
            param.Direction = ParameterDirection.Input;
            this._sqlCommand.Parameters.Add(param);
        }

        public void AddInputParameters<T>(string prefix, T entity)
        {
            //DynamicEntity
            if (typeof(T).IsSubclassOf(typeof(DynamicObject)))
            {
                foreach (var property in ((dynamic)entity).GetProperties())
                {
                    if (property.Value != null)
                    {
                        OracleParameter param = new OracleParameter();
                        param.ParameterName = prefix + property.Key;
                        param.Value = property.Value;
                        param.Direction = ParameterDirection.Input;
                        this._sqlCommand.Parameters.Add(param);
                    }
                }
            }

            //RegularEntity
            foreach (PropertyInfo property in typeof(T).GetProperties())
            {
                if (property.GetValue(entity, null) != null)
                {
                    OracleParameter param = new OracleParameter();
                    param.ParameterName = prefix + property.Name;
                    param.Value = property.GetValue(entity, null);
                    param.Direction = ParameterDirection.Input;
                    this._sqlCommand.Parameters.Add(param);
                }
            }
        }

        public void AddOutputParameter(string paramName, OracleType paramType)
        {
            OracleParameter param = new OracleParameter(paramName, paramType);
            param.Direction = ParameterDirection.Output;
            this._sqlCommand.Parameters.Add(param);
        }

        public void AddOutputParameter(string paramName, OracleType paramType, int paramSize)
        {
            OracleParameter param = new OracleParameter(paramName, paramType, paramSize);
            param.Direction = ParameterDirection.Output;
            this._sqlCommand.Parameters.Add(param);
        }

        public string GetParameter(int index)
        {
            return _sqlCommand.Parameters[index].Value.ToString();
        }

        public string GetParameter(string paramName)
        {
            return _sqlCommand.Parameters[paramName].Value.ToString();
        }

        public void ClearParameter()
        {
            this._sqlCommand.Parameters.Clear();
        }

        public void OpenConnection()
        {
            if (this._sqlCommand.Connection == null)
            {
                this._sqlCommand.Connection = new OracleConnection(this.ConnectionString);
            }

            if (this._sqlCommand.Connection.State != ConnectionState.Open)
            {
                this._sqlCommand.Connection.Open();
            }
        }

        public void CloseConnection()
        {
            if (this._sqlCommand.Connection.State != ConnectionState.Closed)
            {
                this._sqlCommand.Connection.Close();
            }
        }

        public void Dispose()
        {
            if (this._sqlCommand != null)
            {
                if (this._sqlCommand.Connection != null)
                {
                    this.CloseConnection();
                    this._sqlCommand.Connection.Dispose();
                }
                this._sqlCommand.Dispose();
            }
        }

        #endregion General Methods

        #region Execute Methods (Low Level)

        public List<T> ExecuteToList<T>() where T : new()
        {
            List<T> list = new List<T>();

            #region OpenConnection
            if (this._autoConnection)
            {
                this.OpenConnection();
            }
            #endregion

            OracleDataReader reader = this._sqlCommand.ExecuteReader();
            while (reader.Read())
            {
                T entity = new T();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    string field_name = reader.GetName(i);
                    object field_value = null;
                    try
                    {
                        field_value = (DBNull.Value == reader[i]) ? null : reader[i];
                    }
                    catch
                    {

                    }

                    PropertyInfo property = typeof(T).GetProperty(field_name);
                    if (property != null)
                    {
                        //RegularEntity
                        property.SetValue(entity, Convert.ChangeType(field_value, property.PropertyType), null);
                    }
                    else if (typeof(T).IsSubclassOf(typeof(DynamicObject)))
                    {
                        //DynamicEntity
                        ((dynamic)entity).AddProperty(field_name, Convert.ChangeType(field_value, typeof(object)));
                    }
                }
                list.Add(entity);
            }
            reader.Close();

            #region CloseConnection
            if (this._autoConnection)
            {
                this.CloseConnection();
            }
            #endregion

            return list;
        }

        public int ExecuteNonQuery()
        {
            int result = 0;

            #region OpenConnection
            if (this._autoConnection)
            {
                this.OpenConnection();
            }
            #endregion

            result = this._sqlCommand.ExecuteNonQuery();

            #region CloseConnection
            if (this._autoConnection)
            {
                this.CloseConnection();
            }
            #endregion

            return result;
        }

        public Object ExecuteScalar()
        {
            object result = null;

            #region OpenConnection
            if (this._autoConnection)
            {
                this.OpenConnection();
            }
            #endregion

            result = this._sqlCommand.ExecuteScalar();

            #region CloseConnection
            if (this._autoConnection)
            {
                this.CloseConnection();
            }
            #endregion

            return result;
        }

        #endregion
    }
}
