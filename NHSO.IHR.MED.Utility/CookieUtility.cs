﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace NHSO.IHR.MED.Utility
{
    public class CookieUtility
    {
        public static HttpCookie GetCookie(string cookieName)
        {
            return HttpContext.Current.Request.Cookies[cookieName];
        }

        public static string GetCookieValue(string cookieName)
        {
            return HttpContext.Current.Request.Cookies[cookieName].Value;
        }

        public static void CreateCookie(string cookieName, string value, int? expirationMinutes)
        {
            HttpCookie Cookie = new HttpCookie(cookieName, value);
            if (expirationMinutes.HasValue)
                Cookie.Expires = DateTime.Now.AddMinutes(expirationMinutes.Value);
            HttpContext.Current.Response.Cookies.Add(Cookie);
        }
        public static void CreateCookie(string cookieName, Dictionary<string, string> dict, int? expirationMinutes)
        {
            HttpCookie Cookie = new HttpCookie(cookieName);
            foreach (var o in dict)
            {
                Cookie.Values[o.Key] = o.Value;
            }
            if (expirationMinutes.HasValue)
                Cookie.Expires = DateTime.Now.AddMinutes(expirationMinutes.Value);
            HttpContext.Current.Response.Cookies.Add(Cookie);
        }
        public static void CreateCookie(string cookieName, DataRow dr, int? expirationMinutes)
        {
            HttpCookie Cookie = new HttpCookie(cookieName);
            foreach (DataColumn col in dr.Table.Columns)
            {
                Cookie.Values[col.ToString()] = dr.Field<string>(col.ToString());
            }
            if (expirationMinutes.HasValue)
                Cookie.Expires = DateTime.Now.AddMinutes(expirationMinutes.Value);
            HttpContext.Current.Response.Cookies.Add(Cookie);
        }

        public static void DeleteCookie(string cookieName)
        {
            HttpCookie Cookie = HttpContext.Current.Request.Cookies[cookieName];
            if (Cookie != null)
            {
                Cookie.Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies.Add(Cookie);
            }
        }

        public static bool CookieExists(string cookieName)
        {
            if (HttpContext.Current.Request.Cookies[cookieName] != null)
                return true;
            return false;
        }

        public static bool CookieTimeOut(string cookieName)
        {
            if (HttpContext.Current.Request.Cookies[cookieName].Expires > DateTime.Now)
                return false;
            return true;
        }

        public static Dictionary<string, string> GetAllCookies()
        {
            Dictionary<string, string> cookies = new Dictionary<string, string>();
            string[] keys = HttpContext.Current.Request.Cookies.AllKeys;

            foreach (string key in keys)
            {
                cookies.Add(key, HttpContext.Current.Request.Cookies[key].Value);
            }
            return cookies;
        }

        public static void DeleteAllCookies()
        {
            string[] keys = HttpContext.Current.Request.Cookies.AllKeys;
            foreach (string key in keys)
            {
                if (HttpContext.Current.Request.Cookies[key] != null)
                {
                    DeleteCookie(key);
                }
            }
        }

    }
}
