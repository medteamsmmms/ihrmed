﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NHSO.IHR.MED.Utility
{
    public static class SessionManager
    {
        public const string ANNOUCEMENT_ENTITY = "ANNOUCEMENT_ENTITY";
    }

    public static class ViewStateManager
    {
        public const string PAGE_MODE = "PAGE_MODE";
        public const string ANN_ID = "ANN_ID";
        public const string ANN_POSITION_ID = "ANN_POSITION_ID";
        public const string ANN_RESULT_ID = "ANN_RESULT_ID";
        public const string RESUME_ID = "RESUME_ID";
        public const string APPL_ID = "APPL_ID";
        public const string APPL_DEMAND_ID = "APPL_DEMAND_ID";
        public const string REC_ID = "REC_ID";
        public const string EMPLOYEE_ID = "EMPLOYEE_ID";
        public const string ANN_YEAR = "ANN_YEAR";
        public const string RMODE = "RMODE";


        //
        public const string REPORT_BUREAU_ID = "REPORT_BUREAU_ID";
        public const string REPORT_YEAR = "REPORT_YEAR";
        public const string REPORT_MONTH = "REPORT_MONTH";
        public const string REPORT_TRIMAS = "REPORT_TRIMAS";
        public const string REPORT_EMPLOYEE_ID = "REPORT_EMPLOYEE_ID";
        public const string REPORT_TYPE = "REPORT_TYPE";

    }

    public static class CookieManager
    {
        public const string SESSION_KEY = "SESSION_KEY";
        public const string SESSION_VALUE = "ADMIN";
    }

}
