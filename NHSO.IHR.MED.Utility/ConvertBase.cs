﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NHSO.IHR.MED.Utility
{
    public static class ConvertBase
    {
        public static int Toint(this String s)
        {
            int i;
            if (int.TryParse(s, out i))
            {
                return Convert.ToInt32(s);
            }
            else
            {
                return 0;
            }
        }
        public static object ToStringOrNull(this string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return DBNull.Value;
            }
            else
            {
                return s.ToString();
            }
        }
        public static string ToStringTH(this DateTime s)
        {
            return s.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));
        }
        public static string ToStringUS(this DateTime s)
        {
            return s.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US"));
        }
        public static DateTime ToDateUS(this string s)
        {
            try
            {
            return DateTime.ParseExact(s, "dd/MM/yyyy", System.Globalization.CultureInfo.GetCultureInfo("en-US"));
            }
            catch (Exception)
            {
                return DateTime.Now;
            }
        }
    }
}
