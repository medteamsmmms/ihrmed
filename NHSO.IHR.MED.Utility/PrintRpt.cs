﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace NHSO.IHR.MED.Utility
{
    public static class PrintRpt
    {
        public static void PrintLabel(DataSet ds, string path, System.Web.UI.Page page)
        {
            string repFilePath = path;
            using (ReportDocument rptDoc = new ReportDocument())
            {
                // Set Name ให้Datatable ใหม่
                DataTable dt = ds.Tables[0];
                dt.TableName = "DataTable1";
                int i = 1;
                foreach (DataColumn item in dt.Columns)
                {
                    dt.Columns[item.ColumnName].ColumnName = "DataColumn" + i;
                    i += 1;
                }
                string labelname = "label";
                if (dt!=null&&dt.Rows.Count>0)
                {
                    labelname = dt.Rows[0]["DataColumn10"] + "-" + dt.Rows[0]["DataColumn1"] + "-" + dt.Rows[0]["DataColumn9"];
                }
              
                ParameterValues pvCustPattern = new ParameterValues();
                ParameterDiscreteValue pdvCustPattern = new ParameterDiscreteValue();

                rptDoc.FileName = repFilePath;
                rptDoc.SetDataSource(dt);
                //CrystalReportViewer viewer = new CrystalReportViewer();
                //viewer.ReportSource = rptDoc;
                //viewer.PrintMode = PrintMode.ActiveX;
                //rptDoc.PrintOptions.PrinterName = "Intermec PF8t (203 dpi)";
                //rptDoc.PrintToPrinter(1, false, 0, 0);
                rptDoc.ExportToHttpResponse(ExportFormatType.PortableDocFormat, page.Response, true, labelname);
            }
        }
        public static void ExportExcel(DataSet ds, string path, System.Web.UI.Page page)
        {
            string repFilePath = path;
            using (ReportDocument rptDoc = new ReportDocument())
            {
                // Set Name ให้Datatable ใหม่
                DataTable dt = ds.Tables[0];
                dt.TableName = "DataTable1";
                int i = 1;
                foreach (DataColumn item in dt.Columns)
                {
                    dt.Columns[item.ColumnName].ColumnName = "DataColumn" + i;
                    i += 1;
                }
                string labelname = "drugstock";
                ParameterValues pvCustPattern = new ParameterValues();
                ParameterDiscreteValue pdvCustPattern = new ParameterDiscreteValue();

                rptDoc.FileName = repFilePath;
                rptDoc.SetDataSource(dt);
                rptDoc.SetCssClass(ObjectScope.AllReportObjectsInPageHeaderSections, "width:auto");
                //CrystalReportViewer viewer = new CrystalReportViewer();
                //viewer.ReportSource = rptDoc;
                //viewer.PrintMode = PrintMode.ActiveX;
                //rptDoc.PrintOptions.PrinterName = "Intermec PF8t (203 dpi)";
                //rptDoc.PrintToPrinter(1, false, 0, 0);
                rptDoc.ExportToHttpResponse(ExportFormatType.ExcelWorkbook, page.Response, false, labelname);
            }
        }
    }
}
