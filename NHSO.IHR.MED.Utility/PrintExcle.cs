﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NHSO.IHR.MED.Utility
{
    public static class PrintExcle
    {
        public static void PrintLabel(string filename, string Haederhtml, System.Web.UI.Page page, GridView GridView1, List<int> HideColumns)
        {
            string style;
            page.Response.Clear();
            page.Response.Buffer = true;
            page.Response.AddHeader("content-disposition", "attachment;filename="+filename);
            page.Response.Charset = "UTF-8";
            page.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-874");
            page.Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                hw.WriteLine(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 5.0 Transitional//EN"">");
                hw.WriteLine("<meta http-equiv='Content-Type' content='text/html; charset=windows-874'>");
                //To Export all pages
                GridView1.AllowPaging = false;
                // this.BindGrid();
                //bindToListPage(medCtr.searhFor(drugLabel));
                foreach (int i in HideColumns)
                {
                    GridView1.Columns[i].Visible = false;
                }

                if (GridView1.Rows.Count != 0)
                {
                    GridView1.HeaderRow.BackColor = Color.White;
                    GridView1.Width = 700;


                    foreach (TableCell cell in GridView1.HeaderRow.Cells)
                    {
                        cell.BackColor = GridView1.HeaderStyle.BackColor;
                    }
                    foreach (GridViewRow row in GridView1.Rows)
                    {
                        row.BackColor = Color.White;
                        foreach (TableCell cell in row.Cells)
                        {
                            if (row.RowIndex % 2 == 0)
                            {
                                cell.BackColor = GridView1.AlternatingRowStyle.BackColor;
                                cell.Wrap = true;
                            }
                            else
                            {
                                cell.BackColor = GridView1.RowStyle.BackColor;
                            }
                            cell.CssClass = "textmode";

                        }
                    }
                }
                
                GridView1.RenderControl(hw);
                //style to format numbers to string
                style = @"<style> .textmode { mso-number-format:\@; } </style><div align='center'>" + Haederhtml + "</div>";
                page.Response.Write(style);
                page.Response.Output.Write(sw.ToString());
                page.Response.Flush();
                page.Response.End();
            }
        }
    }
}
